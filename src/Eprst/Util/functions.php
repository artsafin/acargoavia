<?php

namespace Eprst\Util;

use Symfony\Component\Translation\TranslatorInterface;

function gettrans(TranslatorInterface $trans, $defaultDomain = 'messages')
{
    return function ($id, $params = array(), $domain = null) use ($trans, $defaultDomain) {
        $return = $trans->trans($id, $params, empty($domain)?$defaultDomain:$domain);
        if ($return == $id)
            $return = $trans->trans($id, $params);

        return $return;
    };
}