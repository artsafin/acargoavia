<?php

namespace Eprst\Bundle\CurrencyBundle\Controller;

use Eprst\Bundle\CurrencyBundle\Entity\CurrencyRate;
use Eprst\Bundle\CurrencyBundle\Repository\CurrencyRate as CurrencyRateRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CurrentController extends Controller
{
    public function indexAction()
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('EprstCurrencyBundle:CurrencyRate');
        $entities = $repo->findBy(array(), array('ts' => 'DESC'));

        return $this->render('EprstCurrencyBundle:Current:index.html.twig',
            array(
                 'items' => $entities
            )
        );
    }

    public function setAction()
    {
        $newRate = new CurrencyRate();
        $form = $this->createCurrencyForm($newRate);

        return $this->render('EprstCurrencyBundle:Current:set.html.twig',
                             array(
                                   'form' => $form->createView()
                              ));
    }

    public function setPersistAction(Request $request)
    {
        $newRate = new CurrencyRate();
        $form    = $this->createCurrencyForm($newRate);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $newRate->setPair('USDRUR');

            $em = $this->getDoctrine()->getManager();
            $em->persist($newRate);
            $em->flush();

            return $this->redirect($this->generateUrl('currency_rates_current'));
        }
        else
        {
            return $this->render(
                'EprstCurrencyBundle:Current:set.html.twig',
                array(
                     'form' => $form->createView(),
                     'errors' => $form->getErrors()
                )
            );
        }
    }

    private function createCurrencyForm(CurrencyRate $entity)
    {
        return $this->createForm(
            'currencybundle_setrate',
            $entity,
            array(
                 'action' => $this->generateUrl('currency_rates_set')
            )
        );
    }

    /**
     * loadCurrency
     *
     * @param $id
     *
     * @return CurrencyRate
     */
    private function loadCurrency($id)
    {
        /** @var CurrencyRateRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('EprstCurrencyBundle:CurrencyRate');
        $entity = $repo->find($id);

        return $entity;
    }

    public function deleteAction($id = null)
    {
        $entity = $this->loadCurrency($id);
        $form    = $this->createForm(
            'acargobundle_sure',
            null,
            array(
                 'action' => $this->generateUrl('currency_rates_delete_persist', array('id' => $entity->getId()))
            )
        );

        return $this->render(
            'EprstCurrencyBundle:Current:delete.html.twig',
            array(
                 'entity' => $entity,
                 'form'    => $form->createView()
            )
        );
    }

    public function deletePersistAction($id, Request $request)
    {
        $entity = $this->loadCurrency($id);

        $form = $this->createForm('acargobundle_sure');
        $form->handleRequest($request);

        if ($form->get('yes')->isClicked()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('currency_rates_current'));
    }

}
