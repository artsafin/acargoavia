<?php
/**
 * Global Trading Technologies Ltd.
 *
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of
 * this source code is governed by the Global Trading Technologies Ltd. Non-Disclosure
 * Agreement previously entered between you and Global Trading Technologies Ltd.
 *
 * By accessing, using, copying, modifying or distributing this
 * software, you acknowledge that you have been informed of your
 * obligations under the Agreement and agree to abide by those obligations.
 */

namespace Eprst\Bundle\CurrencyBundle\Service;
use Eprst\Bundle\CurrencyBundle\Entity\CurrencyRate as CurrencyRateEntity;
use Eprst\Bundle\CurrencyBundle\Repository\CurrencyRate as CurrencyRateRepository;

/**
 * CurrencyService
 */
class CurrencyService
{
    /**
     * @var \Eprst\Bundle\CurrencyBundle\Repository\CurrencyRate
     */
    private $currRepo;

    public function __construct(CurrencyRateRepository $currRepo)
    {
        $this->currRepo = $currRepo;
    }

    /**
     * @return \DateTime
     */
    public function getLastRateTimestamp()
    {
        /** @var CurrencyRateEntity $lastRate */
        $lastRate = $this->currRepo->findOneBy(array(), array('ts' => 'DESC'));

        return $lastRate->getTs();
    }

    /**
     * @param $date
     *
     * @throws \Exception
     * @return float
     */
    public function getRateForDate($date)
    {
        /** @var CurrencyRateEntity $entity */
        $entity = $this->currRepo->findByTs($date);
        if (!$entity) {
            throw new \Exception('Rate doesnt exist');
        }

        return $entity->getValue();
    }
}
