<?php

namespace Eprst\Bundle\CurrencyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CurrencyRateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
                    'ts',
                    'date',
                    array(
                         'required' => true,
                         'label' => 'Date',
                         'widget' => 'single_text',
                         'format' => 'dd.MM.yyyy',
                         'attr' => array('class' => 'datepicked'),
                         'data' => new \DateTime()
                    )
                )->add('value', null, array('required' => true))->add(
                    'save',
                    'submit',
                    array('attr' => array('class' => 'btn btn-primary'))
                );
        $builder->setAction($options['action']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                 'data_class' => 'Eprst\Bundle\CurrencyBundle\Entity\CurrencyRate'
            )
        );
    }

    public function getName()
    {
        return 'currencybundle_setrate';
    }
}
