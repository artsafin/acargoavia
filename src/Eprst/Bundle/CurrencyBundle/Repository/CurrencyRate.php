<?php

namespace Eprst\Bundle\CurrencyBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Eprst\Bundle\CurrencyBundle\Entity\CurrencyRate as CurrencyRateEntity;

/**
 * CurrencyRate
 * 
 */
class CurrencyRate extends EntityRepository
{
    /**
     * findByTs
     *
     * @param \DateTime $ts
     *
     * @return mixed
     */
    public function findByTs($ts)
    {
        $qb = $this->createQueryBuilder('r');
        $qb->where('r.ts <= :ts');
        $qb->orderBy('r.ts', 'DESC');
        $qb->setMaxResults(1);

        $qb->setParameters(array(
                                ':ts' => $ts->format('Y-m-d 23:59:59')
                           ));

        $result = $qb->getQuery()->getOneOrNullResult();

        return $result;
    }
}
