<?php

namespace Eprst\Bundle\CurrencyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CurrencyRate
 *
 * @ORM\Table(name="currency_rate")
 * @ORM\Entity(repositoryClass="Eprst\Bundle\CurrencyBundle\Repository\CurrencyRate")
 */
class CurrencyRate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pair", type="string", length=6, nullable=false)
     */
    private $pair;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts", type="datetime", nullable=false)
     */
    private $ts;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", nullable=false)
     */
    private $value;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pair
     *
     * @param string $pair
     * @return CurrencyRate
     */
    public function setPair($pair)
    {
        $this->pair = $pair;
    
        return $this;
    }

    /**
     * Get pair
     *
     * @return string 
     */
    public function getPair()
    {
        return $this->pair;
    }

    /**
     * Set ts
     *
     * @param \DateTime $ts
     * @return CurrencyRate
     */
    public function setTs($ts)
    {
        $this->ts = $ts;
    
        return $this;
    }

    /**
     * Get ts
     *
     * @return \DateTime 
     */
    public function getTs()
    {
        return $this->ts;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return CurrencyRate
     */
    public function setValue($value)
    {
        $this->value = $value;
    
        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }
}