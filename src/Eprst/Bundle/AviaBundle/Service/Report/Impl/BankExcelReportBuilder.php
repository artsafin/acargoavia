<?php

namespace Eprst\Bundle\AviaBundle\Service\Report\Impl;
use DateTime;
use Eprst\Bundle\AviaBundle\Entity\Shipment as ShipmentEntity;
use Eprst\Bundle\AviaBundle\Service\EntityMapper;
use Eprst\Bundle\AviaBundle\Service\Report\BaseExcelReport;
use Liuggio\ExcelBundle\Service\ExcelContainer;
use PHPExcel_Cell;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Worksheet;
use PHPExcel_Worksheet_Drawing;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * ExcelReportBuilder
 * 
 */
class BankExcelReportBuilder extends BaseExcelReport
{
    const DEFAULT_FONT_SIZE = 9;

    private $logoPath;
    private $locale;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(EntityMapper $mapper,
                                ExcelContainer $excel,
                                $defaultStyles,
                                TranslatorInterface $translator,
                                $locale,
                                $logoPath)
    {
        parent::__construct($mapper, $excel, $defaultStyles);

        $this->translator = $translator;
        $this->locale = $locale;
        $this->logoPath = $logoPath;
    }

    private function _($msg, $params = array())
    {
        return $this->translator->trans($msg, $params, 'reports', $this->locale);
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     */
    private function buildProperties($dateFrom, $dateTo)
    {
        $this->getXls()->getProperties()
            ->setCreator("Aliance Cargo Reporting")
            ->setLastModifiedBy("Aliance Cargo Reporting")
            ->setTitle("Bank report {$dateFrom->format('d.m.Y')} - {$dateTo->format('d.m.Y')}")
            ->setDescription("Bank report {$dateFrom->format('d.m.Y')} - {$dateTo->format('d.m.Y')}")
        ;
    }

    protected function createFirstSheet()
    {
        $this->getXls()->removeSheetByIndex($this->getXls()->getActiveSheetIndex());
        $sheet = $this->getXls()->createSheet();
        $sheet->setTitle($this->_('Report'));

        $this->getXls()->setActiveSheetIndex($this->getXls()->getIndex($sheet));

        $pageSetup = new \PHPExcel_Worksheet_PageSetup();
        $pageSetup->setFitToWidth(1);
        $pageSetup->setFitToHeight(0);
        $pageSetup->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $this->getXls()->getActiveSheet()->setPageSetup($pageSetup);

        return $sheet;
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     */
    protected function buildHeader($sheet, $dateFrom, $dateTo)
    {
        $logo = new PHPExcel_Worksheet_Drawing();
        $logo->setPath($this->logoPath);
        $logo->setCoordinates('A1');
        $logo->setResizeProportional(true);
        $logo->setWidth(250);

        $logo->setWorksheet($sheet);

        $heading1Cell = $sheet->setCellValue('E3', $this->_('_heading1'), true);
        $sheet->getStyle($heading1Cell->getCoordinate())->getFont()->setBold(true)->setSize(self::DEFAULT_FONT_SIZE - 1);

        $heading2Cell = $sheet->setCellValue('A5', $this->_('_heading2', array(
                                                             '%dateFrom%' => $dateFrom->format('«d» F Y'),
                                                             '%dateTo%' => $dateTo->format('«d» F Y'),
                                                        )), true);
        $sheet->getStyle($heading2Cell->getCoordinate())->getFont()->setSize(self::DEFAULT_FONT_SIZE + 5);

        $tableHeaderRows = array(
            array(
                $this->_("O/n"),
                $this->_("Waybill"),
                $this->_("Waybill date"),
                $this->_("Source airport"),
                $this->_("Destination airport"),
                $this->_("Goods nature"),
                $this->_("Number of pieces"),
                $this->_("Gross weight"),
                $this->_("Carrier paid weight"),
                $this->_("Carrier rate"),
                $this->_("Carrier amount"),
                $this->_("Agent rebate"),
                $this->_("Dues"),
                $this->_("Carrier total"),
            ),
            array(
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '(8) х (9)',
                '(10) х 5%',
                '',
                '(10) +(12)'
            ),
            array(
                '',
                '(1)',
                '(2)',
                '(3)',
                '(4)',
                '(5)',
                '(6)',
                '(7)',
                '(8)',
                '(9)',
                '(10)',
                '(11)',
                '(12)',
                '(13)'
            )
        );

        $rowIndex = $sheet->getHighestRow() + 2;

        foreach ($tableHeaderRows as $rowDelta => $row)
        {
            foreach ($row as $columnIndex => $value)
            {
                $sheet->setCellValueByColumnAndRow($columnIndex, $rowIndex + $rowDelta, $value);
            }
        }

        $columnCoord = $sheet->getHighestColumn();
        $rowIndexSubHeader1 = $rowIndex + 1;
        $rowIndexSubHeader2 = $rowIndex + 2;

        /**
         * Set small font and center alignment:
         *
         *     ##########
         * ==> ##########
         * ==> ##########
         */
        $subheadersRange = "A{$rowIndexSubHeader1}:{$columnCoord}{$rowIndexSubHeader2}";
        $style = $sheet->getStyle($subheadersRange);
        $style->getFont()->setSize(self::DEFAULT_FONT_SIZE - 1);
        $style->getAlignment()
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
        ;

        /**
         * Add outline border for top header rows:
         *
         * ==> ##########
         * ==> ##########
         *     ##########
         */
        $topHeadersRange = "A{$rowIndex}:{$columnCoord}{$rowIndexSubHeader1}";
        $style = $sheet->getStyle($topHeadersRange);
        $style->getAlignment()
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $style->getAlignment()->setWrapText(true);
        $style->applyFromArray(array(
                                    'borders' => array(
                                        'outline' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                                        ),
                                        'vertical' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                                        ),
                                    )
                               ));

        /**
         * Add all possible border to the last header row:
         *
         *     ##########
         *     ##########
         * ==> ##########
         */
        $numerationHeaderRange = "A{$rowIndexSubHeader2}:{$columnCoord}{$rowIndexSubHeader2}";
        $style = $sheet->getStyle($numerationHeaderRange);
        $style->applyFromArray(array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                                        ),
                                    ),
                               ));
    }

    /**
     * @param PHPExcel_Worksheet   $sheet
     * @param ShipmentEntity[] $entities
     * @return array
     */
    protected function buildBody($sheet, $entities)
    {
        $totals = array();
        $rowIndex = $bodyBeginRowIndex = $sheet->getHighestRow() + 1;

        $totalableColumns = array(
            "Gross weight",
            "Carrier paid weight",
            "Carrier amount",
            "Agent rebate",
            "Dues",
            "Carrier total"
        );

        $totals = array_fill_keys($totalableColumns, 0);

        foreach ($entities as $rowDelta => $entity)
        {
            $rowValues = array(
                $rowDelta + 1,                                      // 0
                $this->mapper->get($entity, "Waybill"),             // 1
                $this->mapper->get($entity, "Waybill date"),        // 2
                $this->mapper->get($entity, "Source airport"),      // 3
                $this->mapper->get($entity, "Destination airport"), // 4
                $this->mapper->get($entity, "Goods nature"),        // 5
                $this->mapper->get($entity, "Number of pieces"),    // 6
                $this->mapper->get($entity, "Gross weight"),        // 7
                $this->mapper->get($entity, "Carrier paid weight"), // 8
                $this->mapper->get($entity, "Carrier rate"),        // 9
                $this->mapper->get($entity, "Carrier amount"),      // 10
                $this->mapper->get($entity, "Agent rebate"),        // 11
                $this->mapper->get($entity, "Dues"),                // 12
                $this->mapper->get($entity, "Carrier total"),       // 13
            );

            foreach ($totalableColumns as $name) {
                $totals[$name] += $this->mapper->get($entity, $name);
            }

            foreach ($rowValues as $columnIndex => $value)
                $sheet->setCellValueByColumnAndRow($columnIndex, $rowIndex + $rowDelta, $value);
        }

        $totals = array_map(function($item){
            return number_format($item, 2, '.', '');
        }, $totals);

        $rowIndex = $sheet->getHighestRow() + 1;

        $sheet->setCellValueByColumnAndRow(0, $rowIndex, $this->_('Total'));
        $sheet->mergeCellsByColumnAndRow(0, $rowIndex, 6, $rowIndex);
        $totalStyle = $sheet->getStyle("A{$rowIndex}");
        $totalStyle->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $totalStyle->getFont()->setBold(true);

        $sheet->setCellValueByColumnAndRow(7, $rowIndex, $totals["Gross weight"]);
        $sheet->setCellValueByColumnAndRow(8, $rowIndex, $totals["Carrier paid weight"]);
        $sheet->setCellValueByColumnAndRow(10, $rowIndex, $totals["Carrier amount"]);
        $sheet->setCellValueByColumnAndRow(11, $rowIndex, $totals["Agent rebate"]);
        $sheet->setCellValueByColumnAndRow(12, $rowIndex, $totals["Dues"]);
        $sheet->setCellValueByColumnAndRow(13, $rowIndex, $totals["Carrier total"]);

        $maxColumn = $sheet->getHighestColumn();
        $style = $sheet->getStyle("A{$bodyBeginRowIndex}:{$maxColumn}{$rowIndex}");
        $style->applyFromArray(array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                                        )
                                    )
                               ));

        return $totals;
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param array              $totals
     * @param float              $prevPeriodTotalAgentFee
     */
    protected function buildFooter($sheet, $totals, $prevPeriodTotalAgentFee)
    {
        $payable = $totals["Carrier total"];
        $total = $payable - $prevPeriodTotalAgentFee;

        $rowIndex = $footerBeginRowIndex = $sheet->getHighestRow() + 2;
        $sheet->setCellValue("B{$rowIndex}", $this->_('Payable'));
        $sheet->setCellValue("G{$rowIndex}", $payable);
        $sheet->setCellValue("H{$rowIndex}", 'USD');

        $rowIndex++;
        $sheet->setCellValue("B{$rowIndex}", $this->_('Agent fee deduction'));
        $sheet->setCellValue("G{$rowIndex}", $prevPeriodTotalAgentFee);
        $sheet->setCellValue("H{$rowIndex}", 'USD');

        $rowIndex++;
        $sheet->setCellValue("B{$rowIndex}", $this->_('Total payable'));
        $sheet->setCellValue("G{$rowIndex}", $total);
        $sheet->setCellValue("H{$rowIndex}", 'USD');

        $sheet->getStyle("B{$footerBeginRowIndex}:B{$rowIndex}")->getFont()->setBold(true);


        $rowIndex = $sheet->getHighestRow() + 2;

        $sheet->setCellValueByColumnAndRow(1, $rowIndex,
                                           $this->_('_footer_director_name').'_________________');
        $sheet->setCellValueByColumnAndRow(10, $rowIndex, $this->_('Approved'));

        $now = new DateTime();
        $sheet->setCellValueByColumnAndRow(1, $rowIndex + 1,
            $this->_('Date of creation %date%', array('%date%' => $now->format('d.m.Y')))
        );
        $sheet->setCellValueByColumnAndRow(10, $rowIndex + 1, $this->_('Dubai Aviation Corporation'));
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     */
    private function buildColumnSizes($sheet)
    {
        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(13);
        $sheet->getColumnDimension('C')->setWidth(10);
        $sheet->getColumnDimension('D')->setWidth(12);
        $sheet->getColumnDimension('E')->setWidth(12);
        $sheet->getColumnDimension('F')->setWidth(10);
        $sheet->getColumnDimension('G')->setWidth(10);
        $sheet->getColumnDimension('H')->setWidth(10);
        $sheet->getColumnDimension('I')->setWidth(10);
        $sheet->getColumnDimension('J')->setWidth(10);
        $sheet->getColumnDimension('K')->setWidth(10);
        $sheet->getColumnDimension('L')->setWidth(10);
        $sheet->getColumnDimension('M')->setWidth(10);
        $sheet->getColumnDimension('N')->setWidth(10);
    }

    /**
     * @param ShipmentEntity[] $entities
     * @param array            $reportMetaData
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function build($entities, $reportMetaData)
    {
        $dateFrom                = $reportMetaData['dateFrom'];
        $dateTo                  = $reportMetaData['dateTo'];
        $prevPeriodTotalAgentFee = $reportMetaData['prevPeriodTotalAgentFee'];

        $this->buildProperties($dateFrom, $dateTo);

        $sheet = $this->createFirstSheet();

        $this->buildHeader($sheet, $dateFrom, $dateTo);

        $totals = $this->buildBody($sheet, $entities);

        $this->buildFooter($sheet, $totals, $prevPeriodTotalAgentFee);

        $this->buildColumnSizes($sheet);

        $response = $this->excel->getResponse();

        return $response;
    }
}
