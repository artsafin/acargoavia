<?php

namespace Eprst\Bundle\AviaBundle\Service\Report\Impl;
use DateTime;
use Eprst\Bundle\AviaBundle\Entity\Shipment as ShipmentEntity;
use Eprst\Bundle\AviaBundle\Service\EntityMapper;
use Eprst\Bundle\AviaBundle\Service\Report\BaseExcelReport;
use Liuggio\ExcelBundle\Service\ExcelContainer;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Worksheet;
use PHPExcel_Worksheet_Drawing;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * AgentExcelReportBuilder
 * 
 */
class AgentExcelReportBuilder extends BaseExcelReport
{
    private $locale;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * signPath
     *
     * @var string
     */
    private $signPath;

    public function __construct(EntityMapper $mapper,
                                ExcelContainer $excel,
                                $defaultStyles,
                                TranslatorInterface $translator,
                                $locale,
                                $signPath)
    {
        parent::__construct($mapper, $excel, $defaultStyles);

        $this->translator = $translator;
        $this->locale = $locale;
        $this->signPath = $signPath;
    }

    private function _($msg, $params = array())
    {
        return $this->translator->trans($msg, $params, 'reports', $this->locale);
    }

    /**
     * @param DateTime $dateFrom
     * @param DateTime $dateTo
     */
    private function buildProperties($dateFrom, $dateTo)
    {
        $this->getXls()->getProperties()
            ->setCreator("Aliance Cargo Reporting")
            ->setLastModifiedBy("Aliance Cargo Reporting")
            ->setTitle("Agent report {$dateFrom->format('d.m.Y')} - {$dateTo->format('d.m.Y')}")
            ->setDescription("Agent report {$dateFrom->format('d.m.Y')} - {$dateTo->format('d.m.Y')}")
        ;
    }

    protected function createFirstSheet()
    {
        $this->getXls()->removeSheetByIndex($this->getXls()->getActiveSheetIndex());
        $sheet = $this->getXls()->createSheet();
        $sheet->setTitle($this->_('Report'));

        $this->getXls()->setActiveSheetIndex($this->getXls()->getIndex($sheet));

        $pageSetup = new \PHPExcel_Worksheet_PageSetup();
        $pageSetup->setFitToWidth(1);
        $pageSetup->setFitToHeight(0);
        $pageSetup->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $this->getXls()->getActiveSheet()->setPageSetup($pageSetup);

        return $sheet;
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param                    $agentName
     * @param                    $currencyRate
     * @param DateTime           $dateFrom
     * @param DateTime           $dateTo
     */
    protected function buildHeader($sheet, $agentName, $currencyRate, $dateFrom, $dateTo)
    {
        $heading2Cell = $sheet->setCellValue('A2', $this->_('_heading_agent_main', array(
                                                             '%agent%' => $agentName,
                                                             '%rate%' => $currencyRate,
                                                             '%dateFrom%' => $dateFrom->format('«d» F Y'),
                                                             '%dateTo%' => $dateTo->format('«d» F Y'),
                                                        )), true);
        $sheet->getStyle($heading2Cell->getCoordinate())->getFont()->setSize(14);

        $tableHeaderRows = array(
            array(
                $this->_("O/n"),
                $this->_("Waybill"),
                $this->_("Waybill date"),
                $this->_("Departure date"),
                $this->_("Source airport"),
                $this->_("Destination airport"),
                $this->_("Gross weight"),
                $this->_("Agent paid weight"),
                $this->_("Agent rate"),
                $this->_("Agent amount"),
                $this->_("Dues"),
                $this->_("Agent total"),
                $this->_("Agent total RUB"),
            )
        );

        $rowIndex = $sheet->getHighestRow() + 2;

        foreach ($tableHeaderRows as $rowDelta => $row)
        {
            foreach ($row as $columnIndex => $value)
            {
                $sheet->setCellValueByColumnAndRow($columnIndex, $rowIndex + $rowDelta, $value);
            }
        }
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param ShipmentEntity[]   $entities
     * @param                    $currencyRate
     *
     * @return array
     */
    protected function buildBody($sheet, $entities, $currencyRate)
    {
        $totals = array();
        $rowIndex = $bodyBeginRowIndex = $sheet->getHighestRow() + 1;

        foreach ($entities as $rowDelta => $entity)
        {
            $rowValues = array(
                $rowDelta + 1,
                $this->mapper->get($entity, "Waybill"),
                $this->mapper->get($entity, "Waybill date"),
                $this->mapper->get($entity, "Departure date"),
                $this->mapper->get($entity, "Source airport"),
                $this->mapper->get($entity, "Destination airport"),
                $this->mapper->get($entity, "Gross weight"),
                $this->mapper->get($entity, "Agent paid weight"),
                $this->mapper->get($entity, "Agent rate"),
                $this->mapper->get($entity, "Agent amount"),
                $this->mapper->get($entity, "Dues"),
                $this->mapper->get($entity, "Agent total"),
                number_format($this->mapper->get($entity, "Agent total conv", $currencyRate), 2, '.', ''),
            );

            if (empty($totals))
                $totals = $rowValues;
            else
            {
                $totals[11] += $rowValues[11];
                $totals[12] += $rowValues[12];
            }

            foreach ($rowValues as $columnIndex => $value)
                $sheet->setCellValueByColumnAndRow($columnIndex, $rowIndex + $rowDelta, $value);
        }

        $rowIndex = $sheet->getHighestRow() + 1;

        $sheet->setCellValueByColumnAndRow(0, $rowIndex, $this->_('Total'));
        $sheet->mergeCellsByColumnAndRow(0, $rowIndex, 8, $rowIndex);
        $totalStyle = $sheet->getStyle("A{$rowIndex}");
        $totalStyle->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $totalStyle->getFont()->setBold(true);

        $sheet->setCellValueByColumnAndRow(11, $rowIndex, isset($totals[11]) ? $totals[11] : 0);
        $sheet->setCellValueByColumnAndRow(12, $rowIndex, isset($totals[12]) ? $totals[12] : 0);

        $maxColumn = $sheet->getHighestColumn();
        $style = $sheet->getStyle("A{$bodyBeginRowIndex}:{$maxColumn}{$rowIndex}");
        $style->applyFromArray(array(
                                    'borders' => array(
                                        'allborders' => array(
                                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                                        )
                                    )
                               ));

        return $totals;
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param array              $totals
     * @param \DateTime          $dateTo
     * @param                    $isSigned
     */
    protected function buildFooter($sheet, $totals, $dateTo, $isSigned)
    {
        if ($isSigned) {
            $signRow = $sheet->getHighestRow() + 2;

            $sheet->setCellValue("J{$signRow}", "Отчет утвержден {$dateTo->format('d.m.Y')}");

            $logo = new PHPExcel_Worksheet_Drawing();
            $logo->setPath($this->signPath);
            $logo->setCoordinates('K'. ($signRow + 1));
            $logo->setResizeProportional(true);
            $logo->setWidth(150);

            $logo->setWorksheet($sheet);
        }
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     */
    private function buildColumnSizes($sheet)
    {
        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(13);
        $sheet->getColumnDimension('C')->setWidth(10);
        $sheet->getColumnDimension('D')->setWidth(12);
        $sheet->getColumnDimension('E')->setWidth(12);
        $sheet->getColumnDimension('F')->setWidth(10);
        $sheet->getColumnDimension('G')->setWidth(10);
        $sheet->getColumnDimension('H')->setWidth(10);
        $sheet->getColumnDimension('I')->setWidth(10);
        $sheet->getColumnDimension('J')->setWidth(10);
        $sheet->getColumnDimension('K')->setWidth(10);
        $sheet->getColumnDimension('L')->setWidth(10);
        $sheet->getColumnDimension('M')->setWidth(10);
        $sheet->getColumnDimension('N')->setWidth(10);
    }

    /**
     * @param ShipmentEntity[] $entities
     * @param array            $reportMetaData
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function build($entities, $reportMetaData)
    {
        $agentName    = $reportMetaData['agentName'];
        $currencyRate = $reportMetaData['currencyRate'];
        $dateFrom     = $reportMetaData['dateFrom'];
        $dateTo       = $reportMetaData['dateTo'];
        $isSigned     = isset($reportMetaData['isSigned'])? $reportMetaData['isSigned'] : false;

        $this->buildProperties($dateFrom, $dateTo);

        $sheet = $this->createFirstSheet();

        $this->buildHeader($sheet, $agentName, $currencyRate, $dateFrom, $dateTo);

        $totals = $this->buildBody($sheet, $entities, $currencyRate);

        $this->buildFooter($sheet, $totals, $dateTo, $isSigned);

        $this->buildColumnSizes($sheet);

        $response = $this->excel->getResponse();

        return $response;
    }
}
