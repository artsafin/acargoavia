<?php
/**
 * Global Trading Technologies Ltd.
 *
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of
 * this source code is governed by the Global Trading Technologies Ltd. Non-Disclosure
 * Agreement previously entered between you and Global Trading Technologies Ltd.
 *
 * By accessing, using, copying, modifying or distributing this
 * software, you acknowledge that you have been informed of your
 * obligations under the Agreement and agree to abide by those obligations.
 */

namespace Eprst\Bundle\AviaBundle\Service\Report;
use Eprst\Bundle\AviaBundle\Service\EntityMapper;
use Liuggio\ExcelBundle\Service\ExcelContainer;
use PHPExcel;

/**
 * BaseExcelReport
 */
abstract class BaseExcelReport implements ReportResponseBuilderInterface
{
    /**
     * @var ExcelContainer
     */
    protected $excel;

    /**
     * mapper
     *
     * @var \Eprst\Bundle\AviaBundle\Service\EntityMapper
     */
    protected $mapper;

    public function __construct(EntityMapper $mapper,
                                ExcelContainer $excel,
                                $defaultStyles)
    {
        $this->excel = $excel;
        $this->mapper = $mapper;

        if (is_array($defaultStyles) && !empty($defaultStyles)) {
            $this->getXls()->getDefaultStyle()->applyFromArray($defaultStyles);
        }
    }

    /**
     * @return PHPExcel
     */
    protected function getXls()
    {
        return $this->excel->getExcelObj();
    }
}
