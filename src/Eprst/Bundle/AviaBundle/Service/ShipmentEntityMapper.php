<?php
/**
 * Global Trading Technologies Ltd.
 *
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of
 * this source code is governed by the Global Trading Technologies Ltd. Non-Disclosure
 * Agreement previously entered between you and Global Trading Technologies Ltd.
 *
 * By accessing, using, copying, modifying or distributing this
 * software, you acknowledge that you have been informed of your
 * obligations under the Agreement and agree to abide by those obligations.
 */

namespace Eprst\Bundle\AviaBundle\Service;
use Eprst\Bundle\AviaBundle\Entity\Shipment as ShipmentEntity;

/**
 * ShipmentEntityMapper
 */
class ShipmentEntityMapper implements EntityMapper
{
    protected $map = array();

    public function __construct()
    {
        /** @var ShipmentEntity $entity */

        $mapper = $this;

        $this->map = array(
            "Waybill"             => function ($entity) {
                return sprintf('%s-%s', $entity->getWaybillPrefix(), $entity->getWaybill());
            },
            "Waybill date"        => function ($entity) {
                $date = $entity->getDateWaybill();

                return $date ? $date->format('d.m.Y') : '';
            },
            "Source airport"      => function ($entity) {
                $station = $entity->getSrcStation();

                return $station ? $station->getName() : '(n/a)';
            },
            "Destination airport" => function ($entity) {
                $station = $entity->getDstStation();

                return $station ? $station->getName() : '(n/a)';
            },
            "Goods nature"        => function ($entity) {
                return $entity->getGoodsNatureCode();
            },
            "Number of pieces"    => function ($entity) {
                return $entity->getNumPlaces();
            },
            "Gross weight"        => function ($entity) {
                return $entity->getGrossWeight();
            },
            "Carrier paid weight"         => function ($entity) {
                return $entity->getChargeableWeightCarrier();
            },
            "Agent paid weight"         => function ($entity) {
                return $entity->getChargeableWeightAgent();
            },
            "Carrier rate"                => function ($entity) {
                return $entity->getRateCarrier();
            },
            "Agent rate"                => function ($entity) {
                return $entity->getRateAgent();
            },
            "Carrier amount"              => function ($entity) {
                return round($entity->getCarrierRatePay(), 2);
            },
            "Agent amount"              => function ($entity) {
                return round($entity->getAgentRatePay(), 2);
            },
            "Agent amount conv"              => function ($entity, $rate) {
                return round($entity->getAgentRatePay() * $rate, 2);
            },
            "Carrier agent fee"           => function ($entity) {
                return round($entity->getCarrierRatePay() * 0.05, 2);
            },
            "Agent rebate"           => function ($entity) {
                return round($entity->getAgentRebate(), 2);
            },
            "Dues"            => function ($entity) {
                return round($entity->getTotalFee(), 2);
            },
            "Dues conv"       => function ($entity, $rate) use ($mapper) {
                return $mapper->get($entity, 'Fuel charge conv', $rate)
                       + $mapper->get($entity, 'Security charge conv', $rate);
            },
            "Fuel charge conv"       => function ($entity, $rate) {
                return round($rate * ($entity->getFee()->getMyc() + $entity->getFee()->getOpt()), 2);
            },
            "Security charge conv"       => function ($entity, $rate) {
                return round($rate * $entity->getFee()->getXdc(), 2);
            },
            "Carrier total"           => function ($entity) {
                return round($entity->getTotalFee() + $entity->getCarrierRatePay(), 2);
            },
            "Agent total"           => function ($entity) {
                return round($entity->getTotalFee() + $entity->getAgentRatePay(), 2);
            },
            "Agent total conv"      => function ($entity, $rate) use($mapper) {
                return $mapper->get($entity, "Dues conv", $rate)
                       + $mapper->get($entity, 'Agent amount conv', $rate);
            },
            "Volume"           => function ($entity) {
                return $entity->getVolume();
            },
            "Departure date"           => function ($entity) {
                return $entity->getDateFactDeparture() ? $entity->getDateFactDeparture()->format('d.m.Y') : '';
            },
        );
    }

    /**
     * get
     *
     * @param ShipmentEntity $entity
     * @param string         $key
     *
     * @throws \InvalidArgumentException
     * @return mixed
     */
    public function get($entity, $key)
    {
        if (isset($this->map[$key])) {
            $fn = $this->map[$key];
            if (is_callable($fn)) {
                $args = func_get_args();
                unset($args[1]);
                return call_user_func_array($fn, array_merge($args));
            } else if (is_scalar($fn)) {
                return $fn;
            }
        }

        $getterMethod = sprintf('get%s', ucfirst($key));
        if (method_exists($entity, $getterMethod)) {
            return (string) $entity->{$getterMethod}();
        }

        $type = get_class($entity);
        throw new \InvalidArgumentException("Unable to get key '{$key}' from object of type {$type}");
    }
}
