<?php

namespace Eprst\Bundle\AviaBundle\Service;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Eprst\Bundle\AviaBundle\Entity\CarrierFee as CarrierFeeEntity;

/**
 * FeeCalculator
 * 
 * @author Artur.Safin
 * @date 27.07.13 12:49
 */
class FeeCalculator
{
    /**
     * @var ObjectManager
     */
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function calculate($carrier, $weight)
    {
        /** @var ObjectRepository $repo */
        $repo = $this->em->getRepository('EprstAviaBundle:CarrierFee');

        /** @var CarrierFeeEntity $feeEntity */
        $feeEntity = $repo->findOneBy(array(
                              'carrier' => $carrier
                         ));

        return array(
            'myc' => $feeEntity->getMyc() * $weight,
            'xdc' => $feeEntity->getXdc() * $weight,
        );
    }
}
