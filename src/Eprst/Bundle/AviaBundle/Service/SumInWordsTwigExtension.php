<?php
/**
 * Global Trading Technologies Ltd.
 *
 * The following source code is PROPRIETARY AND CONFIDENTIAL. Use of
 * this source code is governed by the Global Trading Technologies Ltd. Non-Disclosure
 * Agreement previously entered between you and Global Trading Technologies Ltd.
 *
 * By accessing, using, copying, modifying or distributing this
 * software, you acknowledge that you have been informed of your
 * obligations under the Agreement and agree to abide by those obligations.
 */

namespace Eprst\Bundle\AviaBundle\Service;
use Twig_Extension;
use Twig_SimpleFunction;

/**
 * SumInWordsTwigExtension
 */
class SumInWordsTwigExtension extends Twig_Extension
{
    /**
     * Service
     *
     * @var SumInWordsService
     */
    private $service;

    public function __construct($service)
    {
        $this->service = $service;
    }

    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('sum_in_words', array($this->service, 'num2str')),
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'sum_in_words_extension';
    }
}
