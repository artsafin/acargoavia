<?php

namespace Eprst\Bundle\AviaBundle\Service;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use Eprst\Bundle\AviaBundle\Entity\AviaRate as AviaRateEntity;
use Eprst\Bundle\AviaBundle\Repository\AviaRate as AviaRateRepository;
use Eprst\Bundle\AviaBundle\Repository\AviaRateType as AviaRateTypeRepository;

/**
 * RateCalculator
 *
 * @author Artur.Safin
 * @date   25.07.13 23:31
 */
class RateCalculator
{
    const STRATEGY_MIN    = 'min';
    const STRATEGY_NORMAL = 'n';
    const STRATEGY_45     = 'range45';
    const STRATEGY_100    = 'range100';
    const STRATEGY_300    = 'range300';
    const STRATEGY_500    = 'range500';
    const STRATEGY_1000   = 'range1000';
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    private function loadStation($stationName)
    {
        /** @var EntityRepository $stationRepo */
        $stationRepo = $this->em->getRepository('EprstAviaBundle:Station');
        return $stationRepo->findOneByName($stationName);
    }

    public function calculateCarrier($paidWeight, $dstStationName)
    {
        $station = $this->loadStation($dstStationName);

        /** @var AviaRateRepository $rateRepo */
        $rateRepo = $this->em->getRepository('EprstAviaBundle:AviaRate');

        $carrierRate = $rateRepo->getRate(AviaRateTypeRepository::TYPE_CARRIER, $station);

        if ($carrierRate) {
            $carrierStrategy = $this->getCalculationStrategy($paidWeight, $carrierRate->getMin(), $carrierRate->getN());
            $carrierRate     = $this->getRate($carrierStrategy, $carrierRate);
        }
        else {
            $carrierStrategy = $this->getCalculationStrategy($paidWeight, 0, 0);
            $carrierRate = 0;
        }

        return array(
            'strategy' => $carrierStrategy,
            'rate'     => $carrierRate,
            'pay'      => ($carrierStrategy == self::STRATEGY_MIN) ? $carrierRate : ($carrierRate * $paidWeight)
        );
    }

    public function calculateAgent($paidWeight, $dstStationName)
    {
        $station = $this->loadStation($dstStationName);

        /** @var AviaRateRepository $rateRepo */
        $rateRepo = $this->em->getRepository('EprstAviaBundle:AviaRate');

        $agentRate = $rateRepo->getRate(AviaRateTypeRepository::TYPE_AGENT, $station);

        if ($agentRate) {
            $agentStrategy = $this->getCalculationStrategy($paidWeight, $agentRate->getMin(), $agentRate->getN());
            $agentRate     = $this->getRate($agentStrategy, $agentRate);
        }
        else {
            $agentStrategy = $this->getCalculationStrategy($paidWeight, 0, 0);
            $agentRate = 0;
        }

        return array(
            'strategy' => $agentStrategy,
            'rate'     => $agentRate,
            'pay'      => ($agentStrategy == self::STRATEGY_MIN) ? $agentRate : ($agentRate * $paidWeight)
        );
    }

    private function getCalculationStrategy($paidWeight, $min, $n)
    {
	    $min = (float) $min;
	    $n = (float) $n;

        if (!$min || !$n)
            $minWeight = 0;
        else
            $minWeight = (int)($min / $n); // no rounding, just throw fractional part

        if ($paidWeight < $minWeight) {
            $tariffMethod = self::STRATEGY_MIN;
        } else if ($paidWeight >= $minWeight && $paidWeight < 45) {
            $tariffMethod = self::STRATEGY_NORMAL;
        } else if ($paidWeight >= 45 && $paidWeight < 100) {
            $tariffMethod = self::STRATEGY_45;
        } else if ($paidWeight >= 100 && $paidWeight < 300) {
            $tariffMethod = self::STRATEGY_100;
        } else if ($paidWeight >= 300 && $paidWeight < 500) {
            $tariffMethod = self::STRATEGY_300;
        } else if ($paidWeight >= 500 && $paidWeight < 1000) {
            $tariffMethod = self::STRATEGY_500;
        } else {
            $tariffMethod = self::STRATEGY_1000;
        }

        return $tariffMethod;
    }

    private function getRate($tariffMethod, AviaRateEntity $entity)
    {
        return floatval($entity->{'get' . ucfirst($tariffMethod)}());
    }
}
