Ext.define('Eprst.AviaRate.RateStore', {
	extend: 'Ext.data.Store',

	model: 'Eprst.AviaRate.RateModel',
	autoLoad: true,
//	autoSync: true,
	autoDestroy: true,
	proxy: {
		type: 'ajax',
		api: {
			create: Routing.generate('avia_rates_crud_create', {type: rateType}),
			read: Routing.generate('avia_rates_crud_read', {type: rateType}),
			update: Routing.generate('avia_rates_crud_update', {type: rateType}),
			destroy: Routing.generate('avia_rates_crud_destroy', {type: rateType})
		},
		reader: {
			type: 'json',
			root: 'root'
		},
		writer: {
			type: 'json',
			allowSingle: false
		}
	}
});