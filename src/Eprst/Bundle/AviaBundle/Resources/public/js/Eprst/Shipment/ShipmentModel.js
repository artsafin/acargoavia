Ext.define('Eprst.Shipment.ShipmentModel', {
	extend: 'Ext.data.Model',

	fields: [
		'numPieces',
		'weight',
		'totalWeight',
		'length',
		'width',
		'height',
		'volume',
		'volumeWeight'
	]
});