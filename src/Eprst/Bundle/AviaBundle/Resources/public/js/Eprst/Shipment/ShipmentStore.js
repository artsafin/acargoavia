Ext.define('Eprst.Shipment.ShipmentStore', {
	extend: 'Ext.data.Store',

	model: 'Eprst.Shipment.ShipmentModel',
	proxy: {
		type: 'memory'
	},

	constructor: function(config){
		var me = this;

		config = Ext.apply({
			listeners: {
				update: me.onUpdateHandler,
				remove: me.onRemoveHandler
			}
		}, config);

		this.callParent([config]);
	},

	//Fires when a Model instance has been updated.
	onUpdateHandler: function(store, model, operation, modifiedFieldNames){

		// Convert plain array to object indexed by array value. That is to be able to do: 'somefield' in modified
		var modified = modifiedFieldNames.reduce(function (acc, i) { acc[i] = i; return acc; }, {});

		if ('numPieces' in modified || 'weight' in modified)
		{
			var totalWeight = model.get('numPieces') * model.get('weight');
			model.set('totalWeight', totalWeight);
		}

		if ('numPieces' in modified || 'length' in modified || 'height' in modified || 'width' in modified)
		{
			var volumeValueCm = model.get('numPieces') * model.get('length') * model.get('height') * model.get('width');
			var centimetersToMeters = 1 / 100 / 100 / 100;

			model.set('volume', volumeValueCm * centimetersToMeters);

			model.set('volumeWeight', volumeValueCm / 6000);
		}

		if (!store.skipAggregateEvent
				&& ('volume' in modified || 'totalWeight' in modified || 'volumeWeight' in modified))
		{
			var values = store.getCurrentStoreTotals(store);
			store.raiseTotalsValueChangedEvent(store, modified, values);
		}
	},

	getCurrentStoreTotals: function(store){
		var ret = {};

		ret.volume = roundToPrecision(adaptFloat(store.sum('volume')), 6);
		ret.totalWeight = roundToPrecision(adaptFloat(store.sum('totalWeight')), 2);
		ret.volumeWeight = roundToPrecision(adaptFloat(store.sum('volumeWeight')), 2);

		return ret;
	},

	onRemoveHandler: function(store){
		var values = store.getCurrentStoreTotals(store);
		store.raiseTotalsValueChangedEvent(store, values, values);
	},

	raiseTotalsValueChangedEvent: function(store, modified, values){
		store.fireEvent('totalvalueschanged', modified, values);
	},

	calculateDerivativeValues: function(){
		var me = this, range = me.getRange();
		for (var i = 0, len = range.length; i < len; i++)
		{
			me.skipAggregateEvent = true;
			me.fireEvent('update', me, range[i], null,
					[
						'numPieces',
						'weight',
						'length',
						'height',
						'width'
					]
			);
			me.skipAggregateEvent = false;
		}
	}

});