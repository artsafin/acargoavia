Ext.define('Eprst.AviaRate.RateGrid', {
	extend: 'Ext.grid.Panel',

	initComponent: function(){

		var me = this;

		me.cellEditing = new Ext.grid.plugin.CellEditing({
			clicksToEdit: 1,
			listeners: {
				beforeedit: function(plugin, e, field, value){
					console.log('beforedit', plugin, e, field, value);

					if (e.field == "arrivalStationName") {
						return e.record.phantom;
					}
				}
			}
		});

		var numberFieldEditor = {
			xtype: 'numberfield',
			allowExponential: false,
			baseChars: '0123456789.',
			decimalSeparator: ',',
			submitLocaleSeparator: false,
			decimalPrecision: 2,
			minValue: 0,
			selectOnFocus: true
		};

		Ext.apply(this, {
			layout: 'fit',
			plugins: [me.cellEditing],
			selModel: {
				selType: 'cellmodel'
			},
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							text: _('Add'),
							cls: 'btn',
							handler: function ()
							{
								me.onRecordAdd();
							}
						}
					]
				}
			],
			columns: [
				{
					text: _('Destination'),
					dataIndex: 'arrivalStationName',
					editor: {
						xtype: 'textfield'
					}
				},
				{
					text: _('Min'),
					dataIndex: 'min',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: numberFieldEditor
				},
				{
					text: _('Normal'),
					dataIndex: 'n',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: numberFieldEditor
				},
				{
					text: _('45'),
					dataIndex: 'range45',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: numberFieldEditor
				},
				{
					text: _('100'),
					dataIndex: 'range100',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: numberFieldEditor
				},
				{
					text: _('300'),
					dataIndex: 'range300',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: numberFieldEditor
				},
				{
					text: _('500'),
					dataIndex: 'range500',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: numberFieldEditor
				},
				{
					text: _('1000+'),
					dataIndex: 'range1000',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: numberFieldEditor
				},
				{
					menuDisabled: true,
					sortable: false,
					xtype: 'actioncolumn',
					width: 50,
					items: [
						{
							iconCls: 'icon-plus',
							altText: 'add',
							text: 'plus',
							tooltip: _('Add'),
							handler: function (grid, rowIndex, colIndex)
							{
								me.onRecordAdd();
							}
						}
					]
				},
				{
					menuDisabled: true,
					sortable: false,
					xtype: 'actioncolumn',
					width: 50,
					items: [
						{
							iconCls: 'icon-trash',
							altText: 'remove',
							text: 'trash',
							tooltip: _('Remove'),
							handler: function (grid, rowIndex, colIndex)
							{
								grid.getStore().removeAt(rowIndex);
							}
						}
					]
				}
			]
		});

		this.callParent();
	},

	onRecordAdd: function(){
		var me = this;

		var rec = Ext.create('Eprst.AviaRate.RateModel', {});

		me.getStore().add(rec);
		me.cellEditing.startEdit(rec, 0);
	}
});