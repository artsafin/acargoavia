Ext.define('Eprst.Shipment.PiecesGrid', {
	extend: 'Ext.grid.Panel',

	initComponent: function(){

		var me = this;

		me.cellEditing = new Ext.grid.plugin.CellEditing({
			clicksToEdit: 1
		});

		Ext.apply(this, {
			layout: 'fit',
			plugins: [me.cellEditing],
			selModel: {
				selType: 'cellmodel'
			},
			features: [
				{
					ftype: 'summary'
				}
			],
			dockedItems: [
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							text: _('Add'),
							cls: 'btn',
							handler: function ()
							{
								me.onShipmentAdd();
							}
						}
					]
				}
			],
			columns: [
				{
					text: _('Number of pieces'),
					dataIndex: 'numPieces',
					xtype: 'numbercolumn',
					format: '0',
					editor: {
						xtype: 'numberfield',
						allowExponential: false,
						allowDecimals: false,
						minValue: 1,
						selectOnFocus: true
					},
					summaryType: 'sum',
					width: 100
				},
				{
					text: _('Weight, kg'),
					dataIndex: 'weight',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: {
						xtype: 'numberfield',
						allowExponential: false,
						baseChars: '0123456789.',
						decimalSeparator: ',',
						submitLocaleSeparator: false,
						decimalPrecision: 2,
						minValue: 0,
						selectOnFocus: true
					}
				},
				{
					text: _('Length, cm'),
					dataIndex: 'length',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: {
						xtype: 'numberfield',
						allowExponential: false,
						baseChars: '0123456789.',
						decimalSeparator: ',',
						submitLocaleSeparator: false,
						decimalPrecision: 2,
						minValue: 0,
						selectOnFocus: true
					}
				},
				{
					text: _('Width, cm'),
					dataIndex: 'width',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: {
						xtype: 'numberfield',
						allowExponential: false,
						baseChars: '0123456789.',
						decimalSeparator: ',',
						submitLocaleSeparator: false,
						decimalPrecision: 2,
						minValue: 0,
						selectOnFocus: true
					}
				},
				{
					text: _('Height, cm'),
					dataIndex: 'height',
					xtype: 'numbercolumn',
					format: '0.00',
					editor: {
						xtype: 'numberfield',
						allowExponential: false,
						baseChars: '0123456789.',
						decimalSeparator: ',',
						submitLocaleSeparator: false,
						decimalPrecision: 2,
						minValue: 0,
						selectOnFocus: true
					}
				},
				{
					text: _('Volume, cbm'),
					dataIndex: 'volume',
					xtype: 'numbercolumn',
					format: '0.000000',
					summaryType: 'sum',
					width: 110
				},
				{
					text: _('Total weight, kg'),
					dataIndex: 'totalWeight',
					xtype: 'numbercolumn',
					format: '0.00',
					summaryType: 'sum',
					width: 150
				},
				{
					text: _('Volume weight, kg'),
					dataIndex: 'volumeWeight',
					xtype: 'numbercolumn',
					format: '0.000',
					summaryType: 'sum',
					width: 150,
					summaryRenderer: function (value, summaryData, dataIndex)
					{
						return roundToPrecision(adaptFloat(value), 2);
					}
				},
				{
					menuDisabled: true,
					sortable: false,
					xtype: 'actioncolumn',
					width: 50,
					items: [
						{
							iconCls: 'icon-plus',
							altText: 'add',
							tooltip: _('Add'),
							handler: function (grid, rowIndex, colIndex)
							{
								me.onShipmentAdd();
							}
						}
					]
				},
				{
					menuDisabled: true,
					sortable: false,
					xtype: 'actioncolumn',
					width: 50,
					items: [
						{
							iconCls: 'icon-trash',
							altText: 'remove',
							tooltip: _('Remove'),
							handler: function (grid, rowIndex, colIndex)
							{
								grid.getStore().removeAt(rowIndex);
							}
						}
					]
				}
			]
		});

		this.callParent();
	},

	onShipmentAdd: function(){
		var me = this;

		var rec = Ext.create('Eprst.Shipment.ShipmentModel', {
			numPieces: 0,
			weight: 0,
			totalWeight: 0,
			length: 0,
			width: 0,
			height: 0,
			volume: 0,
			volumeWeight: 0
		});

		me.getStore().add(rec);
		me.cellEditing.startEdit(rec, 0);
	}
});