Ext.define('Eprst.AviaRate.RateModel', {
	extend: 'Ext.data.Model',

	fields: [
		'id',
		'arrivalStationId',
		'arrivalStationName',
		'min',
		'n',
		'range45',
		'range100',
		'range300',
		'range500',
		'range1000'
	]
});