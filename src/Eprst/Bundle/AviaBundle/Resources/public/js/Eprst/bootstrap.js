Ext.onReady(function ()
{
	Ext.Loader.setConfig({
		enabled: true,
		paths: {
			'Ext': '/bundles/senchaextjs/extjs/src',
			'Eprst': '/bundles/eprstavia/js/Eprst'
		}
	});

	Ext.require([
		'Ext.grid.*',
		'Ext.data.*'
	]);
});

var _ = Translator.get;

var adaptFloat = function (val)
{
	val = ('' + val).replace(',', '.');
	var ret = parseFloat(val);
	return isNaN(ret) ? 0 : ret;
};

var roundToPrecision = function(value, precision)
{
	return Math.round(value * Math.pow(10, precision)) / Math.pow(10, precision);
};

var roundWithBorder = function(value, border){
	if (Math.floor(value) == value) {
		return value;
	} else if (Math.floor(value) + border - value >= 0) {
		return Math.floor(value) + border;
	} else {
		return Math.ceil(value);
	}
};