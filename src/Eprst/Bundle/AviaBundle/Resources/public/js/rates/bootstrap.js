Ext.onReady(function(){

	Ext.require([
		'Eprst.AviaRate.RateModel',
		'Eprst.AviaRate.RateStore',
		'Eprst.AviaRate.RateGrid'
	]);

	var saveButton = $('#eprstaviabundle_rates_grid_save');

	var store = Ext.create('Eprst.AviaRate.RateStore', {
		listeners: {
			update: function(cmp){
				var hasModified = cmp.getModifiedRecords().length;

				console.log('hasModified', hasModified);

				if (hasModified)
					saveButton.removeAttr('disabled');
				else
					saveButton.attr('disabled', 'disabled');
			}
		}
	});

	var grid = Ext.create('Eprst.AviaRate.RateGrid', {
		renderTo: 'eprstaviabundle_rates_grid',
		store: store
	});

	saveButton.click(function(){
//		var gridElDom = grid.getEl().dom;
//		gridElDom.mask(_('Loading'));

		store.sync({
			success: function(batch, options){
//				gridElDom.unmask();
			},
			failure: function(batch, options){
//				gridElDom.unmask();
				console.log('store sync failure', arguments);
				Ext.Msg.alert('There was an error saving this rate. Please check that station is not duplicated.');
			}
		});
	});

});