var Total = function(producerFields, consumerFields){
	this.producerFields = producerFields;
	this.consumerFields = consumerFields;

	var me = this;

	$.each(me.producerFields, function (index, el)
	{
		el.on('change', function (e)
		{
			me.calculate(e);
		});
	});
};

Total.prototype.init = function(){
};

Total.prototype.calculate = function(e){
	var me = this;

	var agentRatePay = adaptFloat(me.producerFields.agentRatePay.val());

	var carrierRatePay = adaptFloat(me.producerFields.carrierRatePay.val());

	var totalFee = adaptFloat(me.producerFields.totalFee.val());

//	console.log('Total calculate', agentRatePay, carrierRatePay, totalFee);

	me.consumerFields.totalAgent.val(roundToPrecision(adaptFloat(agentRatePay + totalFee), 2));

	me.consumerFields.totalCarrier.val(roundToPrecision(adaptFloat(carrierRatePay + totalFee), 2));
};