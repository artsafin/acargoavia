var Mask = function(coveredElement){
	this.coveredElement = coveredElement;

	var me = this;
};

Mask.prototype.show = function(){
	var el = this.coveredElement;
	var loadingCount = el.data('loadingCount') || 0;

	el.data('loadingCount', loadingCount + 1);

	Ext.fly(el.attr('id')).mask(_('Loading'));

	setTimeout(this.forceHide.bind(this), 35000);
};

Mask.prototype.hide = function(){
	var el = this.coveredElement;
	var loadingCount = el.data('loadingCount');

	if (loadingCount <= 1)
	{
		this.forceHide();
	}
	else
	{
		el.data('loadingCount', loadingCount - 1);
	}
};

Mask.prototype.forceHide = function(){
	var el = this.coveredElement;
	Ext.fly(el.attr('id')).unmask();
	el.data('loadingCount', 0);
};