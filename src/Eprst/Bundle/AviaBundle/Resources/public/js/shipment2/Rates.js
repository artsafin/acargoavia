var Rates = function(rateCalcProducer, targetFields, beganLoading, endedLoading){
	this.rateCalcProducer = rateCalcProducer;
	this.targetFields = targetFields;
	this.beganLoading = beganLoading;
	this.endedLoading = endedLoading;

	this.currentRequest = null;

	var me = this;

	$.each(me.rateCalcProducer, function (index, el)
	{
		el.on('change', function (e)
		{
			me.recalculateRates(e, me.rateCalcProducer);
		});
	});

	me.targetFields.rateAgent.on('change', function(e, autoupdate){
//		console.log('agent rate change (1)', autoupdate, me.targetFields.agentRatePay.val());
		if (!autoupdate)
		{
			var input = adaptFloat(me.targetFields.rateAgent.val());
			var paidWeight = adaptFloat(me.rateCalcProducer.chargeableWeightAgent.val());
			var pay = input * paidWeight;
//			console.log('agent rate change (2)', input, paidWeight, pay);
			me.targetFields.agentRatePay.val(pay).trigger('change');
		}
	});

	me.targetFields.rateCarrier.on('change', function(e, autoupdate){
//		console.log('carrier rate change (1)', autoupdate, me.targetFields.carrierRatePay.val());
		if (!autoupdate)
		{
			var input = adaptFloat(me.targetFields.rateCarrier.val());
			var paidWeight = adaptFloat(me.rateCalcProducer.chargeableWeightCarrier.val());
			var pay = input * paidWeight;
//			console.log('carrier rate change (2)', input, paidWeight, pay);

			me.targetFields.carrierRatePay.val(pay).trigger('change');
		}
	});

	me.targetFields.agentRateStrategy.on('change', function(){
		var clsName = "selected-" + $(this).val();
		me.targetFields.rateAgentDescr.children().removeClass().addClass(clsName);
	});

	me.targetFields.carrierRateStrategy.on('change', function(){
		var clsName = "selected-" + $(this).val();
		me.targetFields.rateCarrierDescr.children().removeClass().addClass(clsName);
	});
};

Rates.prototype.init = function(){
};

Rates.prototype.recalculateRates = function (e, producerElements)
{
	var me = this;

	if (me.currentRequest) {
		me.currentRequest.abort();
	} else {
		me.beganLoading();
	}

	me.currentRequest = $.ajax({
		url: Routing.generate('avia_shipment_calculate_rate'),
		method: 'POST',
		dataType: 'json',
		data: {
			chargeableWeightCarrier: adaptFloat(producerElements.chargeableWeightCarrier.val()),
			chargeableWeightAgent: adaptFloat(producerElements.chargeableWeightAgent.val()),
			dstStation: producerElements.dstStation.val()
		}
	}).done(function (response)
			{
				me.currentRequest = null;

				if (!response.agent)
					response.agent = {};
				if (!response.agent.pay || me.targetFields.agentRatePay.val() != response.agent.pay)
				{
					me.targetFields.rateAgent.val(response.agent.rate).trigger('change', ['autoupdate']);
					me.targetFields.agentRateStrategy.val(response.agent.strategy).trigger('change');
					me.targetFields.agentRatePay.val(response.agent.pay).trigger('change');
				}


				if (!response.carrier)
					response.carrier = {};
				if (!response.carrier.pay || me.targetFields.carrierRatePay.val() != response.carrier.pay)
				{
					me.targetFields.rateCarrier.val(response.carrier.rate).trigger('change', ['autoupdate']);
					me.targetFields.carrierRateStrategy.val(response.carrier.strategy).trigger('change');
					me.targetFields.carrierRatePay.val(response.carrier.pay).trigger('change');
				}

				me.endedLoading();
			});
};