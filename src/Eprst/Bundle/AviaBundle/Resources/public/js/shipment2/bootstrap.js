Ext.onReady(function () {

	Ext.require([
		'Eprst.Shipment.ShipmentModel',
		'Eprst.Shipment.ShipmentStore',
		'Eprst.Shipment.PiecesGrid'
	]);

	var formFields = {
		volume: $('#aviabundle_shipmenttype_volume'),
		grossWeight: $('#aviabundle_shipmenttype_grossWeight'),
		agentRateStrategy: $('#aviabundle_shipmenttype_agentRateStrategy'),
		agentRatePay: $('#aviabundle_shipmenttype_agentRatePay'),
		carrierRateStrategy: $('#aviabundle_shipmenttype_carrierRateStrategy'),
		carrierRatePay: $('#aviabundle_shipmenttype_carrierRatePay'),

		carrier: $('#aviabundle_shipmenttype_carrier'),
		chargeableWeightCarrier: $('#aviabundle_shipmenttype_chargeableWeightCarrier'),
		chargeableWeightAgent: $('#aviabundle_shipmenttype_chargeableWeightAgent'),
		srcStation: $('#aviabundle_shipmenttype_srcStation'),
		dstStation: $('#aviabundle_shipmenttype_dstStation'),
		rateAgent: $('#aviabundle_shipmenttype_rateAgent'),
		rateAgentWidget: $('#aviabundle_shipmenttype_rateAgent_widget'),
		rateCarrier: $('#aviabundle_shipmenttype_rateCarrier'),
		rateCarrierWidget: $('#aviabundle_shipmenttype_rateCarrier_widget'),
		feeMyc: $('#aviabundle_shipmenttype_feeMyc'),
		feeXdc: $('#aviabundle_shipmenttype_feeXdc'),
		feeAdd: $('#aviabundle_shipmenttype_feeAdd'),
		feeAddCheck: $('#aviabundle_shipmenttype_feeAdd_check'),
		totalFee: $('#aviabundle_shipmenttype_totalFee'),
		totalAgent: $('#aviabundle_shipmenttype_totalAgent'),
		totalCarrier: $('#aviabundle_shipmenttype_totalCarrier'),
		formHiddenContainer: $('#form_auto_hidden_container')
	};

	var fee, rates, total, mask = new Mask($('#shipment-calculation-panel'));

	var store = Ext.create('Eprst.Shipment.ShipmentStore', {
		data: editPlaces || []
	});

	store.on('totalvalueschanged', function(modified, values){
		onPiecesStoreChange(modified, values);
	});

	Ext.create('Eprst.Shipment.PiecesGrid', {
		store: store,
		renderTo: 'aviabundle_shipmenttype_places',
		listeners: {
			// Prepare store on initial grid rendering
			viewready: function(cmp, e){
				console.log('viewready', e);
				var store = cmp.getStore();
				store.calculateDerivativeValues();

				if (haveEditingData)
				{
					total.calculate();
					formFields.agentRateStrategy.trigger('change');
					formFields.carrierRateStrategy.trigger('change');
				}
			}
		}
	});

    function recalcShipment()
    {
        var isValid = true;

        if (typeof(placesPrototypeDef) != 'undefined')
        {
            var html = [];
            var target = formFields.formHiddenContainer;
            target.empty();

            var items = store.getRange();
            for (var i = 0, len = items.length; i < len; i++) {
                for (var f in placesPrototypeDef) if (placesPrototypeDef.hasOwnProperty(f)) {
                    var inputName = placesPrototypeDef[f].name.replace('__name__', i);
                    html.push("<input type='hidden' name='"
                            + inputName + "' value='"
                            + items[i].data[f] + "' />"
                    );
                }
            }
            target.append(html.join(''));
        }

        total.calculate();

        return isValid;
    }

	$('#shipment-form').on('submit', function (e)
	{
		var result = recalcShipment();

		return true;
	});

	function onPiecesStoreChange(modified, values)
	{
		if ('totalWeight' in modified || 'volumeWeight' in modified)
		{
			calcPaidWeight(values);
		}

		if ('totalWeight' in modified)
		{
			calcFees(values);
		}

		if ('volume' in modified)
		{
			calcVolume(values);
		}

        $('#aviabundle_shipmenttype_save').attr('disabled', 'disabled');
    }

	function calcPaidWeight(values)
	{
		var totalWeightSum = values.totalWeight;
		var volumeWeightSum = values.volumeWeight;
		var paidWeightNewValue = Math.max(totalWeightSum, volumeWeightSum);

		var paidWeightNewValueRounded = roundWithBorder(paidWeightNewValue, 0.5);
		var paidWeightNewValuePrecise = roundToPrecision(paidWeightNewValue, 2);

		if (parseFloat(formFields.chargeableWeightCarrier.val()) != paidWeightNewValuePrecise) {
			formFields.chargeableWeightCarrier.val(paidWeightNewValueRounded).trigger('change');
			formFields.chargeableWeightCarrier.data('precise-value', paidWeightNewValuePrecise);
		}

		if (parseFloat(formFields.chargeableWeightAgent.val()) != paidWeightNewValuePrecise) {
			formFields.chargeableWeightAgent.val(paidWeightNewValueRounded).trigger('change');
			formFields.chargeableWeightAgent.data('precise-value', paidWeightNewValuePrecise);
		}
	}

	function calcFees(values)
	{
		var totalWeightSum = values.totalWeight;

		if (formFields.grossWeight.val() != totalWeightSum)
		{
			formFields.grossWeight.val(totalWeightSum);
			formFields.grossWeight.trigger('change');
		}
	}

	function calcVolume(values)
	{
		var val = values.volume;

		if (formFields.volume.val() != val)
		{
			formFields.volume.val(val);
			formFields.volume.trigger('change');
		}
	}

	rates = new Rates({
		chargeableWeightCarrier: formFields.chargeableWeightCarrier,
		chargeableWeightAgent: formFields.chargeableWeightAgent,
		dstStation: formFields.dstStation
	}, {
		rateAgent: formFields.rateAgent,
		rateAgentDescr: formFields.rateAgentWidget,
		rateCarrier: formFields.rateCarrier,
		rateCarrierDescr: formFields.rateCarrierWidget,

		agentRateStrategy: formFields.agentRateStrategy,
		agentRatePay: formFields.agentRatePay,
		carrierRateStrategy: formFields.carrierRateStrategy,
		carrierRatePay: formFields.carrierRatePay
	}, mask.show.bind(mask), mask.hide.bind(mask));

	fee = new CarrierFee({
		carrier: formFields.carrier,
		grossWeight: formFields.grossWeight,
        chargeableWeightCarrier: formFields.chargeableWeightCarrier
	},{
		totalFee: formFields.totalFee,
		feeMyc: formFields.feeMyc,
		feeXdc: formFields.feeXdc,
		feeAdd: formFields.feeAdd,
        feeAddCheck: formFields.feeAddCheck
	}, mask.show.bind(mask), mask.hide.bind(mask));

	total = new Total({
		agentRatePay: formFields.agentRatePay,
		carrierRatePay: formFields.carrierRatePay,
		totalFee: formFields.totalFee
	},{
		totalAgent: formFields.totalAgent,
		totalCarrier: formFields.totalCarrier
	});

    $('#recalc_button').click(function(){

        recalcShipment();

        $('#aviabundle_shipmenttype_save').removeAttr('disabled', '');
    });

    formFields.feeAddCheck.on('change', function(){
        if (this.checked) {
            formFields.feeAdd.val(roundToPrecision(0.2 * adaptFloat(formFields.chargeableWeightCarrier.val()), 2));
        } else {
            formFields.feeAdd.val(0);
        }
        formFields.feeAdd.trigger('change');
    });
    formFields.feeAdd.on('change', function(){
        formFields.totalFee.val(roundToPrecision(adaptFloat(formFields.feeMyc.val())
                                + adaptFloat(formFields.feeXdc.val())
                                + adaptFloat(formFields.feeAdd.val()), 2));
        formFields.totalFee.trigger('change');
    });
});
