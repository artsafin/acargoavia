var CarrierFee = function(producerFields, consumerFields, beganLoading, endedLoading){
	this.producerFields = producerFields;
	this.consumerFields = consumerFields;
	this.beganLoading = beganLoading;
	this.endedLoading = endedLoading;

	var me = this;

	$.each(me.producerFields, function (index, el)
	{
		$(el).on('change', function (e)
		{
			me.recalculateFee(me.producerFields.chargeableWeightCarrier.val(), me.producerFields.carrier.val());
		});
	});
};

CarrierFee.prototype.init = function(){
};

CarrierFee.prototype.recalculateFee = function(weight, carrier){
	var me = this;

	me.beganLoading();

	$.ajax({
		url: Routing.generate('avia_shipment_calculate_fee'),
		method: 'POST',
		dataType: 'json',
		data: {
			weight: adaptFloat(weight),
			carrier: carrier
		}
	}).done(function (response)
			{
				if (response.total)
				{
					me.consumerFields.feeMyc.val(response.myc);
					me.consumerFields.feeXdc.val(response.xdc);

                    var feeAddValue = 0;
                    if (me.consumerFields.feeAddCheck.get(0).checked) {
                        feeAddValue = roundToPrecision(0.2 * adaptFloat(weight), 2);
                    }
                    me.consumerFields.feeAdd.val(feeAddValue);

                    if (me.consumerFields.totalFee.val() != response.total)
					{
                        var totalVal = roundToPrecision(adaptFloat(response.total) + adaptFloat(me.consumerFields.feeAdd.val()), 2);
						me.consumerFields.totalFee.val(totalVal);
						me.consumerFields.totalFee.trigger('change');
					}
				}
				me.endedLoading();
			});
};