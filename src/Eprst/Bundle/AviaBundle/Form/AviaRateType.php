<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AviaRateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('arrivalStation', null, array('property' => 'name', 'required' => true))
            ->add('min', null, array('required' => true))
            ->add('n', null, array('required' => true))
            ->add('range45', null, array('label' => '45', 'required' => true))
            ->add('range100', null, array('label' => '100', 'required' => true))
            ->add('range300', null, array('label' => '300', 'required' => true))
            ->add('range500', null, array('label' => '500', 'required' => true))
            ->add('range1000', null, array('label' => '1000+', 'required' => true))
        ;
        $builder->add('Add', 'submit', array('attr' => array('class' => 'btn btn-primary')));
        $builder->setAction($options['action']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eprst\Bundle\AviaBundle\Entity\AviaRate'
        ));
    }

    public function getName()
    {
        return 'aviabundle_aviarate';
    }
}
