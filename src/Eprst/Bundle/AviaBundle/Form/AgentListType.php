<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AgentListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('agent', 'entity', array(
                                         'property' => 'name',
                                         'required' => true,
                                         'class' => 'Eprst\Bundle\AviaBundle\Entity\Agent'
                                    )
                )
            ->add('Show', 'submit', array('attr' => array('class' => 'btn btn-primary')))
        ;

        $builder->setAction($options['action']);
    }

    public function getName()
    {
        return 'aviabundle_agentlist';
    }
}
