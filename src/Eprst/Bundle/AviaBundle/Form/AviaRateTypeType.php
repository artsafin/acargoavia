<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AviaRateTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAction($options['action']);
        $builder->add('rates', 'collection', array('type' => 'aviabundle_aviarate_values'));
        $builder->add('save', 'submit', array('attr' => array('class' => 'btn btn-primary')));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eprst\Bundle\AviaBundle\Entity\AviaRateType',
            'cascade_validation' => true,
        ));
    }

    public function getName()
    {
        return 'aviabundle_aviaratetype';
    }
}
