<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CarrierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array('required' => true))
//            ->add('logo', null, array('required' => false))
            ->add('carrierType', null, array('required' => true, 'property' => 'name'))
            ->add('save', 'submit', array('attr' => array('class' => 'btn btn-primary')))
        ;
        $builder->setAction($options['action']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eprst\Bundle\AviaBundle\Entity\Carrier'
        ));
    }

    public function getName()
    {
        return 'aviabundle_carrier';
    }
}
