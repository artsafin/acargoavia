<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AviaRateValuesType extends AviaRateType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('id', 'hidden');
        $builder->remove('Add');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eprst\Bundle\AviaBundle\Entity\AviaRate',
            'allow_add' => true
        ));
    }

    public function getName()
    {
        return 'aviabundle_aviarate_values';
    }
}
