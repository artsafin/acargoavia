<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AgentPeopleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('agent', null, array('property' => 'name', 'required' => true))
            ->add('name', null, array('required' => true))
            ->add('phone', null, array('required' => true))
            ->add('email', null, array('required' => true))
            ->add('save', 'submit', array('attr' => array('class' => 'btn btn-primary')))
        ;

        $builder->setAction($options['action']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eprst\Bundle\AviaBundle\Entity\AgentPeople'
        ));
    }

    public function getName()
    {
        return 'aviabundle_agentpeople';
    }
}
