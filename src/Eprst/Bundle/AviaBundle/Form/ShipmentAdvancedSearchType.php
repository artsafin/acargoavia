<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Eprst\Bundle\AviaBundle\Repository\AgentStatus;
use Eprst\Bundle\AviaBundle\Repository\StationStatus;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ShipmentAdvancedSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('waybill', 'text',
                        array(
                            'label' => 'Waybill',
                            'required' => false,
                        )
                );
        $builder->add('dateWaybillFrom', 'date',
                      array(
                           'label' => 'Waybill date',
                           'required' => false,
                           'widget' => 'single_text',
                           'format' => 'dd.MM.yyyy'
                      ));
        $builder->add('dateWaybillTo', 'date',
                      array(
                           'label' => 'Waybill date',
                           'required' => false,
                           'widget' => 'single_text',
                           'format' => 'dd.MM.yyyy'
                      ));

        $builder->add('srcStation',
                      'entity',
                      array(
                           'label'         => 'Station',
                           'class'         => 'EprstAviaBundle:Station',
                           'query_builder' => function (EntityRepository $repo) {
                               $qb = $repo->createQueryBuilder('s');
                               $qb->where('s.isDeparture = 1 AND s.active=1 and s.stationStatus != :deletedStatus');
                               $qb->orderBy('s.name');
	                           $qb->setParameter(':deletedStatus', StationStatus::STATUS_DELETED);

	                           return $qb;
                           },
                           'property'      => 'name',
                           'required'      => false,
                      ));

        $builder->add('dstStation',
                      'entity',
                      array(
                           'label'         => 'Station',
                           'class'         => 'EprstAviaBundle:Station',
                           'query_builder' => function (EntityRepository $repo) {
                               $qb = $repo->createQueryBuilder('s');
                               $qb->where('s.isArrival = 1 AND s.active=1 and s.stationStatus != :deletedStatus');
                               $qb->orderBy('s.name');

	                           $qb->setParameter(':deletedStatus', StationStatus::STATUS_DELETED);

                               return $qb;
                           },
                           'property'      => 'name',
                           'required'      => false,
                      ));

        $builder->add('agent',
                      'entity',
                      array(
                           'label'    => 'Agent',
                           'class'    => 'EprstAviaBundle:Agent',
	                       'query_builder' => function (EntityRepository $repo)
	                       {
		                       $qb = $repo->createQueryBuilder('a');
		                       $qb->where('a.status != :deletedStatus');
		                       $qb->orderBy('a.name');

		                       $qb->setParameter(':deletedStatus', AgentStatus::STATUS_DELETED);

		                       return $qb;
	                       },
                           'property' => 'name',
                           'required' => false,
                      ));

        $builder->add('search', 'submit', array('label' => 'Search'));

        $builder->setMethod('GET');

        if (isset($options['action'])) {
            $builder->setAction($options['action']);
        }
    }

    public function getName()
    {
        return 'aviabundle_shipment_advancedsearchtype';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
                                    'csrf_protection' => false,
                               ));
    }
}
