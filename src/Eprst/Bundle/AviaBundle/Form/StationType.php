<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Translation\TranslatorInterface;

class StationType extends AbstractType
{
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    private $translator;

    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $_ = \Eprst\Util\gettrans($this->translator, 'EprstAviaBundle');

        $builder
            ->add('name', null, array('required' => true, 'label' => $_('Name')))
            ->add('active', null, array('required' => false, 'label' => $_('Active')))
            ->add('country', null, array('label' => $_('Country')))
            ->add('description', 'textarea', array('label' => $_('Description'), 'required' => false))
            ->add('stationStatus', null, array('property' => 'name', 'required' => true, 'label' => $_('Station status')))
            ->add('isArrival', null, array('required' => false, 'label' => $_('Destination')))
            ->add('isDeparture', null, array('required' => false, 'label' => $_('Source')))
            ->add('save', 'submit', array('attr' => array('class' => 'btn btn-primary'), 'label' => $_('Save')))
        ;
        $builder->setAction($options['action']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eprst\Bundle\AviaBundle\Entity\Station'
        ));
    }

    public function getName()
    {
        return 'geobundle_station';
    }
}
