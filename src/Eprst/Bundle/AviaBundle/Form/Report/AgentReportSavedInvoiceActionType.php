<?php

namespace Eprst\Bundle\AviaBundle\Form\Report;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Eprst\Bundle\AviaBundle\Form\TranslatedFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class AgentReportSavedInvoiceActionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('invoiceNumber', 'text', array('label' => 'Invoice number'));
        $builder->add('signed', 'checkbox', array('label' => 'Is signed', 'required' => false));
        $builder->add('invoice', 'submit', array('label' => 'Make invoice'));

        $builder->setMethod('GET');
    }

    public function getName()
    {
        return 'aviabundle_report_agent_saved_action_invoice';
    }
}
