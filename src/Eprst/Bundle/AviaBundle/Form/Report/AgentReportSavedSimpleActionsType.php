<?php

namespace Eprst\Bundle\AviaBundle\Form\Report;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Eprst\Bundle\AviaBundle\Form\TranslatedFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class AgentReportSavedSimpleActionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('delete', 'submit', array('label' => 'Delete'));

        $builder->setMethod('GET');
    }

    public function getName()
    {
        return 'aviabundle_report_agent_saved_action_simple';
    }
}
