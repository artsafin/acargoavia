<?php

namespace Eprst\Bundle\AviaBundle\Form\Report;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Eprst\Bundle\AviaBundle\Form\TranslatedFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class AgentReportFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('agent', 'entity',
                        array(
                            'label' => 'Agent',
                            'class' => 'EprstAviaBundle:Agent',
                            'property' => 'name',
                            'required' => true,
                        )
                );
        $builder->add('dateFrom', 'date',
                      array(
                           'label' => 'Date period',
                           'required' => true,
                           'widget' => 'single_text',
                           'format' => 'dd.MM.yyyy'
                      ));
        $builder->add('dateTo', 'date',
                      array(
                           'label' => 'Date period',
                           'required' => true,
                           'widget' => 'single_text',
                           'format' => 'dd.MM.yyyy'
                      ));

        $builder->add('currencyRateDate', 'date',
                      array(
                           'label' => 'Currency rate date',
                           'required' => true,
                           'widget' => 'single_text',
                           'format' => 'dd.MM.yyyy'
                      ));

        $builder->add('show', 'submit', array('label' => 'Show'));
        $builder->add('save', 'submit', array('label' => 'Approve report'));
        $builder->add('export', 'submit', array('label' => 'Export'));

        $builder->setMethod('GET');
    }

    public function getName()
    {
        return 'aviabundle_report_agent';
    }
}
