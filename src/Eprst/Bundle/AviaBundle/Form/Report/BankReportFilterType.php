<?php

namespace Eprst\Bundle\AviaBundle\Form\Report;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class BankReportFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date_type', 'choice',
                        array(
                            'choices' => array(
                                'dateWaybill' => 'Waybill date',
                                'dateFactDeparture' => 'Actual departure date',
                            ),
                            'expanded' => true,
                            'required' => true,
                        )
                );
        $builder->add('from', 'date',
                      array(
                           'label' => 'Date period',
                           'required' => true,
                           'widget' => 'single_text',
                           'format' => 'dd.MM.yyyy'
                      ));
        $builder->add('to', 'date',
                      array(
                           'label' => 'Date period',
                           'required' => true,
                           'widget' => 'single_text',
                           'format' => 'dd.MM.yyyy'
                      ));

        $builder->add('show', 'submit', array('label' => 'Show'));
        $builder->add('export', 'submit', array('label' => 'Export'));
        $builder->add('approve', 'submit', array('label' => 'Approve report'));

        $builder->setMethod('GET');
        $builder->setAction($options['action']);
    }

    public function getName()
    {
        return 'aviabundle_report_bank';
    }
}
