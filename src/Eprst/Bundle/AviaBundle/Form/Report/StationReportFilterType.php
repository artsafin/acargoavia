<?php

namespace Eprst\Bundle\AviaBundle\Form\Report;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Eprst\Bundle\AviaBundle\Form\TranslatedFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class StationReportFilterType extends TranslatedFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $_ = \Eprst\Util\gettrans($this->translator);
        $builder->add(
            'date_type',
            'choice',
            array(
                 'choices'  => array(
                     'dateWaybill'       => $_('Waybill date'),
                     'dateFactDeparture' => $_('Actual departure date'),
                 ),
                 'expanded' => true,
                 'required' => true,
            )
        );
        $builder->add('station', 'entity',
                        array(
                            'label' => $_('Station'),
                            'class' => 'EprstAviaBundle:Station',
                            'query_builder' => function(EntityRepository $repo){
                                $qb = $repo->createQueryBuilder('s');
                                $qb->where('s.isDeparture = 1 AND s.active=1');
                                $qb->orderBy('s.name');

                                return $qb;
                            },
                            'property' => 'name',
                            'required' => true,
                        )
                );
        $builder->add('from', 'date',
                      array(
                           'label' => $_('Date period'),
                           'required' => true,
                           'widget' => 'single_text',
                           'format' => 'dd.MM.yyyy'
                      ));
        $builder->add('to', 'date',
                      array(
                           'label' => $_('Date period'),
                           'required' => true,
                           'widget' => 'single_text',
                           'format' => 'dd.MM.yyyy'
                      ));

        $builder->add('show', 'submit', array('label' => $_('Show')));
        $builder->add('export', 'submit', array('label' => $_('Export')));

        $builder->setMethod('GET');
    }

    public function getName()
    {
        return 'aviabundle_report_station';
    }
}
