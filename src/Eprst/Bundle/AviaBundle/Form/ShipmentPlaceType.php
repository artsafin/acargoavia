<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ShipmentPlaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numPieces', null, array('label' => 'Pieces'))
            ->add('length', null, array('label' => 'Length, cm'))
            ->add('width', null, array('label' => 'Width, cm'))
            ->add('height', null, array('label' => 'Height, cm'))
            ->add('volume', null, array('label' => 'Volume, cbm', 'read_only' => false))
            ->add('weight', null, array('label' => 'Weight, kg'))
            ->add('volumeWeight', null, array('label' => 'Volume weight, kg', 'read_only' => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eprst\Bundle\AviaBundle\Entity\ShipmentPlace'
        ));
    }

    public function getName()
    {
        return 'aviabundle_shipmentplacetype';
    }
}
