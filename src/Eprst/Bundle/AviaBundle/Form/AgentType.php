<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AgentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array('required' => true, 'label' => 'Agent name'))
            ->add('nameOfficial', null, array('required' => false, 'label' => 'Agent official name'))
            ->add('legalAddress', 'textarea', array('required' => false))
            ->add('actualAddress', 'textarea', array('required' => false))
            ->add('postAddress', 'textarea', array('required' => false))
            ->add('phone', null, array('required' => false))
            ->add('directorName', null, array('required' => false))
            ->add('accountantName', null, array('required' => false))
            ->add('inn', null, array('required' => true))
            ->add('kpp', null, array('required' => false))
            ->add('bik', null, array('required' => false))
            ->add('corrAccount', null, array('required' => false))
            ->add('currentAccount', null, array('required' => false))
            ->add('bank', null, array('required' => false))
            ->add('agreementNumber', null, array('required' => false))
            ->add('agreementDate', 'date', array('required' => false, 'widget' => 'single_text',
                                                'format' => 'dd.MM.yyyy',
                                           ))
            ->add('station', null, array('property' => 'name', 'required' => true))
            ->add('nameFile', null, array('required' => false, 'label' => 'File name prefix'))
            ->add('save', 'submit', array('attr' => array('class' => 'btn btn-primary')))
        ;

        $builder->setAction($options['action']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Eprst\Bundle\AviaBundle\Entity\Agent'
        ));
    }

    public function getName()
    {
        return 'aviabundle_agent';
    }
}
