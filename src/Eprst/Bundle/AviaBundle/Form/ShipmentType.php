<?php

namespace Eprst\Bundle\AviaBundle\Form;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityRepository;
use Eprst\Bundle\AviaBundle\Form\Transformer\StationNameTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Translation\TranslatorInterface;

class ShipmentType extends AbstractType
{
    /**
     * @var \Symfony\Component\Form\DataTransformerInterface
     */
    private $stationNameTransformer;

    function __construct(DataTransformerInterface $stationNameTransformer)
    {
        $this->stationNameTransformer = $stationNameTransformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'carrier',
            null,
            array(
                 'property' => 'name',
                 'required' => true,
                 'label'    => 'Carrier'
            )
        );
        $builder->add(
            'agent',
            null,
            array(
                 'property' => 'name',
                 'required' => true,
                 'label'    => 'Agent'
            )
        );
        $builder->add(
            'waybillPrefix',
            null,
            array(
                 'required' => true,
                 'label'    => 'Waybill'
            )
        );
        $builder->add(
            'waybill',
            null,
            array(
                 'required' => true,
                 'label'    => 'Waybill'
            )
        );
        $builder->add(
            'dateWaybill',
            'date',
            array(
                 'required' => true,
                 'label'    => 'Waybill date',
                 'widget'   => 'single_text',
                 'format'   => 'dd.MM.yyyy',
                 'attr'     => array('class' => 'datepicked')
            )
        );
        $builder->add(
            'srcStation',
            'entity',
            array(
                 'property' => 'name',
                 'class' => 'EprstAviaBundle:Station',
                 'required' => true,
                 'label'    => 'Source',
                 'query_builder' => function(EntityRepository $repo){
                     $qb = $repo->createQueryBuilder('s');
                     $qb->where('s.isDeparture = 1');
                     $qb->orderBy('s.name');
                     return $qb;
                 }
            )
        );

        $dstStationText = $builder->create(
            'dstStation',
            'text',
            array(
                 'required' => true,
                 'label'    => 'Destination'
            ));
        $dstStationText->addModelTransformer($this->stationNameTransformer);

        $builder->add($dstStationText);


        $builder->add(
            'places',
            'collection',
            array(
                 'type'         => 'aviabundle_shipmentplacetype',
                 'allow_add'    => true,
                 'allow_delete' => true,
                 'by_reference' => false,
                 'required'     => true,
                 'label'        => 'Pieces'
            )
        );
        $builder->add('chargeableWeightCarrier', null, array('required' => true, 'label' => 'Carrier chargeable weight'));
        $builder->add('chargeableWeightAgent', null, array('required' => true, 'label' => 'Agent chargeable weight'));
        $builder->add('goodsNatureCode', null, array('label' => 'Goods nature'));
        $builder->add('goodsNatureDescription', null, array('label' => 'Goods nature'));
        $builder->add('agentRebate', null, array('required' => false, 'label' => 'Agent rebate'));
        $builder->add('fzPrice', null, array('required' => false, 'label' => 'FZ Price'));
        $builder->add('rateAgent', null, array('required' => true, 'label' => 'Agent rate'));
        $builder->add('rateCarrier', null, array('required' => true, 'label' => 'Carrier rate'));
        $builder->add(
            'feeMyc',
            null,
            array(
                 'required'  => true,
                 'label'     => 'MYC',
                 'read_only' => true,
                 'property_path' => 'fee.myc',
            )
        );
        $builder->add(
            'feeXdc',
            null,
            array(
                 'required'  => true,
                 'label'     => 'XDC',
                 'read_only' => true,
                 'property_path' => 'fee.xdc',
            )
        );
        $builder->add(
            'feeAdd',
            null,
            array(
                 'required'  => true,
                 'label'     => 'Add',
                 'read_only' => false,
                 'property_path' => 'fee.opt',
            )
        );
        $builder->add(
            'totalFee',
            null,
            array(
                 'required'  => true,
                 'label'     => 'Total fees',
                 'read_only' => true
            )
        );
        $builder->add('totalAgent', null, array('read_only' => true, 'mapped' => false, 'label' => 'Agent total'));
        $builder->add(
            'totalCarrier',
            null,
            array(
                 'read_only' => true,
                 'mapped'    => false,
                 'label'     => 'Carrier total'
            )
        );
        $builder->add(
            'dateFactDeparture',
            'date',
            array(
                 'required' => false,
                 'label'    => 'Actual departure date',
                 'widget'   => 'single_text',
                 'format'   => 'dd.MM.yyyy',
                 'attr'     => array('class' => 'datepicked')
            )
        );

        $builder->add(
            'save',
            'submit',
            array(
                 'attr'  => array('class' => 'btn btn-primary')
            )
        );

        // Hidden fields
        $builder->add('volume', 'hidden', array('read_only' => true));
        $builder->add('grossWeight', 'hidden', array('read_only' => true));
        $builder->add('agentRateStrategy', 'hidden', array('read_only' => true));
        $builder->add('agentRatePay', 'hidden', array('read_only' => true));
        $builder->add('carrierRateStrategy', 'hidden', array('read_only' => true));
        $builder->add('carrierRatePay', 'hidden', array('read_only' => true));


        $builder->setAction($options['action']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                 'data_class' => 'Eprst\Bundle\AviaBundle\Entity\Shipment'
            )
        );
    }

    public function getName()
    {
        return 'aviabundle_shipmenttype';
    }
}
