<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BankReport
 *
 * @ORM\Table(name="bank_report")
 * @ORM\Entity(repositoryClass="Eprst\Bundle\AviaBundle\Repository\BankReport")
 */
class BankReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts", type="datetime", nullable=true)
     */
    private $ts;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_from", type="date", nullable=false)
     */
    private $dateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_to", type="date", nullable=false)
     */
    private $dateTo;

    /**
     * @var float
     *
     * @ORM\Column(name="total_agent_fee", type="decimal", nullable=true)
     */
    private $totalAgentFee;

    /**
     * Set DateFrom
     *
     * @param \DateTime $dateFrom Value of dateFrom
     *
     * @return void
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * Get DateFrom
     *
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set DateTo
     *
     * @param \DateTime $dateTo Value of dateTo
     *
     * @return void
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }

    /**
     * Get DateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set TotalAgentFee
     *
     * @param float $totalAgentFee Value of totalAgentFee
     *
     * @return void
     */
    public function setTotalAgentFee($totalAgentFee)
    {
        $this->totalAgentFee = $totalAgentFee;
    }

    /**
     * Get TotalAgentFee
     *
     * @return float
     */
    public function getTotalAgentFee()
    {
        return $this->totalAgentFee;
    }

    /**
     * Set Ts
     *
     * @param \DateTime $ts Value of ts
     *
     * @return void
     */
    public function setTs($ts)
    {
        $this->ts = $ts;
    }

    /**
     * Get Ts
     *
     * @return \DateTime
     */
    public function getTs()
    {
        return $this->ts;
    }
}
