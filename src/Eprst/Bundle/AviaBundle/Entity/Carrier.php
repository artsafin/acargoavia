<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Eprst\Bundle\AviaBundle\Repository\CarrierStatus as CarrierStatusRepository;

/**
 * Carrier
 *
 * @ORM\Table(name="carrier")
 * @ORM\Entity
 */
class Carrier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var CarrierStatus
     *
     * @ORM\ManyToOne(targetEntity="CarrierStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="CarrierType")
     * @ORM\JoinColumn(name="carrier_type_id", referencedColumnName="id", nullable=false)
     */
    private $carrierType;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=45, nullable=true)
     */
    private $logo;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set carrierType
     *
     * @param integer $carrierType
     * @return Carrier
     */
    public function setCarrierType($carrierType)
    {
        $this->carrierType = $carrierType;
    
        return $this;
    }

    /**
     * Get carrierType
     *
     * @return integer 
     */
    public function getCarrierType()
    {
        return $this->carrierType;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Carrier
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set logo
     *
     * @param string $logo
     * @return Carrier
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    
        return $this;
    }

    /**
     * Get logo
     *
     * @return string 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set Status
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\CarrierStatus $status Value of status
     *
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get Status
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\CarrierStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

}