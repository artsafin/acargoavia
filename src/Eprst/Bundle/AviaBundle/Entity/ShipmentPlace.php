<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShipmentPlace
 *
 * @ORM\Table(name="shipment_place")
 * @ORM\Entity
 */
class ShipmentPlace
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_pieces", type="integer", nullable=false)
     */
    private $numPieces;

    /**
     * @var float
     *
     * @ORM\Column(name="length", type="decimal", nullable=true)
     */
    private $length;

    /**
     * @var float
     *
     * @ORM\Column(name="width", type="decimal", nullable=true)
     */
    private $width;

    /**
     * @var float
     *
     * @ORM\Column(name="height", type="decimal", nullable=true)
     */
    private $height;

    /**
     * @var float
     *
     * @ORM\Column(name="volume", type="decimal", nullable=true)
     */
    private $volume;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="decimal", nullable=true)
     */
    private $weight;

    /**
     * @var float
     *
     * @ORM\Column(name="volume_weight", type="decimal", nullable=true)
     */
    private $volumeWeight;

    /**
     * @var \Eprst\Bundle\AviaBundle\Entity\Shipment
     *
     * @ORM\ManyToOne(targetEntity="Eprst\Bundle\AviaBundle\Entity\Shipment", inversedBy="places")
     * @ORM\JoinColumn(name="shipment_id", referencedColumnName="id")
     */
    private $shipment;

    /**
     * Set Height
     *
     * @param float $height Value of height
     *
     * @return void
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * Get Height
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set Id
     *
     * @param int $id Value of id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Length
     *
     * @param float $length Value of length
     *
     * @return void
     */
    public function setLength($length)
    {
        $this->length = $length;
    }

    /**
     * Get Length
     *
     * @return float
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set Shipment
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\Shipment $shipment Value of shipment
     *
     * @return void
     */
    public function setShipment($shipment)
    {
        $this->shipment = $shipment;
    }

    /**
     * Get Shipment
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\Shipment
     */
    public function getShipment()
    {
        return $this->shipment;
    }

    /**
     * Set Volume
     *
     * @param float $volume Value of volume
     *
     * @return void
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
    }

    /**
     * Get Volume
     *
     * @return float
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set VolumeWeight
     *
     * @param float $volumeWeight Value of volumeWeight
     *
     * @return void
     */
    public function setVolumeWeight($volumeWeight)
    {
        $this->volumeWeight = $volumeWeight;
    }

    /**
     * Get VolumeWeight
     *
     * @return float
     */
    public function getVolumeWeight()
    {
        return $this->volumeWeight;
    }

    /**
     * Set Weight
     *
     * @param float $weight Value of weight
     *
     * @return void
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * Get Weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set Width
     *
     * @param float $width Value of width
     *
     * @return void
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * Get Width
     *
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set NumPieces
     *
     * @param int $numPieces Value of numPieces
     *
     * @return void
     */
    public function setNumPieces($numPieces)
    {
        $this->numPieces = $numPieces;
    }

    /**
     * Get NumPieces
     *
     * @return int
     */
    public function getNumPieces()
    {
        return $this->numPieces;
    }

}
