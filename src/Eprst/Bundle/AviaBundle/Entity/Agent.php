<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Eprst\Bundle\AviaBundle\Repository\AgentStatus as AgentStatusRepository;

/**
 * Agent
 *
 * @ORM\Table(name="agent")
 * @ORM\Entity
 */
class Agent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var AgentStatus
     *
     * @ORM\ManyToOne(targetEntity="AgentStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_official", type="string", length=100, nullable=false)
     */
    private $nameOfficial;

    /**
     * @var string
     *
     * @ORM\Column(name="name_file", type="string", length=100, nullable=false)
     */
    private $nameFile;

    /**
     * @var string
     *
     * @ORM\Column(name="legal_address", type="string", length=200, nullable=true)
     */
    private $legalAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="actual_address", type="string", length=200, nullable=true)
     */
    private $actualAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="post_address", type="string", length=200, nullable=true)
     */
    private $postAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="director_name", type="string", length=100, nullable=true)
     */
    private $directorName;

    /**
     * @var string
     *
     * @ORM\Column(name="accountant_name", type="string", length=100, nullable=true)
     */
    private $accountantName;

    /**
     * @var string
     *
     * @ORM\Column(name="inn", type="string", length=12, nullable=true)
     */
    private $inn;

    /**
     * @var string
     *
     * @ORM\Column(name="kpp", type="string", length=9, nullable=true)
     */
    private $kpp;

    /**
     * @var string
     *
     * @ORM\Column(name="bik", type="string", length=9, nullable=true)
     */
    private $bik;

    /**
     * @var string
     *
     * @ORM\Column(name="corr_account", type="string", length=34, nullable=true)
     */
    private $corrAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="current_account", type="string", length=34, nullable=true)
     */
    private $currentAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="bank", type="string", length=100, nullable=true)
     */
    private $bank;

    /**
     * @var string
     *
     * @ORM\Column(name="agreement_number", type="string", length=30, nullable=true)
     */
    private $agreementNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="agreement_date", type="datetime", nullable=true)
     */
    private $agreementDate;

    /**
     * @var Station
     *
     * @ORM\ManyToOne(targetEntity="Eprst\Bundle\AviaBundle\Entity\Station")
     * @ORM\JoinColumn(name="station_id", referencedColumnName="id", nullable=false)
     */
    private $station;

    /**
     * @var AgentPeople
     *
     * @ORM\OneToMany(targetEntity="Eprst\Bundle\AviaBundle\Entity\AgentPeople", mappedBy="agent")
     */
    private $people;

    public function __construct()
    {
        $this->people = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Agent
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set legalAddress
     *
     * @param string $legalAddress
     * @return Agent
     */
    public function setLegalAddress($legalAddress)
    {
        $this->legalAddress = $legalAddress;
    
        return $this;
    }

    /**
     * Get legalAddress
     *
     * @return string 
     */
    public function getLegalAddress()
    {
        return $this->legalAddress;
    }

    /**
     * Set actualAddress
     *
     * @param string $actualAddress
     * @return Agent
     */
    public function setActualAddress($actualAddress)
    {
        $this->actualAddress = $actualAddress;
    
        return $this;
    }

    /**
     * Get actualAddress
     *
     * @return string 
     */
    public function getActualAddress()
    {
        return $this->actualAddress;
    }

    /**
     * Set postAddress
     *
     * @param string $postAddress
     * @return Agent
     */
    public function setPostAddress($postAddress)
    {
        $this->postAddress = $postAddress;
    
        return $this;
    }

    /**
     * Get postAddress
     *
     * @return string 
     */
    public function getPostAddress()
    {
        return $this->postAddress;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Agent
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set directorName
     *
     * @param string $directorName
     * @return Agent
     */
    public function setDirectorName($directorName)
    {
        $this->directorName = $directorName;
    
        return $this;
    }

    /**
     * Get directorName
     *
     * @return string 
     */
    public function getDirectorName()
    {
        return $this->directorName;
    }

    /**
     * Set accountantName
     *
     * @param string $accountantName
     * @return Agent
     */
    public function setAccountantName($accountantName)
    {
        $this->accountantName = $accountantName;
    
        return $this;
    }

    /**
     * Get accountantName
     *
     * @return string 
     */
    public function getAccountantName()
    {
        return $this->accountantName;
    }

    /**
     * Set inn
     *
     * @param string $inn
     * @return Agent
     */
    public function setInn($inn)
    {
        $this->inn = $inn;
    
        return $this;
    }

    /**
     * Get inn
     *
     * @return string 
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * Set kpp
     *
     * @param string $kpp
     * @return Agent
     */
    public function setKpp($kpp)
    {
        $this->kpp = $kpp;
    
        return $this;
    }

    /**
     * Get kpp
     *
     * @return string 
     */
    public function getKpp()
    {
        return $this->kpp;
    }

    /**
     * Set bik
     *
     * @param string $bik
     * @return Agent
     */
    public function setBik($bik)
    {
        $this->bik = $bik;
    
        return $this;
    }

    /**
     * Get bik
     *
     * @return string 
     */
    public function getBik()
    {
        return $this->bik;
    }

    /**
     * Set corrAccount
     *
     * @param string $corrAccount
     * @return Agent
     */
    public function setCorrAccount($corrAccount)
    {
        $this->corrAccount = $corrAccount;
    
        return $this;
    }

    /**
     * Get corrAccount
     *
     * @return string 
     */
    public function getCorrAccount()
    {
        return $this->corrAccount;
    }

    /**
     * Set currentAccount
     *
     * @param string $currentAccount
     * @return Agent
     */
    public function setCurrentAccount($currentAccount)
    {
        $this->currentAccount = $currentAccount;
    
        return $this;
    }

    /**
     * Get currentAccount
     *
     * @return string 
     */
    public function getCurrentAccount()
    {
        return $this->currentAccount;
    }

    /**
     * Set bank
     *
     * @param string $bank
     * @return Agent
     */
    public function setBank($bank)
    {
        $this->bank = $bank;
    
        return $this;
    }

    /**
     * Get bank
     *
     * @return string 
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set agreementNumber
     *
     * @param string $agreementNumber
     * @return Agent
     */
    public function setAgreementNumber($agreementNumber)
    {
        $this->agreementNumber = $agreementNumber;
    
        return $this;
    }

    /**
     * Get agreementNumber
     *
     * @return string 
     */
    public function getAgreementNumber()
    {
        return $this->agreementNumber;
    }

    /**
     * Set agreementDate
     *
     * @param \DateTime $agreementDate
     * @return Agent
     */
    public function setAgreementDate($agreementDate)
    {
        $this->agreementDate = $agreementDate;
    
        return $this;
    }

    /**
     * Get agreementDate
     *
     * @return \DateTime 
     */
    public function getAgreementDate()
    {
        return $this->agreementDate;
    }

    /**
     * Set station
     *
     * @param Station $station
     * @return Agent
     */
    public function setStation(Station $station)
    {
        $this->station = $station;
    
        return $this;
    }

    /**
     * Get station
     *
     * @return Station
     */
    public function getStation()
    {
        return $this->station;
    }

    /**
     * Set NameOfficial
     *
     * @param string $nameOfficial Value of nameOfficial
     *
     * @return void
     */
    public function setNameOfficial($nameOfficial)
    {
        $this->nameOfficial = $nameOfficial;
    }

    /**
     * Get NameOfficial
     *
     * @return string
     */
    public function getNameOfficial()
    {
        return $this->nameOfficial;
    }

    /**
     * Set People
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\AgentPeople $people Value of people
     *
     * @return void
     */
    public function setPeople($people)
    {
        $this->people = $people;
    }

    /**
     * Get People
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\AgentPeople
     */
    public function getPeople()
    {
        return $this->people;
    }

    /**
     * Set Status
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\AgentStatus $status Value of status
     *
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get Status
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\AgentStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getNameFile()
    {
        return $this->nameFile;
    }

    /**
     * @param string $nameFile
     */
    public function setNameFile($nameFile)
    {
        $this->nameFile = $nameFile;
    }
}