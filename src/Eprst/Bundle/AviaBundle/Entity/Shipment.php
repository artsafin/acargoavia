<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Eprst\Bundle\AviaBundle\Repository\ShipmentStatus as ShipmentStatusRepository;

/**
 * Shipment
 *
 * @ORM\Table(name="shipment")
 * @ORM\Entity(repositoryClass="Eprst\Bundle\AviaBundle\Repository\Shipment")
 */
class Shipment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ShipmentStatus
     *
     * @ORM\ManyToOne(targetEntity="ShipmentStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_ts", type="datetime", nullable=true)
     */
    private $createTs;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_places_total", type="integer", nullable=true)
     */
    private $numPlaces;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fact_departure", type="datetime", nullable=true)
     */
    private $dateFactDeparture;

    /**
     * @var string
     *
     * @ORM\Column(name="waybill_prefix", type="string", length=3, nullable=false)
     */
    private $waybillPrefix = '141';

    /**
     * @var string
     *
     * @ORM\Column(name="waybill", type="string", length=8, nullable=false)
     */
    private $waybill;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_waybill", type="datetime", nullable=true)
     */
    private $dateWaybill;

    /**
     * @var float
     *
     * @ORM\Column(name="volume", type="decimal", nullable=true)
     */
    private $volume = 0.0;

    /**
     * @var float
     *
     * @ORM\Column(name="gross_weight", type="decimal", nullable=true)
     */
    private $grossWeight = 0.0;

    /**
     * @var float
     *
     * @ORM\Column(name="chargeable_weight_carrier", type="decimal", nullable=true)
     */
    private $chargeableWeightCarrier = 0.0;

    /**
     * @var float
     *
     * @ORM\Column(name="chargeable_weight_agent", type="decimal", nullable=true)
     */
    private $chargeableWeightAgent = 0.0;

    /**
     * @var string
     *
     * @ORM\Column(name="goods_nature_code", type="string", length=5, nullable=true)
     */
    private $goodsNatureCode;

    /**
     * @var string
     *
     * @ORM\Column(name="goods_nature_description", type="string", length=45, nullable=true)
     */
    private $goodsNatureDescription;

    /**
     * @var float
     *
     * @ORM\Column(name="agent_rebate", type="decimal", nullable=false)
     */
    private $agentRebate = 0.0;

    /**
     * @var float
     *
     * @ORM\Column(name="agent_rate", type="decimal", nullable=false)
     */
    private $rateAgent = 0.0;

    /**
     * @var string
     *
     * @ORM\Column(name="agent_rate_strategy", type="string", length=10, nullable=false)
     */
    private $agentRateStrategy;

    /**
     * @var float
     *
     * @ORM\Column(name="agent_rate_pay", type="decimal", nullable=false)
     */
    private $agentRatePay = 0.0;

    /**
     * @var float
     *
     * @ORM\Column(name="carrier_rate", type="decimal", nullable=false)
     */
    private $rateCarrier = 0.0;

    /**
     * @var string
     *
     * @ORM\Column(name="carrier_rate_strategy", type="string", length=10, nullable=false)
     */
    private $carrierRateStrategy;

    /**
     * @var float
     *
     * @ORM\Column(name="carrier_rate_pay", type="decimal", nullable=false)
     */
    private $carrierRatePay = 0.0;

    /**
     * @var float
     *
     * @ORM\Column(name="total_fee", type="decimal", nullable=true)
     */
    private $totalFee = 0.0;

    /**
     * @var \Eprst\Bundle\AviaBundle\Entity\Agent
     *
     * @ORM\ManyToOne(targetEntity="Eprst\Bundle\AviaBundle\Entity\Agent")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     */
    private $agent;

    /**
     * @var \Eprst\Bundle\AviaBundle\Entity\Carrier
     *
     * @ORM\ManyToOne(targetEntity="Eprst\Bundle\AviaBundle\Entity\Carrier")
     * @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     */
    private $carrier;

    /**
     * @var \Eprst\Bundle\AviaBundle\Entity\Station
     *
     * @ORM\ManyToOne(targetEntity="Eprst\Bundle\AviaBundle\Entity\Station")
     * @ORM\JoinColumn(name="src_station_id", referencedColumnName="id")
     */
    private $srcStation;

    /**
     * @var \Eprst\Bundle\AviaBundle\Entity\Station
     *
     * @ORM\ManyToOne(targetEntity="Eprst\Bundle\AviaBundle\Entity\Station")
     * @ORM\JoinColumn(name="dst_station_id", referencedColumnName="id")
     */
    private $dstStation;

    /**
     * @var ShipmentPlace[]
     *
     * @ORM\OneToMany(targetEntity="Eprst\Bundle\AviaBundle\Entity\ShipmentPlace", mappedBy="shipment", cascade={"persist"})
     */
    private $places;

    /**
     * @var \Eprst\Bundle\AviaBundle\Entity\ShipmentFee
     *
     * @ORM\OneToOne(targetEntity="Eprst\Bundle\AviaBundle\Entity\ShipmentFee", mappedBy="shipment", cascade={"persist"})
     */
    private $fee;

    /**
     * @var float
     *
     * @ORM\Column(name="fz_price", type="decimal", nullable=true)
     */
    private $fzPrice;

    public function __construct()
    {
        $this->places = new ArrayCollection();
        $this->setFee(new ShipmentFee());
    }


    /**
     * Set Agent
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\Agent $agent Value of agent
     *
     * @return void
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    /**
     * Get Agent
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\Agent
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set Carrier
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\Carrier $carrier Value of carrier
     *
     * @return void
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;
    }

    /**
     * Get Carrier
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\Carrier
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set CreateTs
     *
     * @param \DateTime $createTs Value of createTs
     *
     * @return void
     */
    public function setCreateTs($createTs)
    {
        $this->createTs = $createTs;
    }

    /**
     * Get CreateTs
     *
     * @return \DateTime
     */
    public function getCreateTs()
    {
        return $this->createTs;
    }

    /**
     * Set DateFactDeparture
     *
     * @param \DateTime $dateFactDeparture Value of dateFactDeparture
     *
     * @return void
     */
    public function setDateFactDeparture($dateFactDeparture)
    {
        $this->dateFactDeparture = $dateFactDeparture;
    }

    /**
     * Get DateFactDeparture
     *
     * @return \DateTime
     */
    public function getDateFactDeparture()
    {
        return $this->dateFactDeparture;
    }

    /**
     * Set DateWaybill
     *
     * @param \DateTime $dateWaybill Value of dateWaybill
     *
     * @return void
     */
    public function setDateWaybill($dateWaybill)
    {
        $this->dateWaybill = $dateWaybill;
    }

    /**
     * Get DateWaybill
     *
     * @return \DateTime
     */
    public function getDateWaybill()
    {
        return $this->dateWaybill;
    }

    /**
     * Set DstStation
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\Station $dstStation Value of dstStation
     *
     * @return void
     */
    public function setDstStation($dstStation)
    {
        $this->dstStation = $dstStation;
    }

    /**
     * Get DstStation
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\Station
     */
    public function getDstStation()
    {
        return $this->dstStation;
    }

    /**
     * Set SrcStation
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\Station $srcStation Value of srcStation
     *
     * @return void
     */
    public function setSrcStation($srcStation)
    {
        $this->srcStation = $srcStation;
    }

    /**
     * Get SrcStation
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\Station
     */
    public function getSrcStation()
    {
        return $this->srcStation;
    }

    /**
     * Set GoodsNatureCode
     *
     * @param string $goodsNatureCode Value of goodsNatureCode
     *
     * @return void
     */
    public function setGoodsNatureCode($goodsNatureCode)
    {
        $this->goodsNatureCode = $goodsNatureCode;
    }

    /**
     * Get GoodsNatureCode
     *
     * @return string
     */
    public function getGoodsNatureCode()
    {
        return $this->goodsNatureCode;
    }

    /**
     * Set GoodsNatureDescription
     *
     * @param string $goodsNatureDescription Value of goodsNatureDescription
     *
     * @return void
     */
    public function setGoodsNatureDescription($goodsNatureDescription)
    {
        $this->goodsNatureDescription = $goodsNatureDescription;
    }

    /**
     * Get GoodsNatureDescription
     *
     * @return string
     */
    public function getGoodsNatureDescription()
    {
        return $this->goodsNatureDescription;
    }

    /**
     * Set GrossWeight
     *
     * @param float $grossWeight Value of grossWeight
     *
     * @return void
     */
    public function setGrossWeight($grossWeight)
    {
        $this->grossWeight = $grossWeight;
    }

    /**
     * Get GrossWeight
     *
     * @return float
     */
    public function getGrossWeight()
    {
        return $this->grossWeight;
    }

    /**
     * Set Id
     *
     * @param int $id Value of id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set NumPlaces
     *
     * @param int $numPlaces Value of numPlaces
     *
     * @return void
     */
    public function setNumPlaces($numPlaces)
    {
        $this->numPlaces = $numPlaces;
    }

    /**
     * Get NumPlaces
     *
     * @return int
     */
    public function getNumPlaces()
    {
        return $this->numPlaces;
    }

    /**
     * Set PaidWeight
     *
     * @param float $paidWeight Value of paidWeight
     *
     * @return void
     */
    public function setChargeableWeightCarrier($paidWeight)
    {
        $this->chargeableWeightCarrier = $paidWeight;
    }

    /**
     * Get PaidWeight
     *
     * @return float
     */
    public function getChargeableWeightCarrier()
    {
        return $this->chargeableWeightCarrier;
    }

    /**
     * Set ChargeableWeightAgent
     *
     * @param float $chargeableWeightAgent Value of chargeableWeightAgent
     *
     * @return void
     */
    public function setChargeableWeightAgent($chargeableWeightAgent)
    {
        $this->chargeableWeightAgent = $chargeableWeightAgent;
    }

    /**
     * Get ChargeableWeightAgent
     *
     * @return float
     */
    public function getChargeableWeightAgent()
    {
        return $this->chargeableWeightAgent;
    }

    /**
     * Set RateAgent
     *
     * @param float $rateAgent Value of rateAgent
     *
     * @return void
     */
    public function setRateAgent($rateAgent)
    {
        $this->rateAgent = $rateAgent;
    }

    /**
     * Get RateAgent
     *
     * @return float
     */
    public function getRateAgent()
    {
        return $this->rateAgent;
    }

    /**
     * Set RateCarrier
     *
     * @param float $rateCarrier Value of rateCarrier
     *
     * @return void
     */
    public function setRateCarrier($rateCarrier)
    {
        $this->rateCarrier = $rateCarrier;
    }

    /**
     * Get RateCarrier
     *
     * @return float
     */
    public function getRateCarrier()
    {
        return $this->rateCarrier;
    }

    /**
     * Set TotalFee
     *
     * @param float $totalFee Value of totalFee
     *
     * @return void
     */
    public function setTotalFee($totalFee)
    {
        $this->totalFee = $totalFee;
    }

    /**
     * Get TotalFee
     *
     * @return float
     */
    public function getTotalFee()
    {
        return $this->totalFee;
    }

    /**
     * Set Volume
     *
     * @param float $volume Value of volume
     *
     * @return void
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
    }

    /**
     * Get Volume
     *
     * @return float
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set Places
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\ShipmentPlace[] $places Value of places
     *
     * @return void
     */
    public function setPlaces($places)
    {
        $this->places = $places;
    }

    /**
     * Get Places
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\ShipmentPlace[]
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * @param ShipmentPlace $place
     */
    public function addPlace($place)
    {
        $place->setShipment($this);
        $this->places->add($place);
    }

    public function removePlace($place)
    {
        $this->places->removeElement($place);
    }

    /**
     * Set Waybill
     *
     * @param string $waybill Value of waybill
     *
     * @return void
     */
    public function setWaybill($waybill)
    {
        $this->waybill = $waybill;
    }

    /**
     * Get Waybill
     *
     * @return string
     */
    public function getWaybill()
    {
        return $this->waybill;
    }

    /**
     * Set WaybillPrefix
     *
     * @param string $waybillPrefix Value of waybillPrefix
     *
     * @return void
     */
    public function setWaybillPrefix($waybillPrefix)
    {
        $this->waybillPrefix = $waybillPrefix;
    }

    /**
     * Get WaybillPrefix
     *
     * @return string
     */
    public function getWaybillPrefix()
    {
        return $this->waybillPrefix;
    }

    public function getFullAirWaybill()
    {
        return sprintf("%s-%s", $this->getWaybillPrefix(), $this->getWaybill());
    }

    /**
     * Set AgentRatePay
     *
     * @param float $agentRatePay Value of agentRatePay
     *
     * @return void
     */
    public function setAgentRatePay($agentRatePay)
    {
        $this->agentRatePay = $agentRatePay;
    }

    /**
     * Get AgentRatePay
     *
     * @return float
     */
    public function getAgentRatePay()
    {
        return $this->agentRatePay;
    }

    /**
     * Set AgentRateStrategy
     *
     * @param string $agentRateStrategy Value of agentRateStrategy
     *
     * @return void
     */
    public function setAgentRateStrategy($agentRateStrategy)
    {
        $this->agentRateStrategy = $agentRateStrategy;
    }

    /**
     * Get AgentRateStrategy
     *
     * @return string
     */
    public function getAgentRateStrategy()
    {
        return $this->agentRateStrategy;
    }

    /**
     * Set CarrierRatePay
     *
     * @param float $carrierRatePay Value of carrierRatePay
     *
     * @return void
     */
    public function setCarrierRatePay($carrierRatePay)
    {
        $this->carrierRatePay = $carrierRatePay;
    }

    /**
     * Get CarrierRatePay
     *
     * @return float
     */
    public function getCarrierRatePay()
    {
        return $this->carrierRatePay;
    }

    /**
     * Set CarrierRateStrategy
     *
     * @param string $carrierRateStrategy Value of carrierRateStrategy
     *
     * @return void
     */
    public function setCarrierRateStrategy($carrierRateStrategy)
    {
        $this->carrierRateStrategy = $carrierRateStrategy;
    }

    /**
     * Get CarrierRateStrategy
     *
     * @return string
     */
    public function getCarrierRateStrategy()
    {
        return $this->carrierRateStrategy;
    }

    /**
     * Set Fee
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\ShipmentFee $fee Value of fee
     *
     * @return void
     */
    public function setFee($fee)
    {
        $this->fee = $fee;
        $this->fee->setShipment($this);
    }

    /**
     * Get Fee
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\ShipmentFee
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set Status
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\ShipmentStatus $status Value of status
     *
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get Status
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\ShipmentStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

	/**
	 * @param float $agentRebate
	 */
	public function setAgentRebate($agentRebate)
	{
		$this->agentRebate = $agentRebate;
	}

	/**
	 * @return float
	 */
	public function getAgentRebate()
	{
		return $this->agentRebate;
	}

    /**
     * @param float $fzPrice
     */
    public function setFzPrice($fzPrice)
    {
        $this->fzPrice = $fzPrice;
    }

    /**
     * @return float
     */
    public function getFzPrice()
    {
        return $this->fzPrice;
    }
}
