<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AgentReport
 *
 * @ORM\Table(name="agent_report")
 * @ORM\Entity
 */
class AgentReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts", type="datetime", nullable=true)
     */
    private $ts;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_from", type="datetime", nullable=false)
     */
    private $dateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_to", type="datetime", nullable=false)
     */
    private $dateTo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currency_rate_date", type="datetime", nullable=false)
     */
    private $currencyRateDate;

    /**
     * @var string
     *
     * @ORM\Column(name="awbs", type="text", nullable=true)
     */
    private $awbs;

    /**
     * @var integer
     *
     * @ORM\Column(name="pieces", type="integer", nullable=true)
     */
    private $pieces;

    /**
     * @var float
     *
     * @ORM\Column(name="total_gross_weight", type="decimal", nullable=true)
     */
    private $totalGrossWeight;

    /**
     * @var float
     *
     * @ORM\Column(name="total_paid_weight", type="decimal", nullable=true)
     */
    private $totalPaidWeight;

    /**
     * @var float
     *
     * @ORM\Column(name="total_amount", type="decimal", nullable=true)
     */
    private $totalAmount;

    /**
     * @var float
     *
     * @ORM\Column(name="total_fee_xdc", type="decimal", nullable=true)
     */
    private $totalFeeXdc;

    /**
     * @var float
     *
     * @ORM\Column(name="total_fee_myc", type="decimal", nullable=true)
     */
    private $totalFeeMyc;

    /**
     * @var \Eprst\Bundle\AviaBundle\Entity\Agent
     *
     * @ORM\ManyToOne(targetEntity="Agent")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     */
    private $agent;

    /**
     * Set Agent
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\Agent $agent Value of agent
     *
     * @return void
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    /**
     * Get Agent
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\Agent
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set Awbs
     *
     * @param string $awbs Value of awbs
     *
     * @return void
     */
    public function setAwbs($awbs)
    {
        $this->awbs = $awbs;
    }

    /**
     * Get Awbs
     *
     * @return string
     */
    public function getAwbs()
    {
        return $this->awbs;
    }

    /**
     * Set CurrencyRateDate
     *
     * @param \DateTime $currencyRateDate Value of currencyRateDate
     *
     * @return void
     */
    public function setCurrencyRateDate($currencyRateDate)
    {
        $this->currencyRateDate = $currencyRateDate;
    }

    /**
     * Get CurrencyRateDate
     *
     * @return \DateTime
     */
    public function getCurrencyRateDate()
    {
        return $this->currencyRateDate;
    }

    /**
     * Set DateFrom
     *
     * @param \DateTime $dateFrom Value of dateFrom
     *
     * @return void
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * Get DateFrom
     *
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set DateTo
     *
     * @param \DateTime $dateTo Value of dateTo
     *
     * @return void
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }

    /**
     * Get DateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set Id
     *
     * @param int $id Value of id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Pieces
     *
     * @param int $pieces Value of pieces
     *
     * @return void
     */
    public function setPieces($pieces)
    {
        $this->pieces = $pieces;
    }

    /**
     * Get Pieces
     *
     * @return int
     */
    public function getPieces()
    {
        return $this->pieces;
    }

    /**
     * Set TotalAmount
     *
     * @param float $totalAmount Value of totalAmount
     *
     * @return void
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
    }

    /**
     * Get TotalAmount
     *
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set TotalFeeMyc
     *
     * @param float $totalFeeMyc Value of totalFeeMyc
     *
     * @return void
     */
    public function setTotalFeeMyc($totalFeeMyc)
    {
        $this->totalFeeMyc = $totalFeeMyc;
    }

    /**
     * Get TotalFeeMyc
     *
     * @return float
     */
    public function getTotalFeeMyc()
    {
        return $this->totalFeeMyc;
    }

    /**
     * Set TotalFeeXdc
     *
     * @param float $totalFeeXdc Value of totalFeeXdc
     *
     * @return void
     */
    public function setTotalFeeXdc($totalFeeXdc)
    {
        $this->totalFeeXdc = $totalFeeXdc;
    }

    /**
     * Get TotalFeeXdc
     *
     * @return float
     */
    public function getTotalFeeXdc()
    {
        return $this->totalFeeXdc;
    }

    /**
     * Set TotalGrossWeight
     *
     * @param float $totalGrossWeight Value of totalGrossWeight
     *
     * @return void
     */
    public function setTotalGrossWeight($totalGrossWeight)
    {
        $this->totalGrossWeight = $totalGrossWeight;
    }

    /**
     * Get TotalGrossWeight
     *
     * @return float
     */
    public function getTotalGrossWeight()
    {
        return $this->totalGrossWeight;
    }

    /**
     * Set TotalPaidWeight
     *
     * @param float $totalPaidWeight Value of totalPaidWeight
     *
     * @return void
     */
    public function setTotalPaidWeight($totalPaidWeight)
    {
        $this->totalPaidWeight = $totalPaidWeight;
    }

    /**
     * Get TotalPaidWeight
     *
     * @return float
     */
    public function getTotalPaidWeight()
    {
        return $this->totalPaidWeight;
    }

    /**
     * Set Ts
     *
     * @param \DateTime $ts Value of ts
     *
     * @return void
     */
    public function setTs($ts)
    {
        $this->ts = $ts;
    }

    /**
     * Get Ts
     *
     * @return \DateTime
     */
    public function getTs()
    {
        return $this->ts;
    }

}
