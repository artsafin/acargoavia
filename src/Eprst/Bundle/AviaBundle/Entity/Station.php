<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Station
 *
 * @ORM\Table(name="station")
 * @ORM\Entity(repositoryClass="Eprst\Bundle\AviaBundle\Repository\Station")
 */
class Station
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @ORM\Column(name="name", type="string", length=3, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_departure", type="boolean", nullable=false)
     */
    private $isDeparture = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_arrival", type="boolean", nullable=false)
     */
    private $isArrival = true;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=100, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=200, nullable=true)
     */
    private $description;

    /**
     * @var StationStatus
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="StationStatus")
     * @ORM\JoinColumn(name="station_status_id", referencedColumnName="id", nullable=false)
     */
    private $stationStatus;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Station
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Station
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Station
     */
    public function setCountry($country)
    {
        $this->country = $country;
    
        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Station
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set stationStatus
     *
     * @param StationStatus $stationStatus
     * @return Station
     */
    public function setStationStatus(StationStatus $stationStatus)
    {
        $this->stationStatus = $stationStatus;
    
        return $this;
    }

    /**
     * Get stationStatus
     *
     * @return StationStatus 
     */
    public function getStationStatus()
    {
        return $this->stationStatus;
    }

    /**
     * Set IsArrival
     *
     * @param boolean $isArrival Value of isArrival
     *
     * @return void
     */
    public function setIsArrival($isArrival)
    {
        $this->isArrival = $isArrival;
    }

    /**
     * Get IsArrival
     *
     * @return boolean
     */
    public function getIsArrival()
    {
        return $this->isArrival;
    }

    /**
     * Set IsDeparture
     *
     * @param boolean $isDeparture Value of isDeparture
     *
     * @return void
     */
    public function setIsDeparture($isDeparture)
    {
        $this->isDeparture = $isDeparture;
    }

    /**
     * Get IsDeparture
     *
     * @return boolean
     */
    public function getIsDeparture()
    {
        return $this->isDeparture;
    }
}