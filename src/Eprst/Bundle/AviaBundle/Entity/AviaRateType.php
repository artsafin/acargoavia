<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AviaRateType
 *
 * @ORM\Table(name="avia_rate_type")
 * @ORM\Entity(repositoryClass="Eprst\Bundle\AviaBundle\Repository\AviaRateType")
 */
class AviaRateType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Eprst\Bundle\AviaBundle\Entity\AviaRate", mappedBy="type")
     */
    private $rates;


    function __construct()
    {
        $this->rates = new ArrayCollection();
    }

    /**
     * Set Rates
     *
     * @param ArrayCollection $rates Value of rates
     *
     * @return void
     */
    public function setRates($rates)
    {
        $this->rates = $rates;
    }

    /**
     * Get Rates
     *
     * @return ArrayCollection
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * Set Id
     *
     * @param int $id Value of id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Name
     *
     * @param string $name Value of name
     *
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
