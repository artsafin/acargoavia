<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShipmentFee
 *
 * @ORM\Table(name="shipment_fee")
 * @ORM\Entity
 */
class ShipmentFee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="myc", type="decimal", nullable=false)
     */
    private $myc = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="xdc", type="decimal", nullable=false)
     */
    private $xdc = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="optional", type="decimal", nullable=false)
     */
    private $opt = 0;

    /**
     * @var \Eprst\Bundle\AviaBundle\Entity\Shipment
     *
     * @ORM\OneToOne(targetEntity="Shipment", inversedBy="fee")
     * @ORM\JoinColumn(name="shipment_id", referencedColumnName="id")
     */
    private $shipment;

    /**
     * Set Id
     *
     * @param int $id Value of id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Myc
     *
     * @param float $myc Value of myc
     *
     * @return void
     */
    public function setMyc($myc)
    {
        $this->myc = $myc;
    }

    /**
     * Get Myc
     *
     * @return float
     */
    public function getMyc()
    {
        return $this->myc;
    }

    /**
     * Set Shipment
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\Shipment $shipment Value of shipment
     *
     * @return void
     */
    public function setShipment($shipment)
    {
        $this->shipment = $shipment;
    }

    /**
     * Get Shipment
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\Shipment
     */
    public function getShipment()
    {
        return $this->shipment;
    }

    /**
     * Set Xdc
     *
     * @param float $xdc Value of xdc
     *
     * @return void
     */
    public function setXdc($xdc)
    {
        $this->xdc = $xdc;
    }

    /**
     * Get Xdc
     *
     * @return float
     */
    public function getXdc()
    {
        return $this->xdc;
    }

    /**
     * @return float
     */
    public function getOpt()
    {
        return $this->opt;
    }

    /**
     * @param float $opt
     */
    public function setOpt($opt)
    {
        $this->opt = $opt;
    }
}
