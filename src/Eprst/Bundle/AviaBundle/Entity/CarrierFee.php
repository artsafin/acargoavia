<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarrierFee
 *
 * @ORM\Table(name="carrier_fee")
 * @ORM\Entity
 */
class CarrierFee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="myc", type="decimal", nullable=false)
     */
    private $myc;

    /**
     * @var float
     *
     * @ORM\Column(name="xdc", type="decimal", nullable=false)
     */
    private $xdc;

    /**
     * @var Carrier
     *
     * @ORM\ManyToOne(targetEntity="Eprst\Bundle\AviaBundle\Entity\Carrier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     * })
     */
    private $carrier;

    /**
     * Set Carrier
     *
     * @param Carrier $carrier Value of carrier
     *
     * @return void
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;
    }

    /**
     * Get Carrier
     *
     * @return Carrier
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set Id
     *
     * @param int $id Value of id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Myc
     *
     * @param float $myc Value of myc
     *
     * @return void
     */
    public function setMyc($myc)
    {
        $this->myc = $myc;
    }

    /**
     * Get Myc
     *
     * @return float
     */
    public function getMyc()
    {
        return $this->myc;
    }

    /**
     * Set Xdc
     *
     * @param float $xdc Value of xdc
     *
     * @return void
     */
    public function setXdc($xdc)
    {
        $this->xdc = $xdc;
    }

    /**
     * Get Xdc
     *
     * @return float
     */
    public function getXdc()
    {
        return $this->xdc;
    }
}
