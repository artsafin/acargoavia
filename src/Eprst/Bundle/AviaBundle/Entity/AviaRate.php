<?php

namespace Eprst\Bundle\AviaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Eprst\Bundle\AviaBundle\Entity\Station;

/**
 * AviaRate
 *
 * @ORM\Table(name="avia_rate")
 * @ORM\Entity(repositoryClass="Eprst\Bundle\AviaBundle\Repository\AviaRate")
 */
class AviaRate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts", type="datetime", nullable=false)
     */
    private $ts;

    /**
     * @var float
     *
     * @ORM\Column(name="min", type="decimal", nullable=true)
     */
    private $min;

    /**
     * @var float
     *
     * @ORM\Column(name="n", type="decimal", nullable=true)
     */
    private $n;

    /**
     * @var float
     *
     * @ORM\Column(name="range_45", type="decimal", nullable=true)
     */
    private $range45;

    /**
     * @var float
     *
     * @ORM\Column(name="range_100", type="decimal", nullable=true)
     */
    private $range100;

    /**
     * @var float
     *
     * @ORM\Column(name="range_300", type="decimal", nullable=true)
     */
    private $range300;

    /**
     * @var float
     *
     * @ORM\Column(name="range_500", type="decimal", nullable=true)
     */
    private $range500;

    /**
     * @var float
     *
     * @ORM\Column(name="range_1000", type="decimal", nullable=true)
     */
    private $range1000;

    /**
     * @var Station
     *
     * @ORM\ManyToOne(targetEntity="Eprst\Bundle\AviaBundle\Entity\Station")
     * @ORM\JoinColumn(name="arrival_station_id", referencedColumnName="id")
     */
    private $arrivalStation;

    /**
     * @var AviaRateType
     *
     * @ORM\ManyToOne(targetEntity="Eprst\Bundle\AviaBundle\Entity\AviaRateType", inversedBy="rates")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->agent = new ArrayCollection();
        $this->carrier = new ArrayCollection();
    }

    /**
     * Set ArrivalStation
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\Station $arrivalStation Value of arrivalStation
     *
     * @return void
     */
    public function setArrivalStation($arrivalStation)
    {
        $this->arrivalStation = $arrivalStation;
    }

    /**
     * Get ArrivalStation
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\Station
     */
    public function getArrivalStation()
    {
        return $this->arrivalStation;
    }

    /**
     * Set Type
     *
     * @param \Eprst\Bundle\AviaBundle\Entity\AviaRateType $type Value of type
     *
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get Type
     *
     * @return \Eprst\Bundle\AviaBundle\Entity\AviaRateType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set Id
     *
     * @param int $id Value of id
     *
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Min
     *
     * @param float $min Value of min
     *
     * @return void
     */
    public function setMin($min)
    {
        $this->min = $min;
    }

    /**
     * Get Min
     *
     * @return float
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Set N
     *
     * @param float $n Value of n
     *
     * @return void
     */
    public function setN($n)
    {
        $this->n = $n;
    }

    /**
     * Get N
     *
     * @return float
     */
    public function getN()
    {
        return $this->n;
    }

    /**
     * Set Range100
     *
     * @param float $range100 Value of range100
     *
     * @return void
     */
    public function setRange100($range100)
    {
        $this->range100 = $range100;
    }

    /**
     * Get Range100
     *
     * @return float
     */
    public function getRange100()
    {
        return $this->range100;
    }

    /**
     * Set Range1000
     *
     * @param float $range1000 Value of range1000
     *
     * @return void
     */
    public function setRange1000($range1000)
    {
        $this->range1000 = $range1000;
    }

    /**
     * Get Range1000
     *
     * @return float
     */
    public function getRange1000()
    {
        return $this->range1000;
    }

    /**
     * Set Range300
     *
     * @param float $range300 Value of range300
     *
     * @return void
     */
    public function setRange300($range300)
    {
        $this->range300 = $range300;
    }

    /**
     * Get Range300
     *
     * @return float
     */
    public function getRange300()
    {
        return $this->range300;
    }

    /**
     * Set Range45
     *
     * @param float $range45 Value of range45
     *
     * @return void
     */
    public function setRange45($range45)
    {
        $this->range45 = $range45;
    }

    /**
     * Get Range45
     *
     * @return float
     */
    public function getRange45()
    {
        return $this->range45;
    }

    /**
     * Set Range500
     *
     * @param float $range500 Value of range500
     *
     * @return void
     */
    public function setRange500($range500)
    {
        $this->range500 = $range500;
    }

    /**
     * Get Range500
     *
     * @return float
     */
    public function getRange500()
    {
        return $this->range500;
    }

    /**
     * Set Ts
     *
     * @param \DateTime $ts Value of ts
     *
     * @return void
     */
    public function setTs($ts)
    {
        $this->ts = $ts;
    }

    /**
     * Get Ts
     *
     * @return \DateTime
     */
    public function getTs()
    {
        return $this->ts;
    }
}
