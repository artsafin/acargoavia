<?php

namespace Eprst\Bundle\AviaBundle\Controller;

use Eprst\Bundle\AcargoBundle\Controller\CrudController;
use Eprst\Bundle\AviaBundle\Entity\AgentPeople as AgentPeopleEntity;
use Eprst\Bundle\AviaBundle\Entity\Agent as AgentEntity;

class AgentPeopleController extends CrudController
{
    protected $templates = array(
        'create' => 'EprstAviaBundle:AgentPeople:add.html.twig',
        'read'   => 'EprstAviaBundle:AgentPeople:list.html.twig',
        'update' => 'EprstAviaBundle:AgentPeople:edit.html.twig',
        'delete' => 'EprstAviaBundle:AgentPeople:delete.html.twig'
    );
    protected $updateFormAlias = 'aviabundle_agentpeople';

    protected function getRepository()
    {
        return $this->getEntityManager()->getRepository('EprstAviaBundle:AgentPeople');
    }

    protected function createNewEntity()
    {
        return new AgentPeopleEntity();
    }

    /**
     * @param AgentPeopleEntity $entity
     *
     * @return string
     */
    protected function getUrlRead($entity)
    {
        return $this->generateUrl('avia_agent_people', array('agent' => $entity->getAgent()->getId()));
    }

    protected function getUrlUpdate($entity)
    {
        return $this->generateUrl('avia_agent_people_persist', array('id' => $entity->getId()));
    }

    protected function getUrlDeletePersist($id)
    {
        return $this->generateUrl(
            'avia_agent_people_delete_persist',
            array('id' => $id)
        );
    }

    public function listAction($agent = null)
    {
        $agentEntity = $this->getAgentEntity($agent);

        if (empty($agent))
        {
            $items = $this->getRepository()->findAll();
        }
        else
        {
            $items = $this->getRepository()->findBy(array('agent' => $agent));
        }

        return $this->render(
            $this->getTemplateName('read'),
            array('items' => $items, 'agent' => $agentEntity)
        );
    }

    public function addAction($agent = null)
    {
        $entity = $this->createNewEntity();

        if (!empty($agent))
        {
            $agentEntity = $this->getAgentEntity($agent);
            $entity->setAgent($agentEntity);
        }

        return $this->render(
            $this->getTemplateName('create'),
            array('form' => $this->createEntityForm($entity)->createView())
        );
    }

    private function getAgentEntity($id)
    {
        $agentEntity = new AgentEntity();
        if (!empty($id)) {
            $agentEntity = $this->loadAgentEntity($id);
        }

        return $agentEntity;
    }

    private function loadAgentEntity($id)
    {
        $agentRepo = $this->getEntityManager()->getRepository('EprstAviaBundle:Agent');
        return $agentRepo->find($id);
    }
}
