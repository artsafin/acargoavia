<?php

namespace Eprst\Bundle\AviaBundle\Controller;

use Eprst\Bundle\AviaBundle\Entity\Shipment as ShipmentEntity;
use Eprst\Bundle\AviaBundle\Repository\Shipment as ShipmentRepository;
use Eprst\Bundle\AviaBundle\Service\ExcelStationReportBuilder;
use Eprst\Bundle\AviaBundle\Service\Report\ReportResponseBuilderInterface;
use Eprst\Bundle\AviaBundle\Service\ShipmentEntityMapper;
use Eprst\Bundle\AviaBundle\Entity\Station as StationEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * StationReportController
 * 
 */
class StationReportController extends Controller
{
    private function createReportFilterForm()
    {
        $options = array();
        $form = $this->createForm('aviabundle_report_station', null, $options);

        return $form;
    }

    /**
     * @param $station
     * @param $dateCriteriaField
     * @param $dateFrom
     * @param $dateTo
     *
     * @return ShipmentEntity[]
     */
    private function loadReport($station, $dateCriteriaField, $dateFrom, $dateTo)
    {
        /** @var ShipmentRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('EprstAviaBundle:Shipment');

        $entities = $repo->findByDatesAndStation($station, $dateCriteriaField, $dateFrom, $dateTo);

        return $entities;
    }

    public function indexAction(Request $request)
    {
        $form = $this->createReportFilterForm();

        $form->handleRequest($request);

        $reportItems = $totalValue = null;

	    /** @var ShipmentEntityMapper $shipmentMapper */
	    $shipmentMapper = $this->container->get('eprst_avia.shipment_mapper');

        if ($form->isValid() && ($form->get('show')->isClicked() || $form->get('export')->isClicked()))
        {
            $dateType = $form->get('date_type')->getData();

            $dateCriteriaField =
                    in_array($dateType, array('dateWaybill', 'dateFactDeparture')) ? $dateType : 'dateWaybill';

            $station = $form->get('station')->getData();

            $dateFrom = $form->get('from')->getData();
            $dateTo = $form->get('to')->getData();

            $reportItems = $this->loadReport($station, $dateCriteriaField, $dateFrom, $dateTo);

            if ($form->get('export')->isClicked())
            {
                return $this->createExcelResponse($station, $dateFrom, $dateTo, $reportItems);
            }

	        $totalValue = array_reduce($reportItems, function(&$acc, $item) use ($shipmentMapper){
		        $acc = $acc + $shipmentMapper->get($item, 'Carrier total');
		        return $acc;
	        }, 0);
        }

        return $this->render(
            'EprstAviaBundle:StationReport:index.html.twig',
            array(
                 'filterForm' => $form->createView(),
                 'items' => $reportItems,
	             'itemsTotal' => $totalValue,
                 'mapper' => $shipmentMapper,
                 'lang' => 'ru'
            )
        );
    }

    /**
     * @param StationEntity    $station
     * @param                  $dateFrom
     * @param                  $dateTo
     * @param ShipmentEntity[] $items
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createExcelResponse($station, $dateFrom, $dateTo, $items)
    {
        /** @var ReportResponseBuilderInterface $reportBuilder */
        $reportBuilder = $this->container->get('eprst_avia.report.excel.station');

        $response = $reportBuilder->build(
            $items,
            array('stationName' => $station->getName(), 'dateFrom' => $dateFrom, 'dateTo' => $dateTo)
        );

        $fileName = $this->getReportFileName($dateFrom, $dateTo);
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $fileName . '"');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    /**
     * getReportFileName
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return string
     */
    private function getReportFileName($dateFrom, $dateTo)
    {
        return sprintf('station_report_%s_%s.xls', $dateFrom->format('dmY'), $dateTo->format('dmY'));
    }
}
