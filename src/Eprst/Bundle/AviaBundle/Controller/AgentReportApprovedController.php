<?php

namespace Eprst\Bundle\AviaBundle\Controller;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityRepository;
use Eprst\Bundle\AcargoBundle\Controller\CrudController;
use Eprst\Bundle\AviaBundle\Entity\AgentReport;
use Eprst\Bundle\AviaBundle\Service\Report\ReportResponseBuilderInterface;
use Eprst\Bundle\CurrencyBundle\Service\CurrencyService;
use Symfony\Component\HttpFoundation\Request;
use Eprst\Bundle\AviaBundle\Entity\AgentReport as AgentReportEntity;
use Eprst\Bundle\AviaBundle\Entity\Shipment as ShipmentEntity;
use Eprst\Bundle\AviaBundle\Entity\Agent as AgentEntity;
use Eprst\Bundle\AviaBundle\Service\ShipmentEntityMapper;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Eprst\Bundle\AviaBundle\Repository\Shipment as ShipmentRepository;

class AgentReportApprovedController extends CrudController
{
    protected $templates = array(
        'read'   => 'EprstAviaBundle:AgentReportApproved:list.html.twig',
        'delete' => 'EprstAviaBundle:AgentReportApproved:delete.html.twig'
    );

    public function listAction()
    {
        return $this->render(
            $this->getTemplateName('read'),
            array('items' => $this->getRepository()->findBy(array(), array('dateTo' => 'DESC')))
        );
    }

    /**
     * @return object
     */
    protected function createNewEntity()
    {
        return null;
    }

    /**
     * @param object $entity
     *
     * @return string
     */
    protected function getUrlRead($entity)
    {
        return $this->generateUrl('avia_report_agent_saved');
    }

    /**
     * @param object $entity
     *
     * @return string
     */
    protected function getUrlUpdate($entity)
    {
        return null;
    }

    /**
     * @param $id
     *
     * @return string
     */
    protected function getUrlDeletePersist($id)
    {
        return $this->generateUrl(
            'avia_report_agent_saved_delete_persist',
            array('id' => $id)
        );
    }

    /**
     * @return CurrencyService
     */
    private function getCurrencyService()
    {
        return $this->container->get('eprst_currency.service.currency');
    }

    /**
     * @return ObjectRepository
     */
    protected function getRepository()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EntityRepository $repo */
        return $em->getRepository('EprstAviaBundle:AgentReport');
    }

    private function loadReportEntity($id)
    {
        $repo = $this->getRepository();

        return $repo->find($id);
    }

    /**
     * @param $agent
     * @param $dateCriteriaField
     * @param $dateFrom
     * @param $dateTo
     *
     * @return ShipmentEntity[]
     */
    private function loadReport($agent, $dateCriteriaField, $dateFrom, $dateTo)
    {
        /** @var ShipmentRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('EprstAviaBundle:Shipment');

        $entities = $repo->findByDatesAndAgent($agent, $dateCriteriaField, $dateFrom, $dateTo);

        return $entities;
    }

    public function showAction($reportId, Request $request)
    {
        /** @var AgentReportEntity $reportEntity */
        $reportEntity = $this->loadReportEntity($reportId);

        /** @var ShipmentEntityMapper $shipmentMapper */
        $shipmentMapper = $this->container->get('eprst_avia.shipment_mapper');

        $simpleActionsForm = $this->createForm('aviabundle_report_agent_saved_action_simple');
        $invoiceActionForm = $this->createForm('aviabundle_report_agent_saved_action_invoice');
        $exportActionForm = $this->createForm('aviabundle_report_agent_saved_action_export');

        $simpleActionsForm->handleRequest($request);
        $invoiceActionForm->handleRequest($request);
        $exportActionForm->handleRequest($request);

        if ($invoiceActionForm->isSubmitted() && $invoiceActionForm->get('invoice')->isClicked()) {
            $isSigned = $invoiceActionForm->get('signed')->getData();

            $invoiceNumber = $invoiceActionForm->get('invoiceNumber')->getData();

            if ($isSigned) {
                return $this->createPdfInvoiceResponse($invoiceNumber, $reportEntity, $isSigned, $reportId);
            } else {
                return $this->createWordInvoiceResponse($invoiceNumber, $reportEntity, $isSigned, $reportId);
            }
        }

        if ($simpleActionsForm->isSubmitted()
                && $simpleActionsForm->isValid()
                && $simpleActionsForm->get('delete')->isClicked()
        ) {
            return $this->redirect(
                $this->generateUrl('avia_report_agent_saved_delete', array('id' => $reportId))
            );
        }

        $currencyRate = $this->getCurrencyService()->getRateForDate($reportEntity->getCurrencyRateDate());

        $reportItems = $this->loadReport(
            $reportEntity->getAgent(),
            AgentReportController::DATE_CRITERIA_FIELD,
            $reportEntity->getDateFrom(),
            $reportEntity->getDateTo()
        );

        if ($exportActionForm->isSubmitted()
                && $exportActionForm->isValid()
                && $exportActionForm->get('export')->isClicked()
        ) {
            $isSigned = $exportActionForm->get('signed')->getData();

            return $this->createExcelExportResponse(
                $reportEntity->getAgent(),
                $reportItems,
                $currencyRate,
                $reportEntity->getDateFrom(),
                $reportEntity->getDateTo(),
                $isSigned
            );
        }

	    $totalAgentTotal = array_reduce($reportItems, function (&$acc, $item) use ($shipmentMapper)
	    {
		    $acc = $acc + $shipmentMapper->get($item, 'Agent total');
		    return $acc;
	    }, 0);
        $totalAgentTotalRub = array_reduce($reportItems, function (&$acc, $item) use ($shipmentMapper, $currencyRate)
	    {
		    $acc = $acc + $shipmentMapper->get($item, 'Agent total conv', $currencyRate);
		    return $acc;
	    }, 0);

        return $this->render(
            'EprstAviaBundle:AgentReportApproved:show.html.twig',
            array(
	             'agentName' => $reportEntity->getAgent()->getNameOfficial(),
	             'currencyRate' => $currencyRate,
                 'formSimple'   => $simpleActionsForm->createView(),
                 'formInvoice'  => $invoiceActionForm->createView(),
                 'formExport'   => $exportActionForm->createView(),
	             'total_Agent_total' => $totalAgentTotal,
	             'total_Agent_total_RUB' => $totalAgentTotalRub,
                 'items'        => $reportItems,
                 'mapper'       => $shipmentMapper,
                 'lang'         => 'ru'
            )
        );
    }

    /**
     * createPdfInvoiceResponse
     *
     * @param $invoiceNumber
     * @param AgentReport $reportEntity
     * @param $isSigned
     * @param $reportId
     *
     * @return StreamedResponse
     */
    private function createPdfInvoiceResponse($invoiceNumber, $reportEntity, $isSigned, $reportId)
    {
        $html   = $this->renderView(
            'EprstAviaBundle:AgentReportApproved:invoice.html.twig',
            array(
                 'signed'        => $isSigned,
                 'invoiceNumber' => $invoiceNumber,
                 'entity'        => $reportEntity
            )
        );
        $dompdf = $this->get('slik_dompdf');

        // Generate the pdf
        $dompdf->getpdf($html);

        $fileName = $this->getInvoiceFileName('pdf', $reportEntity->getAgent()->getName(), $invoiceNumber, $reportId);

        return new StreamedResponse(function () use ($dompdf, $fileName) {
            $dompdf->stream($fileName);
        });
    }

    /**
     * createWordInvoiceResponse
     *
     * @param $invoiceNumber
     * @param AgentReport $reportEntity
     * @param $isSigned
     * @param $reportId
     *
     * @return Response
     */
    private function createWordInvoiceResponse($invoiceNumber, $reportEntity, $isSigned, $reportId)
    {
        $xml = $this->renderView(
            'EprstAviaBundle:AgentReportApproved:invoice.xml.twig',
            array(
                 'signed'        => $isSigned,
                 'invoiceNumber' => $invoiceNumber,
                 'entity'        => $reportEntity
            )
        );

        $response = new Response($xml);

        $fileName = $this->getInvoiceFileName('xml', $reportEntity->getAgent()->getName(), $invoiceNumber, $reportId);
        $response->headers->set('Content-Type', 'application/msword; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $fileName . '"');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    /**
     * @param AgentEntity      $agent
     * @param ShipmentEntity[] $items
     *
     * @param                  $currencyRate
     * @param                  $dateFrom
     * @param                  $dateTo
     * @param bool             $isSigned
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createExcelExportResponse($agent, $items, $currencyRate, $dateFrom, $dateTo, $isSigned = false)
    {
        /** @var ReportResponseBuilderInterface $reportBuilder */
        $reportBuilder = $this->container->get('eprst_avia.report.excel.agent');

        $response = $reportBuilder->build(
            $items,
            array(
                 'agentName'    => $agent->getNameOfficial(),
                 'currencyRate' => $currencyRate,
                 'dateFrom'     => $dateFrom,
                 'dateTo'       => $dateTo,
                 'isSigned'     => $isSigned
            )
        );

        $fileName = $this->getExportReportFileName($agent, $dateFrom, $dateTo);
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $fileName . '"');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

	/**
	 * getReportFileName
	 *
	 * @param AgentEntity $agent
	 * @param \DateTime $dateFrom
	 * @param \DateTime $dateTo
	 *
	 * @return string
	 */
    private function getExportReportFileName($agent, $dateFrom, $dateTo)
    {
        return sprintf('%s_Report_%s_%s.xls', $agent->getNameFile(), $dateFrom->format('dmY'), $dateTo->format('dmY'));
    }

    private function getInvoiceFileName($extension, $agentName, $invoiceNumber, $reportId)
    {
        $agentName = str_replace(array('/', '\\', ), '', $agentName);
        $now      = new \DateTime();
        $fileName = sprintf("%s %s.%s", $agentName, $invoiceNumber, $extension);

        return $fileName;
    }
}
