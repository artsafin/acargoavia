<?php

namespace Eprst\Bundle\AviaBundle\Controller;

use Eprst\Bundle\AcargoBundle\Controller\CrudController;
use Eprst\Bundle\AviaBundle\Entity\Agent as AgentEntity;
use Eprst\Bundle\AviaBundle\Repository\AgentStatus;

class AgentController extends CrudController
{
    protected $templates = array(
        'create' => 'EprstAviaBundle:Agent:add.html.twig',
        'read'   => 'EprstAviaBundle:Agent:list.html.twig',
        'update' => 'EprstAviaBundle:Agent:edit.html.twig',
        'delete' => 'EprstAviaBundle:Agent:delete.html.twig'
    );
    protected $updateFormAlias = 'aviabundle_agent';

    protected function getRepository()
    {
        return $this->getEntityManager()->getRepository('EprstAviaBundle:Agent');
    }

    /**
     * persistDeleteEntity
     *
     * @param AgentEntity $entity
     *
     * @return void
     */
    protected function persistDeleteEntity($entity)
    {
        $em                 = $this->getEntityManager();
        $statusRepo = $em->getRepository('EprstAviaBundle:AgentStatus');
        $deletedStatus      = $statusRepo->find(AgentStatus::STATUS_DELETED);
        $entity->setStatus($deletedStatus);
        $em->persist($entity);
    }

    public function listAction()
    {
        $criteria = array('status' => array(AgentStatus::STATUS_ENABLED));
        return $this->render($this->getTemplateName('read'),
                             array('items' => $this->getRepository()->findBy($criteria)));
    }

    protected function createNewEntity()
    {
        $entity = new AgentEntity();

        $statusRepo    = $this->getEntityManager()->getRepository('EprstAviaBundle:AgentStatus');
        $enabledStatus = $statusRepo->find(AgentStatus::STATUS_ENABLED);
        $entity->setStatus($enabledStatus);

        return $entity;
    }

    protected function getUrlRead($entity)
    {
        return $this->generateUrl('avia_agent');
    }

    protected function getUrlUpdate($entity)
    {
        return $this->generateUrl('avia_agent_persist', array('id' => $entity->getId()));
    }

    protected function getUrlDeletePersist($id)
    {
        return $this->generateUrl(
            'avia_agent_delete_persist',
            array('id' => $id)
        );
    }
}
