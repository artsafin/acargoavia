<?php

namespace Eprst\Bundle\AviaBundle\Controller;
use Doctrine\ORM\NoResultException;
use Eprst\Bundle\AviaBundle\Entity\Shipment as ShipmentEntity;
use Eprst\Bundle\AviaBundle\Repository\Shipment as ShipmentRepository;
use Eprst\Bundle\AviaBundle\Service\Report\ReportResponseBuilderInterface;
use Eprst\Bundle\AviaBundle\Service\ShipmentEntityMapper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Eprst\Bundle\AviaBundle\Entity\BankReport as BankReportEntity;
use Eprst\Bundle\AviaBundle\Repository\BankReport as BankReportRepository;

/**
 * CarrierReportController
 * 
 */
class CarrierReportController extends Controller
{
    private function createReportFilterForm($actionRouteName)
    {
        $options = array(
            'action' => $this->generateUrl($actionRouteName)
        );
        $form = $this->createForm('aviabundle_report_bank', null, $options);

        return $form;
    }

    /**
     * @param $dateCriteriaField
     * @param $dateFrom
     * @param $dateTo
     *
     * @return ShipmentEntity[]
     */
    private function loadReport($dateCriteriaField, $dateFrom, $dateTo)
    {
        /** @var ShipmentRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('EprstAviaBundle:Shipment');

        $entities = $repo->findByDates($dateCriteriaField, $dateFrom, $dateTo);

        return $entities;
    }

    public function indexAction(Request $request)
    {
        $lang = 'en';
        $form = $this->createReportFilterForm('avia_report_carrier');

        $form->handleRequest($request);

        $reportItems = $prevPeriodTotalAgentFee = null;

        if ($form->isValid() && ($form->get('show')->isClicked() || $form->get('export')->isClicked()))
        {
            $dateType = $form->get('date_type')->getData();

            $dateCriteriaField = in_array($dateType, array('dateWaybill', 'dateFactDeparture'))?$dateType:'dateWaybill';

            $dateFrom = $form->get('from')->getData();
            $dateTo = $form->get('to')->getData();

            $reportItems = $this->loadReport($dateCriteriaField, $dateFrom, $dateTo);
	        $prevPeriodTotalAgentFee = $this->getTotalAgentFeeBeforeDate($dateFrom);

	        if ($form->get('export')->isClicked())
            {
                return $this->createExcelResponse($dateFrom, $dateTo, $reportItems);
            }
        }

        /** @var ShipmentEntityMapper $shipmentMapper */
        $shipmentMapper = $this->container->get('eprst_avia.shipment_mapper');

        return $this->render(
            'EprstAviaBundle:BankReport:index.html.twig',
            array(
                 'filterForm' => $form->createView(),
                 'items' => $reportItems,
	             'prevPeriodTotalAgentFee' => $prevPeriodTotalAgentFee,
                 'mapper' => $shipmentMapper,
                 'lang' => $lang
            )
        );
    }

	private function getTotalAgentFeeBeforeDate($date)
	{
		$em = $this->getDoctrine()->getManager();

		/** @var BankReportRepository $bankReportRepository */
		$bankReportRepository = $em->getRepository('EprstAviaBundle:BankReport');

		try
		{
			/** @var BankReportEntity $bankReport */
			$bankReport = $bankReportRepository->getReportBefore($date);
		} catch (NoResultException $exc)
		{
			return 0;
		}

		return $bankReport->getTotalAgentFee();
	}

    /**
     * @param                  $dateFrom
     * @param                  $dateTo
     * @param ShipmentEntity[] $items
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createExcelResponse($dateFrom, $dateTo, $items)
    {
        /** @var ReportResponseBuilderInterface $reportBuilder */
        $reportBuilder = $this->container->get('eprst_avia.report.excel.carrier');

        $response = $reportBuilder->build($items, array('dateFrom' => $dateFrom, 'dateTo' => $dateTo));

        $fileName = $this->getReportFileName($dateFrom, $dateTo);
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=' . $fileName);
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    /**
     * getReportFileName
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return string
     */
    private function getReportFileName($dateFrom, $dateTo)
    {
        return sprintf('carrier_report_%s_%s.xls', $dateFrom->format('dmY'), $dateTo->format('dmY'));
    }
}
