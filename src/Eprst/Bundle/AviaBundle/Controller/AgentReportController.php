<?php

namespace Eprst\Bundle\AviaBundle\Controller;

use Eprst\Bundle\AviaBundle\Entity\AgentReport as AgentReportEntity;
use Eprst\Bundle\AviaBundle\Entity\Shipment as ShipmentEntity;
use Eprst\Bundle\AviaBundle\Repository\Shipment as ShipmentRepository;
use Eprst\Bundle\AviaBundle\Service\ExcelAgentReportBuilder;
use Eprst\Bundle\AviaBundle\Entity\Agent as AgentEntity;
use Eprst\Bundle\AviaBundle\Service\Report\ReportResponseBuilderInterface;
use Eprst\Bundle\AviaBundle\Service\ShipmentEntityMapper;
use Eprst\Bundle\CurrencyBundle\Service\CurrencyService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * AgentReportController
 * 
 */
class AgentReportController extends Controller
{
    const DATE_CRITERIA_FIELD = 'dateFactDeparture';

    /**
     * @return CurrencyService
     */
    private function getCurrencyService()
    {
        return $this->container->get('eprst_currency.service.currency');
    }

    private function createReportFilterForm()
    {
        $options = array();
        $form = $this->createForm('aviabundle_report_agent', null, $options);

        $form->get('currencyRateDate')->setData($this->getCurrencyService()->getLastRateTimestamp());

        return $form;
    }

    /**
     * @param $agent
     * @param $dateCriteriaField
     * @param $dateFrom
     * @param $dateTo
     *
     * @return ShipmentEntity[]
     */
    private function loadReport($agent, $dateCriteriaField, $dateFrom, $dateTo)
    {
        /** @var ShipmentRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('EprstAviaBundle:Shipment');

        $entities = $repo->findByDatesAndAgent($agent, $dateCriteriaField, $dateFrom, $dateTo);

        return $entities;
    }

    public function indexAction(Request $request)
    {
        $form = $this->createReportFilterForm();

        $form->handleRequest($request);

        /** @var ShipmentEntityMapper $shipmentMapper */
        $shipmentMapper = $this->container->get('eprst_avia.shipment_mapper');

        if ($form->isValid())
        {
            $dateCriteriaField = self::DATE_CRITERIA_FIELD;

            $agent = $form->get('agent')->getData();
            $currencyRateDate = $form->get('currencyRateDate')->getData();
            $dateFrom = $form->get('dateFrom')->getData();
            $dateTo = $form->get('dateTo')->getData();

            $reportItems = $this->loadReport($agent, $dateCriteriaField, $dateFrom, $dateTo);

            $currencyRate = $this->getCurrencyService()->getRateForDate($currencyRateDate);

            switch (true)
            {
                case $form->get('export')->isClicked():
                    return $this->createExcelResponse($agent, $reportItems, $currencyRate, $dateFrom, $dateTo);
                case $form->get('save')->isClicked():
                    $agentReportEntity = $this->approveReport($agent, $reportItems, $currencyRate, $currencyRateDate, $dateFrom, $dateTo);
                    return $this->redirect(
                        $this->generateUrl(
                            'avia_report_agent_show',
                            array('reportId' => $agentReportEntity->getId())
                        )
                    );
                default:
                case $form->get('show')->isClicked():
					$totalAgentTotal = array_reduce($reportItems, function(&$acc, $item) use($shipmentMapper){
						$acc = $acc + $shipmentMapper->get($item, 'Agent total');
						return $acc;
					}, 0);
                    $totalAgentTotalRub = array_reduce($reportItems,
                    function (&$acc, $item) use ($shipmentMapper, $currencyRate) {
                        $acc = $acc + $shipmentMapper->get($item, 'Agent total conv', $currencyRate);

                        return $acc;
                    },
                                                       0);
                    return $this->render(
                        'EprstAviaBundle:AgentReport:index.html.twig',
                        array(
                             'currencyRate' => $currencyRate,
                             'filterForm'   => $form->createView(),
                             'items'        => $reportItems,
                             'mapper'       => $shipmentMapper,
	                         'total_Agent_total' => $totalAgentTotal,
	                         'total_Agent_total_RUB' => $totalAgentTotalRub,
                             'lang'         => 'ru',
                             'submitted'    => true,
                  	         'agentName' => $agent->getNameOfficial(),
                            )
                    );
            }
        }
        else
        {
            return $this->render(
                'EprstAviaBundle:AgentReport:index.html.twig',
                array(
                     'filterForm'   => $form->createView(),
                     'lang'         => 'ru',
                     'submitted'  => false
                )
            );
        }
    }

    private function approveReport($agent, $reportItems, $currencyRate, $currencyRateDate, $dateFrom, $dateTo)
    {
        $agentReportEntity = new AgentReportEntity();
        $agentReportEntity->setAgent($agent);
        $agentReportEntity->setDateFrom($dateFrom);
        $agentReportEntity->setDateTo($dateTo);
        $agentReportEntity->setCurrencyRateDate($currencyRateDate);

        /** @var ShipmentEntityMapper $mapper */
        $mapper = $this->container->get('eprst_avia.shipment_mapper');

        $totals = array_reduce($reportItems, function(&$acc, ShipmentEntity $shipment) use ($currencyRate, $mapper){

            $acc['awbs'][] = $shipment->getFullAirWaybill();
            $acc['pieces']           += $mapper->get($shipment, 'Number of pieces');
            $acc['totalFeeMyc']      += $mapper->get($shipment, 'Fuel charge conv', $currencyRate);
            $acc['totalFeeXdc']      += $mapper->get($shipment, 'Security charge conv', $currencyRate);
            $acc['totalGrossWeight'] += $mapper->get($shipment, 'Gross weight');
            $acc['totalPaidWeight']  += $mapper->get($shipment, 'Carrier paid weight');
            $acc['totalAmount']      += $mapper->get($shipment, 'Agent amount conv', $currencyRate);

                return $acc;
            }, array(
                    'awbs'             => array(),
                    'pieces'           => 0,
                    'totalAmount'      => 0,
                    'totalFeeMyc'      => 0,
                    'totalFeeXdc'      => 0,
                    'totalGrossWeight' => 0,
                    'totalPaidWeight'  => 0,
               ));

        $totals['awbs'] = implode(', ', $totals['awbs']);

        foreach ($totals as $k => $v) {
            $agentReportEntity->{'set'.ucfirst($k)}($v);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($agentReportEntity);
        $em->flush();

        return $agentReportEntity;
    }

    /**
     * @param AgentEntity      $agent
     * @param                  $currencyRate
     * @param                  $dateFrom
     * @param                  $dateTo
     * @param ShipmentEntity[] $items
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createExcelResponse($agent, $items, $currencyRate, $dateFrom, $dateTo)
    {
        /** @var ReportResponseBuilderInterface $reportBuilder */
        $reportBuilder = $this->container->get('eprst_avia.report.excel.agent');

        $response = $reportBuilder->build($items,
                                          array(
                                               'agentName' => $agent->getNameOfficial(),
                                               'currencyRate'=> $currencyRate,
                                               'dateFrom'    => $dateFrom,
                                               'dateTo'      => $dateTo
                                          ));

        $fileName = $this->getReportFileName($dateFrom, $dateTo);
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $fileName . '"');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    /**
     * getReportFileName
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return string
     */
    private function getReportFileName($dateFrom, $dateTo)
    {
        return sprintf('agent_report_%s_%s.xls', $dateFrom->format('dmY'), $dateTo->format('dmY'));
    }
}
