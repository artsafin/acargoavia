<?php

namespace Eprst\Bundle\AviaBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query as DoctrineQuery;
use Eprst\Bundle\AviaBundle\Entity\AviaRateType as AviaRateTypeEntity;
use Eprst\Bundle\AviaBundle\Repository\AviaRate as AviaRateRepository;
use Eprst\Bundle\AviaBundle\Repository\AviaRateType as AviaRateTypeRepository;
use Eprst\Bundle\AviaBundle\Entity\Station as StationEntity;
use Eprst\Bundle\AviaBundle\Repository\Station as StationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RatesController extends Controller
{
    public function indexAction($type)
    {
        $form = $this->createForm('aviabundle_aviarate_values');

        return $this->render(
            'EprstAviaBundle:Rates:index.html.twig',
            array(
                 'type' => $type,
            )
        );
    }

    /**
     * @param string $type {carrier|agent}
     *
     * @return AviaRateTypeEntity
     */
    private function loadTypeEntity($type)
    {
        $map = array(
            'carrier' => AviaRateTypeRepository::TYPE_CARRIER,
            'agent' => AviaRateTypeRepository::TYPE_AGENT,
        );

        /** @var AviaRateTypeRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('EprstAviaBundle:AviaRateType');
        $entity = $repo->find($map[$type]);

        return $entity;
    }



    public function crudReadAction($type)
    {
        /** @var AviaRateRepository $rateRepo */
        $rateRepo = $this->getDoctrine()->getManager()->getRepository('EprstAviaBundle:AviaRate');

        $typeEntity = $this->loadTypeEntity($type);
        $entities = $rateRepo->findByType($typeEntity);

        $arrayData = array_map(array($rateRepo, 'toArray'), $entities);
        return new JsonResponse(array(
                                     'root' => $arrayData,
                                     'total' => count($arrayData)
                                ));
    }

    public function crudUpdateAction(Request $request)
    {
        $jsonRequestText = $request->getContent();
        $jsonRequest = json_decode($jsonRequestText, true);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var AviaRateRepository $rateRepo */
        $rateRepo = $em->getRepository('EprstAviaBundle:AviaRate');

        $result = array();

        if (is_array($jsonRequest)) {

            foreach ($jsonRequest as $row) {
                $rateEntity = $rateRepo->fromArray($row);
                if (!$rateEntity) {
                    continue;
                }
                $em->persist($rateEntity);

                $result[] = $row;
            }

            $em->flush();
        }

        return new JsonResponse($result);
    }

    public function crudDestroyAction(Request $request)
    {
        $jsonRequestText = $request->getContent();
        $jsonRequest     = json_decode($jsonRequestText, true);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var AviaRateRepository $rateRepo */
        $rateRepo = $em->getRepository('EprstAviaBundle:AviaRate');

        $result = array();

        if (is_array($jsonRequest)) {

            foreach ($jsonRequest as $row) {
                if (!isset($row['id'])) {
                    continue;
                }

                $entity = $rateRepo->find($row['id']);

                if (!$entity) {
                    continue;
                }

                $em->remove($entity);

                $result[] = $row;
            }

            $em->flush();
        }

        return new JsonResponse($result);
    }

    public function crudCreateAction($type, Request $request)
    {
        $jsonRequestText = $request->getContent();
        $jsonRequest     = json_decode($jsonRequestText, true);

        $resultPlain = array();

        if (is_array($jsonRequest)) {

            $rateTypeEntity = $this->loadTypeEntity($type);

            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $em->getConnection()->beginTransaction();
            /** @var AviaRateRepository $rateRepo */
            $rateRepo = $em->getRepository('EprstAviaBundle:AviaRate');

            /** @var StationRepository $stationRepo */
            $stationRepo = $em->getRepository('EprstAviaBundle:Station');
            $existingStations = array_reduce($stationRepo->findAll(), function($acc, StationEntity $item){
                    $acc[$item->getName()] = $item;
                    return $acc;
                }, array());

            $result = array();

            foreach ($jsonRequest as $row) {
                if (!isset($existingStations[$row['arrivalStationName']])) {
                    $station = $stationRepo->createBasicStation($row['arrivalStationName']);
                    $em->persist($station);
                } else {
                    $station = $existingStations[$row['arrivalStationName']];
                }

                $rateEntity = $rateRepo->fromArray($row);
                $rateEntity->setArrivalStation($station);
                $rateEntity->setType($rateTypeEntity);
                $em->persist($rateEntity);

                $result[] = $rateEntity;
            }

            $em->flush();
            $em->getConnection()->commit();

            $resultPlain = array_map(
                function ($item) use($rateRepo) {
                    return $rateRepo->toArray($item);
                },
                $result
            );
        }

        return new JsonResponse($resultPlain);
    }
}