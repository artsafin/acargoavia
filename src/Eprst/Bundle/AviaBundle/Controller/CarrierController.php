<?php

namespace Eprst\Bundle\AviaBundle\Controller;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityRepository;
use Eprst\Bundle\AcargoBundle\Controller\CrudController;
use Eprst\Bundle\AviaBundle\Entity\Carrier as CarrierEntity;
use Eprst\Bundle\AviaBundle\Repository\CarrierStatus;
use Symfony\Component\HttpFoundation\Request;

class CarrierController extends CrudController
{
    protected $templates = array(
        'create' => 'EprstAviaBundle:Carrier:add.html.twig',
        'read'   => 'EprstAviaBundle:Carrier:list.html.twig',
        'update' => 'EprstAviaBundle:Carrier:edit.html.twig',
        'delete' => 'EprstAviaBundle:Carrier:delete.html.twig'
    );
    protected $updateFormAlias = 'aviabundle_carrier';

    /**
     * persistDeleteEntity
     *
     * @param CarrierEntity $entity
     *
     * @return void
     */
    protected function persistDeleteEntity($entity)
    {
        $em                 = $this->getEntityManager();
        $statusRepo = $em->getRepository('EprstAviaBundle:CarrierStatus');
        $deletedStatus      = $statusRepo->find(CarrierStatus::STATUS_DELETED);
        $entity->setStatus($deletedStatus);
        $em->persist($entity);
    }

    public function listAction()
    {
        $criteria = array('status' => array(CarrierStatus::STATUS_ENABLED));

        return $this->render($this->getTemplateName('read'),
                             array('items' => $this->getRepository()->findBy($criteria)));
    }

    /**
     * @return object
     */
    protected function createNewEntity()
    {
        $entity = new CarrierEntity();

        $statusRepo    = $this->getEntityManager()->getRepository('EprstAviaBundle:CarrierStatus');
        $enabledStatus = $statusRepo->find(CarrierStatus::STATUS_ENABLED);
        $entity->setStatus($enabledStatus);

        return $entity;
    }

    /**
     * @param object $entity
     *
     * @return string
     */
    protected function getUrlRead($entity)
    {
        return $this->generateUrl('avia_carrier');
    }

    /**
     * @param object $entity
     *
     * @return string
     */
    protected function getUrlUpdate($entity)
    {
        return $this->generateUrl('avia_carrier_persist', array('id' => $entity->getId()));
    }

    /**
     * @param $id
     *
     * @return string
     */
    protected function getUrlDeletePersist($id)
    {
        return $this->generateUrl(
            'avia_carrier_delete_persist',
            array('id' => $id)
        );
    }

    /**
     * @return ObjectRepository
     */
    protected function getRepository()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EntityRepository $repo */
        return $em->getRepository('EprstAviaBundle:Carrier');
    }
}
