<?php

namespace Eprst\Bundle\AviaBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Eprst\Bundle\AviaBundle\Entity\Station;
use Eprst\Bundle\AviaBundle\Entity\StationStatus as StationStatusEntity;
use Eprst\Bundle\AviaBundle\Repository\StationStatus as StationStatusRepository;
use Eprst\Bundle\AviaBundle\Repository\StationStatus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;

class StationController extends Controller
{
    private function createStationForm(Station $stationEntity = null)
    {
        return $this->createForm('geobundle_station',
            $stationEntity,
            array('action' => $this->generateUrl('geo_stations_persist', array(
                'id' => $stationEntity->getId()?:null
            ))));
    }

    /**
     * @param integer $id
     *
     * @return Station
     */
    private function loadStationById($id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EntityRepository $stationRepo */
        $stationRepo = $em->getRepository('EprstAviaBundle:Station');

        /** @var Station $station */
        return $stationRepo->find($id);
    }

    public function listAction($type = null)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EntityRepository $stationRepo */
        $stationRepo = $em->getRepository('EprstAviaBundle:Station');

        $criterias = array(
            'stationStatus' => array(StationStatus::STATUS_OK, StationStatus::STATUS_WARNING)
        );
        if ($type == 'arrival') {
            $criterias['isArrival'] = 1;
        } else if ($type == 'departure') {
            $criterias['isDeparture'] = 1;
        }
        $items = $stationRepo->findBy($criterias, array('description' => 'ASC'));

        return $this->render('EprstAviaBundle:Station:list.html.twig', array(
            'items' => $items,
            'type' => $type
        ));
    }

    private function createNewEntity()
    {
        $entity = new Station();

        $statusRepo    = $this->getDoctrine()->getManager()->getRepository('EprstAviaBundle:StationStatus');
        $enabledStatus = $statusRepo->find(StationStatus::STATUS_OK);
        $entity->setStationStatus($enabledStatus);

        return $entity;
    }

    public function addAction()
    {
        $station = $this->createNewEntity();

        return $this->render('EprstAviaBundle:Station:add.html.twig', array(
            'form' => $this->createStationForm($station)->createView()
        ));
    }

    public function editAction($id = null)
    {
        $station = $this->loadStationById($id);

        return $this->render('EprstAviaBundle:Station:edit.html.twig',
            array('form' => $this->createStationForm($station)->createView())
        );
    }

    public function deleteAction($id = null)
    {
        $station = $this->loadStationById($id);
        $form = $this->createForm('acargobundle_sure', null, array(
            'action' => $this->generateUrl('geo_stations_delete_persist', array('id' => $station->getId()))
        ));

        return $this->render('EprstAviaBundle:Station:delete.html.twig',
            array(
                'station' => $station,
                'form' => $form->createView()
            )
        );
    }

    public function deletePersistAction($id, Request $request)
    {
        $station = $this->loadStationById($id);

        $form = $this->createForm('acargobundle_sure');
        $form->handleRequest($request);

        if ($form->get('yes')->isClicked())
        {
            $em = $this->getDoctrine()->getManager();
            /** @var StationStatusRepository $stationStatusRepo */
            $stationStatusRepo = $em->getRepository('EprstAviaBundle:StationStatus');
            /** @var StationStatusEntity $deletedStationStatus */
            $deletedStationStatus = $stationStatusRepo->find(StationStatusRepository::STATUS_DELETED);
            $station->setStationStatus($deletedStationStatus);
            $em->persist($station);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('geo_stations'));
    }

    public function persistAction($id = null, Request $request)
    {
        $station = empty($id) ? $this->createNewEntity() : $this->loadStationById($id);

        $form = $this->createStationForm($station);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $station->setName(strtoupper($station->getName()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($station);
            $em->flush();

            return $this->redirect($this->generateUrl('geo_stations'));
        }
        else
        {
            throw new InvalidArgumentException();
        }
    }
}
