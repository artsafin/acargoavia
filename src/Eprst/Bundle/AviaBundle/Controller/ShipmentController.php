<?php

namespace Eprst\Bundle\AviaBundle\Controller;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Eprst\Bundle\AcargoBundle\Controller\CrudController;
use Eprst\Bundle\AviaBundle\Entity\Shipment as ShipmentEntity;
use Eprst\Bundle\AviaBundle\Repository\Shipment as ShipmentRepository;
use Eprst\Bundle\AviaBundle\Repository\ShipmentStatus;
use Eprst\Bundle\AviaBundle\Repository\StationStatus;
use Eprst\Bundle\AviaBundle\Service\FeeCalculator;
use Eprst\Bundle\AviaBundle\Service\RateCalculator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class ShipmentController extends CrudController
{
    protected $templates = array(
        'create' => 'EprstAviaBundle:Shipment:add.html.twig',
        'read'   => 'EprstAviaBundle:Shipment:list.html.twig',
        'update' => 'EprstAviaBundle:Shipment:edit.html.twig',
        'delete' => 'EprstAviaBundle:Shipment:delete.html.twig'
    );
    protected $updateFormAlias = 'aviabundle_shipmenttype';

    /**
     * persistDeleteEntity
     *
     * @param ShipmentEntity $entity
     *
     * @return void
     */
    protected function persistDeleteEntity($entity)
    {
        $em = $this->getEntityManager();
        $shipmentStatusRepo = $em->getRepository('EprstAviaBundle:ShipmentStatus');
        $deletedStatus = $shipmentStatusRepo->find(ShipmentStatus::STATUS_DELETED);
        $entity->setStatus($deletedStatus);
        $em->persist($entity);
    }

    /**
     * @return object
     */
    protected function createNewEntity()
    {
        $entity = new ShipmentEntity();

        $statusRepo = $this->getEntityManager()->getRepository('EprstAviaBundle:ShipmentStatus');
        $enabledStatus = $statusRepo->find(ShipmentStatus::STATUS_ENABLED);
        $entity->setStatus($enabledStatus);

        $carrierRepo = $this->getEntityManager()->getRepository('EprstAviaBundle:Carrier');
        $carrierEntities = $carrierRepo->findAll();
        if (count($carrierEntities) == 1)
        {
            list($firstCarrier) = $carrierEntities;
            $entity->setCarrier($firstCarrier);
        }

        return $entity;
    }

    protected function getRenderVars($entity, $default = array())
    {
        $stationRepo = $this->getDoctrine()->getManager()->getRepository('EprstAviaBundle:Station');
        $arrivalStations = $stationRepo->findBy(array(
	        'isArrival' => true,
	        'stationStatus' => array(StationStatus::STATUS_OK, StationStatus::STATUS_WARNING)
        ));
        $arrivalStations = array_map(function($item){ return $item->getName(); }, $arrivalStations);

        return array_merge(parent::getRenderVars($entity, $default),
                           array(
                                'arrivalStations' => $arrivalStations
                           ));
    }

    public function listAction(Request $req = null, $limit = null, $page = null)
    {
        if ($page < 1) {
            $page = 1;
        }
        $pageZeroBased = $page - 1;

        $orderBy = array(
            'dateFactDeparture' => 'DESC',
            'srcStation' => 'ASC',
            'dstStation' => 'ASC',
        );

        $filterForm = $this->createForm('aviabundle_shipment_advancedsearchtype', null, array('action' => $this->generateUrl('avia_shipment_unlimited')));
        $filterForm->handleRequest($req);

        $search = $req->get('search');
        if (strlen($search) >= 3) {
            list($items, $total) = $this->getRepository()->search($search);
        } else if ($filterForm->isSubmitted()) {
            list($items, $total) = $this->getRepository()->findEnabledJoined($filterForm->getData(), $orderBy, $limit, $pageZeroBased * $limit);
        } else {
            list($items, $total) = $this->getRepository()->findEnabledJoined(array(), $orderBy, $limit, $pageZeroBased * $limit);
        }

        return $this->render(
            $this->getTemplateName('read'),
            array('items' => $items,
                  'total' => $total,
                  'limit' => $limit,
                  'page' => $page,
                  'numPages' => ceil($total/$limit),
                  'filterForm' => $filterForm->createView()
                 )
        );
    }

    public function persistAction($id = null, Request $request)
    {
        /** @var ShipmentEntity $entity */
        $entity = empty($id) ? $this->createNewEntity() : $this->loadEntity($id);

        $oldPlaces = clone $entity->getPlaces();

        $form = $this->createEntityForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

	        $newPlacesIds = array_map(function($item){return $item->getId();}, iterator_to_array($entity->getPlaces()));

            foreach ($oldPlaces as $oldPlace) {
                if (array_search($oldPlace->getId(), $newPlacesIds) === false) {
                    $this->getEntityManager()->remove($oldPlace);
                }
            }

            /** @var ShipmentRepository $shipmentRepo */
            $shipmentRepo = $this->getDoctrine()->getRepository('EprstAviaBundle:Shipment');
            $saveResults = $shipmentRepo->persist($entity);

            $this->getEntityManager()->flush();

            $this->addFlashMessages('persist_effects', array_keys(array_filter($saveResults)));

            return $this->redirect($this->generateUrl('avia_shipment_edit', array('id' => $entity->getId())));
        } else {
            return $this->render(
                $this->getTemplateName('update'),
                array(
                     'errors' => $form->getErrors(),
                     'form'   => $form->createView()
                )
            );
        }
    }

    private function addFlashMessages($type, $messages)
    {
        /** @var Session $session */
        $session = $this->get('session');

        foreach ($messages as $msg) {
            $session->getFlashBag()->add(
                $type,
                $msg
            );
        }
        $session->save();
    }



    /**
     * @param object $entity
     *
     * @return string
     */
    protected function getUrlRead($entity)
    {
        return $this->generateUrl('avia_shipment_unlimited');
    }

    /**
     * @param object $entity
     *
     * @return string
     */
    protected function getUrlUpdate($entity)
    {
        return $this->generateUrl('avia_shipment_persist', array('id' => $entity->getId()));
    }

    /**
     * @param $id
     *
     * @return string
     */
    protected function getUrlDeletePersist($id)
    {
        return $this->generateUrl(
            'avia_shipment_delete_persist',
            array('id' => $id)
        );
    }

    /**
     * @return ShipmentRepository
     */
    protected function getRepository()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EntityRepository $repo */

        return $em->getRepository('EprstAviaBundle:Shipment');
    }

    public function calculateRateAction(Request $request)
    {
        $chargeableWeightCarrier = $request->request->get('chargeableWeightCarrier');
        $chargeableWeightAgent = $request->request->get('chargeableWeightAgent');
        $dstStation = $request->request->get('dstStation');

        /** @var RateCalculator $rateCalculator */
        $rateCalculator = $this->container->get('eprst_avia.rate_calculator');

        try
        {
            $agentRate = $rateCalculator->calculateAgent($chargeableWeightAgent, $dstStation);
            $carrierRate = $rateCalculator->calculateCarrier($chargeableWeightCarrier, $dstStation);
        }
        catch (NoResultException $exc)
        {
            list($agentRate, $carrierRate) = array(array(), array());
        }

        /** @var LoggerInterface $logger */
        $logger = $this->container->get('logger');
        $logger->info('calculateRateAction', array(
                                                  'chargeableWeightCarrier' => $chargeableWeightCarrier,
                                                  'chargeableWeightAgent' => $chargeableWeightAgent,
                                                  'dstStation' => $dstStation,
                                                  'agentRate' => $agentRate,
                                                  'carrierRate' => $carrierRate,
                                             ));

        return new JsonResponse(array(
            'agent' => $agentRate,
            'carrier' => $carrierRate,
        ));
    }

    public function calculateFeeAction(Request $request)
    {
        $carrierId = $request->request->get('carrier');
        $weight = $request->request->get('weight');

        /** @var FeeCalculator $calc */
        $calc = $this->container->get('eprst_avia.fee_calculator');

        try
        {
            $fee = $calc->calculate($carrierId, $weight);
        }
        catch (NoResultException $exc)
        {
            $fee = array();
        }

        $fee = array_map(function($value){
                return round($value, 2);
            }, $fee);

        $fee['total'] = round(array_sum($fee), 2);

        /** @var LoggerInterface $logger */
        $logger = $this->container->get('logger');
        $logger->info(
            'calculateFeeAction',
            array_merge(array(
                 'carrierId' => $carrierId,
                 'weight'   => $weight,
            ), $fee)
        );

        return new JsonResponse($fee);
    }
}
