<?php

namespace Eprst\Bundle\AviaBundle\Controller;
use Doctrine\ORM\NoResultException;
use Eprst\Bundle\AviaBundle\Entity\Shipment as ShipmentEntity;
use Eprst\Bundle\AviaBundle\Entity\BankReport as BankReportEntity;
use Eprst\Bundle\AviaBundle\Repository\BankReport as BankReportRepository;
use Eprst\Bundle\AviaBundle\Repository\Shipment as ShipmentRepository;
use Eprst\Bundle\AviaBundle\Service\Report\ReportResponseBuilderInterface;
use Eprst\Bundle\AviaBundle\Service\ShipmentEntityMapper;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * BankReportController
 * 
 */
class BankReportController extends Controller
{
    private function createReportFilterForm($actionRouteName)
    {
        $options = array(
            'action' => $this->generateUrl($actionRouteName)
        );
        $form = $this->createForm('aviabundle_report_bank', null, $options);

        return $form;
    }

    /**
     * @param $dateCriteriaField
     * @param $dateFrom
     * @param $dateTo
     *
     * @return ShipmentEntity[]
     */
    private function loadReport($dateCriteriaField, $dateFrom, $dateTo)
    {
        /** @var ShipmentRepository $repo */
        $repo = $this->getDoctrine()->getManager()->getRepository('EprstAviaBundle:Shipment');

        $entities = $repo->findByDates($dateCriteriaField, $dateFrom, $dateTo);

        return $entities;
    }

    public function indexAction(Request $request)
    {
        $lang = 'ru';
        $form = $this->createReportFilterForm('avia_report_bank');

        $form->handleRequest($request);

        $reportItems = null;
        $prevPeriodTotalAgentFee = null;

        $someButtonClicked = $form->get('show')->isClicked()
                            || $form->get('export')->isClicked()
                            || $form->get('approve')->isClicked();

        if ($form->isValid() && $someButtonClicked)
        {
            $dateType = $form->get('date_type')->getData();

            $dateCriteriaField = in_array($dateType, array('dateWaybill', 'dateFactDeparture'))?$dateType:'dateWaybill';

            $dateFrom = $form->get('from')->getData();
            $dateTo = $form->get('to')->getData();

            $reportItems = $this->loadReport($dateCriteriaField, $dateFrom, $dateTo);
            $prevPeriodTotalAgentFee = $this->getTotalAgentFeeBeforeDate($dateFrom);

            if ($form->get('approve')->isClicked())
            {
                $totalAgentFee = $this->calculateTotalAgentFee($reportItems);
                $this->approveReport($dateFrom, $dateTo, $totalAgentFee);
            }
            else if ($form->get('export')->isClicked())
            {
                return $this->createExcelResponse($dateFrom, $dateTo, $reportItems, $prevPeriodTotalAgentFee);
            }
        }

        /** @var ShipmentEntityMapper $shipmentMapper */
        $shipmentMapper = $this->container->get('eprst_avia.shipment_mapper');

        return $this->render(
            'EprstAviaBundle:BankReport:index.html.twig',
            array(
                 'filterForm' => $form->createView(),
                 'items' => $reportItems,
                 'prevPeriodTotalAgentFee' => $prevPeriodTotalAgentFee,
                 'mapper' => $shipmentMapper,
                 'lang' => $lang
            )
        );
    }

    private function calculateTotalAgentFee($reportItems)
    {
        /** @var ShipmentEntityMapper $shipmentMapper */
        $shipmentMapper = $this->container->get('eprst_avia.shipment_mapper');

        $carrierAgentFees = array_map(function ($item) use ($shipmentMapper) {
            return $shipmentMapper->get($item, 'Carrier agent fee');
        },
            $reportItems);

        $totalAgentFee = array_sum($carrierAgentFees);

        return $totalAgentFee;
    }

    private function getTotalAgentFeeBeforeDate($date)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var BankReportRepository $bankReportRepository */
        $bankReportRepository = $em->getRepository('EprstAviaBundle:BankReport');

        try {
            /** @var BankReportEntity $bankReport */
            $bankReport = $bankReportRepository->getReportBefore($date);
        } catch (NoResultException $exc) {
            return 0;
        }

        return $bankReport->getTotalAgentFee();
    }

    /**
     * approveReport
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param float $totalAgentFee
     *
     * @return BankReportEntity
     */
    private function approveReport($dateFrom, $dateTo, $totalAgentFee)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var BankReportRepository $bankReportRepository */
        $bankReportRepository = $em->getRepository('EprstAviaBundle:BankReport');
        $entity = $bankReportRepository->createReportEntity($dateFrom, $dateTo, $totalAgentFee);

        $em->persist($entity);
        $em->flush();

        return $entity;
    }

    /**
     * @param                  $dateFrom
     * @param                  $dateTo
     * @param ShipmentEntity[] $items
     * @param float            $prevPeriodTotalAgentFee
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createExcelResponse($dateFrom, $dateTo, $items, $prevPeriodTotalAgentFee)
    {
        /** @var ReportResponseBuilderInterface $reportBuilder */
        $reportBuilder = $this->container->get('eprst_avia.report.excel.bank');

        $response = $reportBuilder->build($items, array(
                                                       'dateFrom' => $dateFrom,
                                                       'dateTo' => $dateTo,
                                                       'prevPeriodTotalAgentFee' => $prevPeriodTotalAgentFee,
                                                  ));

        $fileName = $this->getReportFileName($dateFrom, $dateTo);
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $fileName . '"');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    /**
     * getReportFileName
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return string
     */
    private function getReportFileName($dateFrom, $dateTo)
    {
        return sprintf('bank_report_%s_%s.xls', $dateFrom->format('dmY'), $dateTo->format('dmY'));
    }
}
