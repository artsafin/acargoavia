<?php

namespace Eprst\Bundle\AviaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use Eprst\Bundle\AviaBundle\Entity\BankReport as BankReportEntity;

/**
 * BankReport
 */
class BankReport extends EntityRepository
{
    /**
     * createReportEntity
     *
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     * @param float $totalAgentFee
     *
     * @return BankReportEntity
     */
    public function createReportEntity($dateFrom, $dateTo, $totalAgentFee)
    {
        $entity = new BankReportEntity();
        $entity->setDateFrom($dateFrom);
        $entity->setDateTo($dateTo);
        $entity->setTotalAgentFee($totalAgentFee);

        return $entity;
    }

    public function getReportBefore($date)
    {
        $qb = $this->createQueryBuilder('br');

        $qb->where('br.dateTo <= :date');
        $qb->setParameter(':date', $date);

        $qb->orderBy('br.dateTo', 'DESC');
        $qb->addOrderBy('br.dateTo - br.dateFrom', 'ASC');

        $qb->setMaxResults(1);

        $query = $qb->getQuery();

        return $query->getSingleResult();
    }
}