<?php

namespace Eprst\Bundle\AviaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * StationStatus
 */
class StationStatus extends EntityRepository
{
    const STATUS_OK = 1;
    const STATUS_WARNING = 2;
    const STATUS_DELETED = 3;
}