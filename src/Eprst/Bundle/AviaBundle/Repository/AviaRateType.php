<?php

namespace Eprst\Bundle\AviaBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * AviaRateType
 *
 */
class AviaRateType extends EntityRepository
{
    const TYPE_CARRIER = 1;
    const TYPE_AGENT = 2;

    public function findJoined($id)
    {
        $qb = $this->createQueryBuilder('rt');
        $qb->select('rt', 'r', 'dst');
        $qb
            ->join('rt.rates', 'r')
            ->join('r.arrivalStation', 'dst')
        ;
        $qb->where('rt.id=:id');
        $qb->orderBy('dst.name');

        $query = $qb->getQuery();
        $query->setParameters(
            array(
                 ':id' => $id
            ));
        return $query->getSingleResult();
    }
}
