<?php

namespace Eprst\Bundle\AviaBundle\Repository;

use Doctrine\ORM\ORMInvalidArgumentException;
use Eprst\Bundle\AviaBundle\Entity\AviaRate as AviaRateEntity;
use Eprst\Bundle\AviaBundle\Entity\AviaRateType as AviaRateTypeEntity;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Eprst\Bundle\AviaBundle\Entity\Station as StationEntity;

/**
 * AviaRate
 *
 */
class AviaRate extends EntityRepository
{
    /**
     * @param $type
     * @param $station
     *
     * @return AviaRateEntity
     */
    public function getRate($type, $station)
    {
        if ($station && !$this->getEntityManager()->getUnitOfWork()->isInIdentityMap($station)) {
            return null;
        }

        $qb = $this->createQueryBuilder('r');
        $qb
            ->where($qb->expr()->andX(
                        $qb->expr()->eq('r.type', ':type'),
                        $qb->expr()->eq('r.arrivalStation', ':station')
                    )
                );
        $qb->setMaxResults(1);

        $query = $qb->getQuery();
        $query->execute(array(
                              ':type' => $type,
                              ':station' => $station,
                         ));
        return $query->getOneOrNullResult();
    }

    public function findByType($type)
    {
        $qb = $this->createQueryBuilder('r', 'st');

        $qb->join('r.arrivalStation', 'st');
        $qb->where('r.type = :type');
        $qb->andWhere('st.stationStatus != :stationDeletedStatus');
        $qb->orderBy('st.name', 'ASC');

        $qb->setParameters(array(
                                ':type' => $type,
                                ':stationDeletedStatus' => StationStatus::STATUS_DELETED
                           ));

        $query = $qb->getQuery();

        return $query->getResult();
    }

    /**
     * @param AviaRateEntity $entity
     * @param string         $prefix
     * @param string         $rateStrategy
     *
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     * @throws \BadMethodCallException
     * @return string
     */
    private function getRateMethodByStrategy($entity, $prefix, $rateStrategy)
    {
        $cm = $this->getEntityManager()->getClassMetadata('EprstAviaBundle:AviaRate');
        if (!$cm->hasField($rateStrategy)) {
            throw new ORMInvalidArgumentException("Invalid rate strategy {$rateStrategy}");
        }
        $strategyFieldNameMethod = $prefix . ucfirst($rateStrategy);

        if (!method_exists($entity, $strategyFieldNameMethod)) {
            throw new \BadMethodCallException("Method {$strategyFieldNameMethod} doesn't exist");
        }

        return $strategyFieldNameMethod;
    }

    public function setRateValueByStrategy($entity, $strategy, $value)
    {
        $method = $this->getRateMethodByStrategy($entity, 'set', $strategy);

        return $entity->$method($value);
    }

    public function getRateValueByStrategy($entity, $strategy)
    {
        $method = $this->getRateMethodByStrategy($entity, 'get', $strategy);

        return floatval($entity->$method());
    }

    /**
     * @param int             $type
     * @param StationEntity   $station
     * @param string          $rateStrategy
     * @param float           $value
     *
     * @return AviaRateEntity
     *
     * @throws ORMInvalidArgumentException
     */
    public function createRateByStrategy($type, $station, $rateStrategy, $value)
    {
        $aviaRateTypeRepo = $this->getEntityManager()->getRepository('EprstAviaBundle:AviaRateType');
        /** @var AviaRateTypeEntity $typeEntity */
        $typeEntity = $aviaRateTypeRepo->find($type);

        if (!$typeEntity) {
            throw new ORMInvalidArgumentException("AviaRateType {$type} is invalid");
        }



        $entity = new AviaRateEntity();
        $entity->setType($typeEntity);
        $entity->setArrivalStation($station);
        $this->setRateValueByStrategy($entity, $rateStrategy, $value);

        return $entity;
    }

    public function toArray(AviaRateEntity $entity)
    {
        return array(
            'id'                 => $entity->getId(),
            'arrivalStationId'   => $entity->getArrivalStation()->getId(),
            'arrivalStationName' => $entity->getArrivalStation()->getName(),
            'typeId'             => $entity->getType()->getId(),
            'typeName'           => $entity->getType()->getName(),
            'min'                => floatval($entity->getMin()),
            'n'                  => floatval($entity->getN()),
            'range45'            => floatval($entity->getRange45()),
            'range100'           => floatval($entity->getRange100()),
            'range300'           => floatval($entity->getRange300()),
            'range500'           => floatval($entity->getRange500()),
            'range1000'          => floatval($entity->getRange1000()),
        );
    }

    public function fromArray(array $row)
    {
        list($pk) = $this->getClassMetadata()->getIdentifierFieldNames();

        if (isset($row[$pk])) {
            $baseEntity = $this->find($row[$pk]);
        } else {
            $baseEntity = new AviaRateEntity();
        }

        foreach (array('min', 'n', 'range45', 'range100', 'range300', 'range500', 'range1000') as $field) {
            $methodName = sprintf('set%s', ucfirst($field));
            if (isset($row[$field]) && method_exists($baseEntity, $methodName)) {
                $baseEntity->$methodName(floatval($row[$field]));
            }
        }

        return $baseEntity;
    }
}
