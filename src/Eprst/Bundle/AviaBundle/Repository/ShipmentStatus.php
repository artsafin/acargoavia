<?php

namespace Eprst\Bundle\AviaBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * ShipmentStatus
 */
class ShipmentStatus extends EntityRepository
{
    const STATUS_ENABLED = 1;
    const STATUS_DELETED = 2;
}