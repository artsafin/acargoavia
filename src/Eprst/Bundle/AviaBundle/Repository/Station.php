<?php

namespace Eprst\Bundle\AviaBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Eprst\Bundle\AviaBundle\Entity\Station as StationEntity;
use Eprst\Bundle\AviaBundle\Entity\StationStatus as StationStatusEntity;

/**
 * Station
 */
class Station extends EntityRepository
{
    public function createBasicStation($name)
    {
        $entity = new StationEntity();
        $entity->setName($name);
        $entity->setIsDeparture(false);
        $entity->setIsArrival(true);

        /** @var StationStatus $stationStatus */
        $stationStatus = $this->getEntityManager()->getRepository('EprstAviaBundle:StationStatus');
        /** @var StationStatusEntity $warningStationStatus */
        $warningStationStatus = $stationStatus->find(StationStatus::STATUS_WARNING);

        $entity->setStationStatus($warningStationStatus);

        return $entity;
    }
}
