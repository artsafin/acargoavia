<?php

namespace Eprst\Bundle\AviaBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMInvalidArgumentException;
use Eprst\Bundle\AviaBundle\Entity\Shipment as ShipmentEntity;
use Eprst\Bundle\AviaBundle\Repository\AviaRate as AviaRateRepository;
use Eprst\Bundle\AviaBundle\Repository\AviaRateType as AviaRateTypeRepository;
use Eprst\Bundle\AviaBundle\Entity\Station as StationEntity;


/**
 * Shipment
 * 
 * @author Artur.Safin
 * @date 28.07.13 14:35
 */
class Shipment extends EntityRepository
{
    private function createQbEnabledByDate($dateField, $dateFrom, $dateTo)
    {
        if (!$this->getClassMetadata()->hasField($dateField)) {
            throw new ORMInvalidArgumentException("Field {$dateField} is invalid");
        }

        $qb = $this->createQueryBuilder('s');
        $qb->select(array('s', 'src', 'dst', 'sfee', 'sagent', 'scarrier'));
        $qb->join('s.srcStation', 'src');
        $qb->join('s.dstStation', 'dst');
        $qb->join('s.fee', 'sfee');
        $qb->join('s.agent', 'sagent');
        $qb->join('s.agent', 'scarrier');
        $qb->where(
            $qb->expr()->between("s.{$dateField}", ':dateFrom', ':dateTo')
        );
        $qb->setParameter(':dateFrom', $dateFrom);
        $qb->setParameter(':dateTo', $dateTo);

        $qb->andWhere('s.status = :statusEnabled');
        $qb->setParameter(':statusEnabled', ShipmentStatus::STATUS_ENABLED);

        $qb->andWhere('sagent.status = :agentStatusEnabled');
        $qb->setParameter(':agentStatusEnabled', AgentStatus::STATUS_ENABLED);
        $qb->andWhere('scarrier.status = :carrierStatusEnabled');
        $qb->setParameter(':carrierStatusEnabled', CarrierStatus::STATUS_ENABLED);

        return $qb;
    }

    private function createQbByCriteria(array $criteria = array())
    {
        $qb = $this->createQueryBuilder('s');

        if (isset($criteria['dateWaybillFrom']) && $criteria['dateWaybillFrom'] instanceof \DateTime) {
            $qb->andWhere('s.dateWaybill >= :dateWaybillFrom');
            $qb->setParameter(':dateWaybillFrom', $criteria['dateWaybillFrom']->format('Y-m-d 00:00:00'));
        }

        if (isset($criteria['dateWaybillTo']) && $criteria['dateWaybillTo'] instanceof \DateTime) {
            $qb->andWhere('s.dateWaybill <= :dateWaybillTo');
            $qb->setParameter(':dateWaybillTo', $criteria['dateWaybillTo']->format('Y-m-d 23:59:59'));
        }

        if (isset($criteria['waybill'])) {
            $qb->setParameter(':waybill', '%'.preg_replace('/[^\d]/', '', $criteria['waybill']).'%');
            $qb->andWhere('concat(s.waybillPrefix, s.waybill) like :waybill');
        }

        if (isset($criteria['srcStation'])) {
            $qb->setParameter(':srcStation', $criteria['srcStation']);
            $qb->andWhere('s.srcStation = :srcStation');
        }

        if (isset($criteria['dstStation'])) {
            $qb->setParameter(':dstStation', $criteria['dstStation']);
            $qb->andWhere('s.dstStation = :dstStation');
        }

        if (isset($criteria['agent'])) {
            $qb->setParameter(':agent', $criteria['agent']);
            $qb->andWhere('s.agent = :agent');
        }

        return $qb;
    }

    public function findEnabledJoined(array $criteria = array(), array $orderBy = null, $limit = null, $offset = null)
    {
        $qb = $this->createQbByCriteria($criteria);
        $qb->select(array('s', 'src', 'dst', 'agent', 'carrier', 'fee'));

        $qb->andWhere('s.status = :statusEnabled');
        $qb->setParameter(':statusEnabled', ShipmentStatus::STATUS_ENABLED);

        $qb->join('s.srcStation', 'src');
        $qb->join('s.dstStation', 'dst');
        $qb->join('s.agent', 'agent');
        $qb->join('s.carrier', 'carrier');
        $qb->join('s.fee', 'fee');

        foreach ($orderBy as $orderField => $orderDir) {
            if (strpos($orderField, '.') === false) {
                $orderField = "s.{$orderField}";
            }
            $qb->addOrderBy($orderField, $orderDir);
        }

        $qb->setMaxResults($limit);
        $qb->setFirstResult($offset);

        $query = $qb->getQuery();

        $items = $query->getResult();

        $qbUnlimited = $this->createQbByCriteria($criteria);
        $qbUnlimited->select('count(s)');
        $total = $qbUnlimited->getQuery()->getSingleScalarResult();

        return array($items, $total);
    }

    public function search($search, $limit = null, $page = null)
    {
        $qb = $this->createQueryBuilder('s');

        $qb->where('s.waybill like :query');

        $query = $qb->getQuery();
        $query->setParameters(array(':query' => "%{$search}%"));

        $result = $query->getResult();

        return array($result, count($result));
    }

    /**
     * @param string    $dateField
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return Shipment[]
     * @throws \Doctrine\ORM\ORMInvalidArgumentException
     */
    public function findByDates($dateField, $dateFrom, $dateTo)
    {
        $qb = $this->createQbEnabledByDate($dateField, $dateFrom, $dateTo);

        $qb->orderBy('src.name', 'ASC');
        $qb->addOrderBy('s.dateWaybill', 'ASC');

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function findByDatesAndStation($station, $dateField, $dateFrom, $dateTo)
    {
        $qb = $this->createQbEnabledByDate($dateField, $dateFrom, $dateTo);

        $qb->orderBy('src.name', 'ASC');
        $qb->addOrderBy('s.dateWaybill', 'ASC');

        $qb->andWhere('src = :station');

        $qb->setParameter(':station', $station);

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function findByDatesAndAgent($agent, $dateField, $dateFrom, $dateTo)
    {
        $qb = $this->createQbEnabledByDate($dateField, $dateFrom, $dateTo);

        $qb->orderBy('src.name', 'ASC');
        $qb->addOrderBy('s.dateWaybill', 'ASC');

        $qb->andWhere('s.agent = :agent');

        $qb->setParameter(':agent', $agent);

        $query = $qb->getQuery();

        return $query->getResult();
    }

    public function persist(ShipmentEntity $entity)
    {
        $isEdit = $this->getEntityManager()->contains($entity);

        $numPlaces = 0;
        foreach ($entity->getPlaces() as $placeEntity) {
            $numPlaces += $placeEntity->getNumPieces();
        }
        $entity->setNumPlaces($numPlaces);

        $results = $this->persistLinkedEntities($entity);

        $this->getEntityManager()->persist($entity);

        if ($isEdit) {
            $results['edit_shipment'] = true;
        } else {
            $results['add_shipment'] = true;
        }

        return $results;
    }

    private function ensureArrivalStationExists($station)
    {
        $addingResult = array();

        $em = $this->getEntityManager();

        if (!$em->contains($station)) {
            $em->persist($station);

            $addingResult['add_station'] = true;
        }

        return $addingResult;
    }

    private function persistLinkedEntities(ShipmentEntity $shipment)
    {
        $addingResult = $this->ensureArrivalStationExists($shipment->getDstStation());

        $addingResult['add_agent_rate'] = $this->ensureRateExists(
            AviaRateTypeRepository::TYPE_AGENT,
            $shipment->getDstStation(),
            $shipment->getAgentRateStrategy(),
            $shipment->getRateAgent()
        );

        $addingResult['add_carrier_rate'] = $this->ensureRateExists(
            AviaRateTypeRepository::TYPE_CARRIER,
            $shipment->getDstStation(),
            $shipment->getCarrierRateStrategy(),
            $shipment->getRateCarrier()
        );

        return $addingResult;
    }

    /**
     * @param int             $type
     * @param StationEntity   $station
     * @param string          $rateStrategy
     * @param float           $value
     *
     * @return bool
     */
    private function ensureRateExists($type, $station, $rateStrategy, $value)
    {
        $contains = $this->getEntityManager()->contains($station);
        /** @var AviaRateRepository $aviaRateRepo */
        $aviaRateRepo = $this->getEntityManager()->getRepository('EprstAviaBundle:AviaRate');
        $rate         = $aviaRateRepo->getRate(
            $type,
            $station
        );

        // The whole rate not found or just single rate value
        if (!$rate) {
            $rateEntity = $aviaRateRepo->createRateByStrategy(
                $type,
                $station,
                $rateStrategy,
                $value
            );
            $this->getEntityManager()->persist($rateEntity);

            return true;
        } else if (!$aviaRateRepo->getRateValueByStrategy($rate, $rateStrategy)) {
            $aviaRateRepo->setRateValueByStrategy($rate, $rateStrategy, $value);
            $this->getEntityManager()->persist($rate);

            return true;
        }

        return false;
    }
}
