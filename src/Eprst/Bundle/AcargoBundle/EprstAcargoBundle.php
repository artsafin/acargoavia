<?php

namespace Eprst\Bundle\AcargoBundle;

use Eprst\Bundle\AcargoBundle\DependencyInjection\Compiler\NavigationCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EprstAcargoBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new NavigationCompilerPass());
    }
}
