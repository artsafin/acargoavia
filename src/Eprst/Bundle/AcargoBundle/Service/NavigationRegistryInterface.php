<?php

namespace Eprst\Bundle\AcargoBundle\Service;

/**
 * NavigationRegistryInterface
 *
 * @author Artur.Safin
 * @date 05.07.13 0:22
 */
interface NavigationRegistryInterface {
    public function addNavigationService(NavigationInterface $service);

    public function getStructure();

    public function getPathByRoute($route);

    public function getStructureForKey($key);
}
