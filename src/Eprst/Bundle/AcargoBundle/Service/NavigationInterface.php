<?php

namespace Eprst\Bundle\AcargoBundle\Service;

/**
 * NavigationInterface
 *
 * @author Artur.Safin
 * @date 04.07.13 23:28
 */
interface NavigationInterface
{
    /**
     * @return array
     */
    public function getNavigationStructure();
}
