<?php

namespace Eprst\Bundle\AcargoBundle\Service;
use Eprst\Bundle\AcargoBundle\Service\NavigationInterface;

/**
 * BaseNavigation
 * 
 * @author Artur.Safin
 * @date 05.07.13 1:10
 */
class BaseNavigation implements NavigationInterface
{
    /**
     * @var array
     */
    private $structure;

    public function __construct($structure)
    {
        $this->structure = $structure;
    }

    /**
     * @return array
     */
    public function getNavigationStructure()
    {
        return $this->structure;
    }
}
