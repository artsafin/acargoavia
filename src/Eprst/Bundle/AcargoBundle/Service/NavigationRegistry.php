<?php

namespace Eprst\Bundle\AcargoBundle\Service;

/**
 * NavigationRegistry
 * 
 * @author Artur.Safin
 * @date 04.07.13 23:26
 */
class NavigationRegistry implements NavigationRegistryInterface
{
    private $services = array();
    private $structure = array();

    private $routePaths = array();

    private function extractRoutes($navStructure, $path = array())
    {
        $routes = array();

        foreach ($navStructure as $pathKey => $items)
        {
            if (is_string($pathKey))
            {
                $currentPath = array_merge($path, array($pathKey));

                if (isset($items['route']))
                {
                    $routes[$items['route']] = $currentPath;
                }
            }
            else
            {
                $currentPath = $path;
            }

            if (isset($items['children']))
            {
                $routes = array_merge($routes, $this->extractRoutes($items['children'], $currentPath));
            }
        }

        return $routes;
    }

    public function addNavigationService(NavigationInterface $service)
    {
        $this->services[] = $service;

        $navStructure = $service->getNavigationStructure();
        $this->routePaths = array_merge($this->routePaths, $this->extractRoutes($navStructure));

        $this->structure = $this->removeDuplicates(array_merge_recursive($this->structure, $navStructure));
    }

    private function removeDuplicates($array)
    {
        foreach ($array as $key => &$item)
        {
            if (in_array($key, array('name', 'route'), true) && is_array($item) && isset($item[0]))
                $item = $item[0];
            else if (is_array($item))
                $item = $this->removeDuplicates($item);
        }

        return $array;
    }

    public function getStructure()
    {
        return $this->structure;
    }

    public function getStructureForKey($key)
    {
        if (isset($this->structure[$key]))
        {
            return $this->structure[$key];
        }
        else
        {
            return array();
        }
    }

    public function getPathByRoute($route)
    {
        if (isset($this->routePaths[$route]))
        {
            return $this->routePaths[$route];
        }
        else
        {
            return array();
        }
    }
}
