<?php

namespace Eprst\Bundle\AcargoBundle\Service;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * NavRoutingLoader
 * 
 * @author Artur.Safin
 * @date 05.07.13 0:12
 */
class RoutingLoader implements LoaderInterface
{
    private $loaded = false;

    /**
     * @var NavigationRegistryInterface
     */
    private $navRegistry;

    /**
     * @var string
     */
    private $routeName;

    public function __construct($routeName, NavigationRegistryInterface $navRegistry)
    {
        $this->routeName = $routeName;
        $this->navRegistry = $navRegistry;
    }

    /**
     * Loads a resource.
     *
     * @param mixed  $resource The resource
     * @param string $type     The resource type
     *
     * @throws \RuntimeException
     *
     * @return RouteCollection
     */
    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }

        $routes = new RouteCollection();

        foreach ($this->navRegistry->getStructure() as $key => $items)
        {
            // prepare a new route
            $pattern      = "/{$key}";
            $defaults     = array('navigationKey' => $key, '_controller' => $this->routeName);
            $route        = new Route($pattern, $defaults);

            // add the new route to the route collection:
            $routeName = $items['route'];

            $routes->add($routeName, $route);
        }

        $this->loaded = true;

        return $routes;
    }

    /**
     * Returns true if this class supports the given resource.
     *
     * @param mixed  $resource A resource
     * @param string $type     The resource type
     *
     * @return Boolean true if this class supports the given resource, false otherwise
     */
    public function supports($resource, $type = null)
    {
        return $type == 'eprst_acargo_navigation_routing';
    }

    /**
     * Gets the loader resolver.
     *
     * @return LoaderResolverInterface A LoaderResolverInterface instance
     */
    public function getResolver()
    {
        // TODO: Implement getResolver() method.
    }

    /**
     * Sets the loader resolver.
     *
     * @param LoaderResolverInterface $resolver A LoaderResolverInterface instance
     */
    public function setResolver(LoaderResolverInterface $resolver)
    {
        // TODO: Implement setResolver() method.
    }
}
