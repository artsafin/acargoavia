<?php

namespace Eprst\Bundle\AcargoBundle\Controller;

use Eprst\Bundle\AcargoBundle\Service\NavigationRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MenuController extends Controller
{
    /**
     * @return NavigationRegistry
     */
    private function getNavigationRegistry()
    {
        return $this->container->get('eprst_acargo.navigation.registry');
    }

    public function leftMenuAction($_route)
    {
        $navigation = $this->getNavigationRegistry();

        $selectedKeys = array();
        $topMenuKey = null;

        $navigationPath = $navigation->getPathByRoute($_route);

        if (count($navigationPath))
        {
            list($topMenuKey,) = $navigationPath;
            $selectedKeys = array_slice($navigationPath, 1);
        }

        $leftMenu = $navigation->getStructureForKey($topMenuKey);

        return $this->render('EprstAcargoBundle:Menu:left.html.twig',
            array(
                'topMenuKey' => $topMenuKey,
                'selectedKeys' => $selectedKeys,
                'leftMenu' => $leftMenu
            )
        );
    }

    public function topMenuAction($_route)
    {
        $navigation = $this->getNavigationRegistry();

        $navigationPath = $navigation->getPathByRoute($_route);

        $topMenuKey = null;
        if (count($navigationPath)) {
            list($topMenuKey,) = $navigationPath;
        }

        $navStructure = $navigation->getStructure();

        return $this->render('EprstAcargoBundle:Menu:top.html.twig',
            array(
                'homeRouteName' => 'eprst_acargo_homepage',
                'navStructure' => $navStructure,
                'selectedKeys' => array($topMenuKey)
            )
        );
    }
}
