<?php

namespace Eprst\Bundle\AcargoBundle\Controller;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * CrudController
 * 
 * @author Artur.Safin
 * @date 20.07.13 21:08
 */
abstract class CrudController extends Controller
{
    const SURE_FORM_ALIAS = 'acargobundle_sure';
    /**
     * @var array
     */
    protected $templates;

    /**
     * @var string
     */
    protected $updateFormAlias;

    /**
     * @var string
     */
    protected $sureFormAlias;

    protected function getTemplateName($type)
    {
        return $this->templates[$type];
    }

    public function listAction()
    {
        return $this->render(
            $this->getTemplateName('read'),
            array('items' => $this->getRepository()->findAll())
        );
    }

    public function addAction()
    {
        $entity = $this->createNewEntity();

        return $this->render(
            $this->getTemplateName('create'),
            $this->getRenderVars($entity)
        );
    }

    protected function getRenderVars($entity, $default = array())
    {
        return array_merge(array(
                                'form' => $this->createEntityForm($entity)->createView(),
                                'entity' => $entity
                           ), $default);
    }

    public function editAction($id = null)
    {
        $entity = $this->loadEntity($id);

        return $this->render(
            $this->getTemplateName('update'),
            $this->getRenderVars($entity)
        );
    }

    public function deleteAction($id = null)
    {
        $entity = $this->loadEntity($id);
        $form   = $this->createForm(
            self::SURE_FORM_ALIAS,
            null,
            array(
                 'action' => $this->getUrlDeletePersist($id)
            )
        );

        return $this->render(
            $this->getTemplateName('delete'),
            array('entity' => $entity, 'form' => $form->createView())
        );
    }

    public function deletePersistAction($id, Request $request)
    {
        $entity = $this->loadEntity($id);

        $form = $this->createForm(self::SURE_FORM_ALIAS);
        $form->handleRequest($request);

        if ($form->get('yes')->isClicked()) {
            $this->persistDeleteEntity($entity);
            $this->getEntityManager()->flush();
        }

        return $this->redirect($this->getUrlRead($entity));
    }

    protected function persistDeleteEntity($entity)
    {
        $this->getEntityManager()->remove($entity);
    }

    public function persistAction($id = null, Request $request)
    {
        $entity = empty($id) ? $this->createNewEntity() : $this->loadEntity($id);

        $form = $this->createEntityForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();

            return $this->redirect($this->getUrlRead($entity));
        } else {
            return $this->render(
                $this->getTemplateName('update'),
                array(
                     'errors' => $form->getErrors(),
                     'form'   => $form->createView()
                )
            );
        }
    }

    protected function createEntityForm($entity = null)
    {
        return $this->createForm(
            $this->updateFormAlias,
            $entity,
            $this->getFormOptions($entity)
        );
    }

    protected function getFormOptions($entity)
    {
        return array(
            'action' => $this->getUrlUpdate($entity)
        );
    }

    /**
     * @return object
     */
    protected abstract function createNewEntity();

    /**
     * @param object $entity
     *
     * @return string
     */
    protected abstract function getUrlRead($entity);

    /**
     * @param object $entity
     *
     * @return string
     */
    protected abstract function getUrlUpdate($entity);

    /**
     * @param $id
     *
     * @return string
     */
    protected abstract function getUrlDeletePersist($id);

    /**
     * @param int $id
     *
     * @return object
     */
    protected function loadEntity($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * @return ObjectRepository
     */
    protected abstract function getRepository();

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
