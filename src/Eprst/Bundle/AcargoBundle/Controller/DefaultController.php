<?php

namespace Eprst\Bundle\AcargoBundle\Controller;

use Eprst\Bundle\AcargoBundle\Service\NavigationRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('EprstAcargoBundle:Default:index.html.twig');
    }

    public function subindexAction($navigationKey)
    {
        /** @var NavigationRegistry $navRegistry */
        $navRegistry = $this->get('eprst_acargo.navigation.registry');
        $navStruct = $navRegistry->getStructureForKey($navigationKey);

        $route = $this->extractRouteFromNavigation($navStruct);

        if (!empty($route))
        {
            /** @var Router $routes */
            $routes = $this->get('router');
            $routeObject = $routes->getRouteCollection()->get($route);
            $controller = $routeObject->getDefault('_controller');

            return $this->forward($controller, array_merge($routeObject->getDefaults(), array('_route' => $route)));
        }
        else
        {
            return $this->render('EprstAcargoBundle:Default:emptypage.html.twig', array(
                'navigationKey' => $navigationKey
            ));
        }
    }

    private function extractRouteFromNavigation($navStruct)
    {
        if (isset($navStruct['route']) && !isset($navStruct['is_auto_route']))
            return $navStruct['route'];

        if (isset($navStruct['children']) && is_array($navStruct['children']))
        {
            foreach ($navStruct['children'] as $child)
            {
                $route = $this->extractRouteFromNavigation($child);
                if (!empty($route))
                    return $route;
            }
        }

        return null;
    }
}
