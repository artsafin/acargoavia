<?php

namespace Eprst\Bundle\AcargoBundle\DependencyInjection\Compiler;

use Eprst\Bundle\AcargoBundle\Service\NavigationInterface;
use Eprst\Bundle\AcargoBundle\Service\NavigationRegistry;
use Eprst\Bundle\AcargoBundle\Service\NavigationRegistryInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * NavigationCompilerPass
 * 
 * @author Artur.Safin
 * @date 04.07.13 23:20
 */
class NavigationCompilerPass implements CompilerPassInterface
{
    const NAVIGATION_SERVICE_TAG_NAME = 'eprst.acargo.navigation';
    const REGISTRY_SERVICE = 'eprst_acargo.navigation.registry';

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition(self::REGISTRY_SERVICE))
        {
            return;
        }

        /** @var Definition $registryDef */
        $registryDef = $container->getDefinition(self::REGISTRY_SERVICE);

        $serviceIds = $container->findTaggedServiceIds(self::NAVIGATION_SERVICE_TAG_NAME);
        foreach ($serviceIds as $serviceKey => $tags)
        {
            $navService = $container->getDefinition($serviceKey);
            $navigationsParams = $navService->getArgument(0);
            if (is_string($navigationsParams))
                $navigationsParams = $container->getParameter(str_replace('%', '', $navigationsParams));

            if (is_array($navigationsParams))
            {
                foreach ($navigationsParams as $key => &$items)
                {
                    $items['route'] = "_auto_route_{$key}";
                    $items['is_auto_route'] = true;
                }
                $navService->replaceArgument(0, $navigationsParams);
            }

            $registryDef->addMethodCall('addNavigationService', array(new Reference($serviceKey)));
        }
    }
}
