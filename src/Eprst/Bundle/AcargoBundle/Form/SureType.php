<?php

namespace Eprst\Bundle\AcargoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Translation\TranslatorInterface;

class SureType extends AbstractType
{
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    private $translator;

    function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $_ = \Eprst\Util\gettrans($this->translator, 'common');
        $builder
            ->add('yes', 'submit', array('attr' => array('class' => 'btn btn-danger'), 'label' => $_('Yes')))
            ->add('no', 'submit', array('attr' => array('class' => 'btn'), 'label' => $_('No')))
        ;
        $builder->setAction($options['action']);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    }

    public function getName()
    {
        return 'acargobundle_sure';
    }
}
