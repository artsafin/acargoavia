<?php

if (getenv('APP_ENV') === 'dev') {
    require __DIR__ . '/app_dev.php';
} else {
    require __DIR__ . '/app.php';
}