(function($){
	$.fn.acRowSelect = function (options)
	{
		options = $.extend({
			selectClass: 'acrowselect-selected',
			editLinkSelector: 'a.ac-edit-link'
		}, options);

		var prevSelectedEl = null;

		var onRowSelect = function(e){
			var target = $(e.currentTarget);

			if (target == prevSelectedEl)
				return;

			if (prevSelectedEl) {
				prevSelectedEl.removeClass(options.selectClass);
			}
			prevSelectedEl = target.addClass(options.selectClass);
		};

		var onRowEdit = function(e){
			var target = $(e.currentTarget);

			window.location.href = target.find(options.editLinkSelector).attr('href');
		};

		this.find('tbody tr').each(function(index, tr){
			$(tr).on('click', onRowSelect);
			$(tr).on('dblclick', onRowEdit);
		});

		return this;
	};
})(jQuery);