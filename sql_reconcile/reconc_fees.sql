SELECT s.id, s.waybill, s.create_ts, s.date_waybill, s.total_fee, round( f.myc + f.xdc, 2 )
FROM `shipment` s
JOIN shipment_fee f ON f.shipment_id = s.id
WHERE s.total_fee != round( f.myc + f.xdc, 2 )