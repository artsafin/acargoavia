SELECT 'AGENT', id, create_ts, date_waybill, waybill, agent_rate_pay as actual_value, round(agent_rate*chargeable_weight_agent, 3) as calc_value FROM `shipment`
where agent_rate_strategy != 'min' and agent_rate_pay != round(agent_rate*chargeable_weight_agent, 3)

union

SELECT 'CARR', id, create_ts, date_waybill, waybill, carrier_rate_pay, round(carrier_rate*chargeable_weight_carrier, 3) FROM `shipment`
where carrier_rate_strategy != 'min' and carrier_rate_pay != round(carrier_rate*chargeable_weight_carrier, 3)